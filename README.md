# LPMS-Control Version 2

## Introduction

This is the source code of LPMS-Control 2, an application based on OpenZen to adjust the operational parameters of LPMS sensors and to visualize their data.
The GUI of the application is very similar to our LPMS-Control 1 application, but the backend is now completely based on OpenZen and has therefore been completely rewritten.
The application is still work in progress. Our goal is to update this application frequently and set by step make it work for all our sensors and their features.

We do not provide binaries for this application yet. If you are looking for binaries, please refer to LPMS-Control version 1 for now. 
LPMS-Control version 1 can be found on our support page: https://lp-research.com/support/

## Compilation

1. Download external dependencies:

```git submodule update --init --recursive```

## Linux Deployment

As LpmsControl depends on Qt5 for Bluetooth support, the Qt shared libraries
need to bundled to correctly install LpmsControl on a vanilla machine. Follow
these steps to create a Linux AppImage that can be installed on other Linux
machines:

1. Download the [linuxdeployqt](https://github.com/probonopd/linuxdeployqt)
   AppImage for Qt5
   ([binaries](https://github.com/probonopd/linuxdeployqt/releases/tag/5))
   and make it executable

```chmod +x ~/Downloads/linuxdeployqt-5-x86_64.AppImage```

2. Make sure you use the Qt5 installation as default

```sudo apt install qt5-default``` (for Ubuntu)

3. Run CMake

```cmake -DCMAKE_BUILD_TYPE=Release -DZEN_STATIC_LINK_LIBCXX=On {srcpath}```

4. Install LpmsControl to `appdir`

```make DESTDIR=appdir -j8 install```

5. Generate an AppImage

```~/Downloads/linuxdeployqt-5-x86_64.AppImage appdir/usr/local/share/applications/LpmsControl.desktop -appimage```

6. The `LpmsControl-x86_64.AppImage` can be deployed to any Linux system. Make
   sure to execute with sudo rights, as LpmsControl requires elevated
   permissions.

```sudo ./LpmsControl-x86_64.AppImage```
