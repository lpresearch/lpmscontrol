/***********************************************************************
** (c) LP-RESEARCH Inc.
** All rights reserved
** Contact: info@lp-research.com
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#include "MainWindow.h"

#include <functional>
#include <iomanip>
#include <optional>
#include <string_view>

#include <boost/tokenizer.hpp>
#include <pugixml.hpp>

#include "OpenZen.h"

namespace
{
    void serialize(pugi::xml_node& node, std::string_view tag, const std::string& value)
    {
        node.append_child(tag.data()).append_child(pugi::node_pcdata).set_value(value.data());
    }

    std::string deserializeString(const pugi::xml_node& node, std::string_view tag)
    {
        return node.child_value(tag.data());
    }

    template <typename T>
    std::optional<T> deserialize(const pugi::xml_node& node, std::string_view tag);

    template <typename T>
    void serialize(pugi::xml_node& node, std::string_view tag, T value)
    {
        const std::string valueString = std::to_string(value);
        serialize(node, tag, valueString);
    }

    template void serialize<float>(pugi::xml_node& node, std::string_view tag, float value);

    template <>
    std::optional<float> deserialize<float>(const pugi::xml_node& node, std::string_view tag)
    {
        const auto line = deserializeString(node, tag);
        try
        {
            return std::stof(line);
        }
        catch (...)
        {
            return {};
        }
    }

    void serialize(pugi::xml_node& node, std::string_view tag, const LpVector3f& value)
    {
        const std::string valueString =
            std::to_string(value.data[0]) + ", " +
            std::to_string(value.data[1]) + ", " +
            std::to_string(value.data[2]);

        serialize(node, tag, valueString);
    }

    template <>
    std::optional<LpVector3f> deserialize<LpVector3f>(const pugi::xml_node& node, std::string_view tag)
    {
        const auto line = deserializeString(node, tag);
        if (line.empty())
            return {};

        boost::char_separator<char> sep(", ");
        boost::tokenizer<boost::char_separator<char>> tokens(line, sep);

        LpVector3f result;
        unsigned idx = 0;
        for (const std::string& token : tokens)
        {
            try
            {
                result.data[idx] = std::stof(token);
            }
            catch (...)
            {
                return {};
            }

            if (++idx == 3)
                return result;
        }

        return {};
    }

    void serialize(pugi::xml_node& node, std::string_view tag, const LpMatrix3x3f& value)
    {
        const std::string valueString =
            std::to_string(value.data[0][0]) + ", " + std::to_string(value.data[0][1]) + ", " + std::to_string(value.data[0][2]) + ", " +
            std::to_string(value.data[1][0]) + ", " + std::to_string(value.data[1][1]) + ", " + std::to_string(value.data[1][2]) + ", " +
            std::to_string(value.data[2][0]) + ", " + std::to_string(value.data[2][1]) + ", " + std::to_string(value.data[2][2]);

        serialize(node, tag, valueString);
    }

    template <>
    std::optional<LpMatrix3x3f> deserialize<LpMatrix3x3f>(const pugi::xml_node& node, std::string_view tag)
    {
        const auto line = deserializeString(node, tag);
        if (line.empty())
            return {};

        boost::char_separator<char> sep(", ");
        boost::tokenizer<boost::char_separator<char>> tokens(line, sep);

        LpMatrix3x3f result;
        unsigned idx = 0;
        for (const std::string& token : tokens)
        {
            try
            {
                result.data[idx / 3][idx % 3] = std::stof(token);
            }
            catch (...)
            {
                return {};
            }

            if (++idx == 9)
                return result;
        }

        return {};
    }

    SensorGuiContainerBase* findContainerById(std::list<SensorGuiContainerBase*>& containers, int openMatId)
    {
        for (auto* container : containers)
            if (openMatId == container->getOpenMatId())
                return container;

        return nullptr;
    }

    const SensorGuiContainerBase* findContainerByImu(const std::list<SensorGuiContainerBase*>& containers, ZenSensorHandle_t sensorHandle, ZenComponentHandle_t componentHandle)
    {
        if (!sensorHandle.handle || !componentHandle.handle)
            return nullptr;

        for (const auto* container : containers)
            if (container->getImuComponent().sensor().handle == sensorHandle.handle &&
                container->getImuComponent().component().handle == componentHandle.handle)
                return container;

        return nullptr;
    }


    ZenError prepareForAccelerometerAlignmentCalibration(zen::ZenSensorComponent imu)
    {
        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputQuat, true))
            return error;

        return imu.setBoolProperty(ZenImuProperty_OutputRawAcc, true);
    }

    ZenError prepareForGyroAlignmentCalibration(zen::ZenSensorComponent imu)
    {
        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputQuat, true))
            return error;

        return imu.setBoolProperty(ZenImuProperty_OutputRawGyr, true);
    }

    ZenError prepareForMagnetometerCalibration(zen::ZenSensorComponent imu)
    {
        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputEuler, true))
            return error;

        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputRawAcc, true))
            return error;

        return imu.setBoolProperty(ZenImuProperty_OutputRawMag, true);
    }

    ZenError prepareForMagnetometerAlignmentCalibration(zen::ZenSensorComponent imu)
    {
        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputQuat, true))
            return error;

        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputRawAcc, true))
            return error;

        return imu.setBoolProperty(ZenImuProperty_OutputRawMag, true);
    }

    ZenError prepareForMagnetometerReferenceCalibration(zen::ZenSensorComponent imu)
    {
        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputQuat, true))
            return error;

        if (auto error = imu.setBoolProperty(ZenImuProperty_OutputRawAcc, true))
            return error;

        return imu.setBoolProperty(ZenImuProperty_OutputRawMag, true);
    }
}

static std::vector<ZenSensorDesc> deviceList;

MainWindow::MainWindow(zen::ZenClient client, QWidget* parent)
    : m_client(std::move(client))
    , m_defaultBaudRate(921600)
    , m_isVisualizing(false)
    , rePlayer(new MotionPlayer())
    , m_terminate(false)
    , m_accAlignCalibrating(false)
    , m_accAlignInstructing(false)
    , m_gyrAlignCalibrating(false)
    , m_gyrAlignInstructing(false)
    , m_magAlignCalibrating(false)
    , m_magAlignInstructing(false)
    , m_magRefCalibrating(false)
    , m_magRefInstructing(false)
    , m_magCalibrating(false)
    , m_magInstructing(false)
    , m_recording(false)
    , m_uploadingFirmware(false)
{
    std::cout << "[MainWindow] Initializing program" << std::endl;

    QSplitter *s0 = new QSplitter();

    s0->addWidget(createDeviceList());
    s0->addWidget(createGraphs());

    createMenuAndToolbar();

    rescanD = new RescanDialog(m_client, comboDeviceList, deviceList, this);

    s0->setStretchFactor(0, 3);
    s0->setStretchFactor(1, 5);

    QHBoxLayout *h0 = new QHBoxLayout();
    h0->addWidget(s0);

    QWidget* cw = new QWidget();
    cw->setLayout(h0);
    setCentralWidget(cw);

    this->setMinimumSize(800, 600);
    showMaximized();

    setWindowTitle("LpmsControl-V"+ QString(LPMS_CONTROL_VERSION) + " GUI");

    isConnecting = false;
    calibratingMag = false;

    m_graphThread = std::thread(&MainWindow::graphLoop, this);

    textUpdateCounter = 0;
    mode = MODE_GRAPH_WIN;

    rePlayer = new MotionPlayer();
    connect(rePlayer, &MotionPlayer::finished, this, &MainWindow::playbackFinished);
    softwareSyncCount = 0;

    connect(this, &MainWindow::accAlignCaliStageChanged, this, &MainWindow::stopWaitBar);
    connect(this, &MainWindow::gyrAlignCaliStageChanged, this, &MainWindow::stopWaitBar);
    connect(this, &MainWindow::magAlignCaliStageChanged, this, &MainWindow::stopWaitBar);

    qRegisterMetaType<ZenImuData>("ZenImuData");

    /* connect(this, &MainWindow::imuDataChanged, this, [this](int openMatId, const ZenImuData& imuData) {
        if (auto* container = findContainerById(lpmsList, openMatId))
            container->setLastImuData(imuData);
    }); */
    connect(this, &MainWindow::imuDataChanged, this, &MainWindow::updateGraph);
    /* connect(this, &MainWindow::imuDataChanged, this, &MainWindow::updateFieldmap);
    connect(this, &MainWindow::imuDataChanged, this, &MainWindow::recordImuData); */
}

MainWindow::~MainWindow()
{
    m_terminate = true;
    if (m_graphThread.joinable())
        m_graphThread.join();
}

QWidget *MainWindow::createDeviceList(void)
{
    lpmsTree = new QTreeWidget();

    lpmsTree->setColumnCount(1);
    lpmsTree->setHeaderLabel(QString("Connected devices"));
    currentLpms = 0;
    lpmsTree->selectionModel()->setCurrentIndex(QModelIndex(), QItemSelectionModel::Clear);
    lpmsTree->setVerticalScrollMode(QTreeView::ScrollPerPixel);

    connect(lpmsTree, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT(updateCurrentLpms(QTreeWidgetItem *, QTreeWidgetItem *)));

    return (QWidget*)lpmsTree;
}

QGroupBox *MainWindow::createGraphs(void)
{
    graphLayout = new QVBoxLayout();
    cubeWindowContainer = new CubeWindowSelector();
    graphWindow = new GraphWindow();
    fieldMapWindow = new FieldMapContainer();

    graphLayout->addWidget(graphWindow);
    graphLayout->addWidget(cubeWindowContainer);
    graphLayout->addWidget(fieldMapWindow);

    gb = new QGroupBox("Data view");
    gb->setLayout(graphLayout);

    cubeWindowContainer->hide();
    fieldMapWindow->hide();

    return gb;
}

void MainWindow::createMenuAndToolbar(void)
{
    toolbar = new QToolBar("Toolbar");
    addToolBar(Qt::TopToolBarArea, toolbar);
    toolbar->setMovable(false);
    toolbar->setFloatable(false);
    QMenu* connectMenu = menuBar()->addMenu("&Connect");

    QAction* connectAction = new QAction(QIcon(":/icons/bolt_32x32.png"), "&Connect", this);
    QAction* disconnectAction = new QAction(QIcon(":/icons/x_28x28.png"), "&Disconnect", this);
    QAction* addRemoveAction = new QAction(QIcon(":/icons/plus_32x32.png"), "&Add / remove sensor", this);
    QAction* exitAction = new QAction("E&xit program", this);

    QVBoxLayout *v3 = new QVBoxLayout();
    comboDeviceList = new QComboBox();
    v3->addWidget(new QLabel("Preferred devices:"));
    v3->addWidget(comboDeviceList);
    QWidget *w3 = new QWidget();
    w3->setLayout(v3);
    w3->setFixedWidth(200);
    toolbar->addWidget(w3);

    QVBoxLayout *v6 = new QVBoxLayout();
    baudRateList = new QComboBox();
    baudRateList->addItem("19200 bps", 19200);
    baudRateList->addItem("38400 bps", 38400);
    baudRateList->addItem("57600 bps", 57600);
    baudRateList->addItem("115200 bps", 115200);
    baudRateList->addItem("230400 bps", 230400);
    baudRateList->addItem("256000 bps", 256000);
    baudRateList->addItem("460800 bps", 460800);
    baudRateList->addItem("921600 bps", 921600);
    baudRateList->setCurrentIndex(7);
    v6->addWidget(new QLabel("Baud rate:"));
    v6->addWidget(baudRateList);
    QWidget *w6 = new QWidget();
    w6->setLayout(v6);
    w6->setFixedWidth(150);
    toolbar->addWidget(w6);
    connect(baudRateList, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBaudRate(int)));

    connectMenu->addAction(connectAction);
    connectMenu->addAction(disconnectAction);
    connectMenu->addAction(addRemoveAction);
    connectMenu->addSeparator();
    connectMenu->addAction(exitAction);

    toolbar->addAction(connectAction);
    toolbar->addAction(disconnectAction);
    toolbar->addAction(addRemoveAction);

    QMenu* measurementMenu = menuBar()->addMenu("&Measurement");
    startAction = new QAction(QIcon(":/icons/play_24x32.png"), "&Start measurement", this);
    QAction* browseAction = new QAction(QIcon(":/icons/folder_stroke_32x32.png"), "&Browse record file", this);
    QAction* stopAction = new QAction("Stop measurement", this);
    saveAction = new QAction(QIcon(":/icons/layers_32x28.png"), "&Record data", this);

    measurementMenu->addAction(startAction);
    measurementMenu->addAction(browseAction);
    measurementMenu->addAction(saveAction);

    toolbar->addSeparator();
    QVBoxLayout *v = new QVBoxLayout();
    recordFileEdit = new QLineEdit();
    recordFileEdit->setReadOnly(true);
    recordFileEdit->setText("Not set, please browse..");
    recordFileSet = false;
    v->addWidget(new QLabel("Record filename:"));
    v->addWidget(recordFileEdit);
    QWidget *w = new QWidget();
    w->setLayout(v);
    w->setFixedWidth(200);

    toolbar->addAction(startAction);
    toolbar->addAction(saveAction);
    toolbar->addWidget(w);
    toolbar->addAction(browseAction);

    replayAction = new QAction(QIcon(":/icons/loop_alt2_32x28.png"), "&Playback data", this);
    QAction* browseReplayAction = new QAction(QIcon(":/icons/folder_stroke_32x32.png"), "&Browse replay file", this);

    toolbar->addSeparator();
    QVBoxLayout *v7 = new QVBoxLayout();
    playbackFileEdit = new QLineEdit();
    playbackFileEdit->setReadOnly(true);
    playbackFileEdit->setText("Not set, please browse..");
    playbackFileSet = false;
    v7->addWidget(new QLabel("Playback filename:"));
    v7->addWidget(playbackFileEdit);
    QWidget *w7 = new QWidget();
    w7->setLayout(v7);
    w7->setFixedWidth(200);

    toolbar->addAction(replayAction);
    toolbar->addWidget(w7);
    toolbar->addAction(browseReplayAction);

    measurementMenu->addSeparator();
    measurementMenu->addAction(browseReplayAction);
    measurementMenu->addAction(replayAction);

    //startFlashLogAction = new QAction("Start Flash Log", this);
    //stopFlashLogAction = new QAction("Stop Flash Log", this);
    //clearFlashLogAction = new QAction("Clear Flash Log", this);
    //eraseFlashAction = new QAction("Full Flash Erase", this);
    //saveFlashLogAction = new QAction("Save Flash Log", this);

    //measurementMenu->addSeparator();
    //measurementMenu->addAction(startFlashLogAction);
    //measurementMenu->addAction(stopFlashLogAction);
    //measurementMenu->addAction(clearFlashLogAction);
    //measurementMenu->addAction(eraseFlashAction);
    //measurementMenu->addAction(saveFlashLogAction);

    toolbar->addSeparator();

    QVBoxLayout *v2 = new QVBoxLayout();
    targetCombo = new QComboBox();
    targetCombo->addItem("All sensors");
    targetCombo->addItem("Selected sensor");
    v2->addWidget(new QLabel("Reset target:"));
    v2->addWidget(targetCombo);
    QWidget *w2 = new QWidget();
    w2->setLayout(v2);
    w2->setFixedWidth(150);
    toolbar->addWidget(w2);

    QVBoxLayout *v5 = new QVBoxLayout();
    resetMethodCombo = new QComboBox();
    resetMethodCombo->addItem("Object reset");
    resetMethodCombo->addItem("Heading reset");
    resetMethodCombo->addItem("Alignment reset");
    v5->addWidget(new QLabel("Reset method:"));
    v5->addWidget(resetMethodCombo);
    QWidget *w5 = new QWidget();
    w5->setLayout(v5);
    w5->setFixedWidth(150);
    toolbar->addWidget(w5);

    QAction* setOffsetAction = new QAction(QIcon(":/icons/fullscreen_exit_32x32.png"), "Set offset", this);
    toolbar->addAction(setOffsetAction);
    QAction* resetOffsetAction = new QAction(QIcon(":/icons/denied_32x32.png"), "Reset offset", this);
    toolbar->addAction(resetOffsetAction);

    QMenu* calibrationMenu = menuBar()->addMenu("&Calibration");

    QAction* gyroAction = new QAction("Calibrate &gyroscope", this);
    QAction* startMagAction = new QAction("Calibrate &mag. (ellipsoid fit)", this);
    QAction* resetSingleRefAction = new QAction("Reset &heading (selected)", this);
    QAction* resetAllRefAction = new QAction("Reset heading (&all)", this);
    QAction* resetSingleOrientationAction = new QAction("Reset &offset (selected)", this);
    QAction* resetAllOrientationAction = new QAction("Reset o&ffset (all)", this);
    QAction* saveCalAction = new QAction("Save &parameters to sensor", this);
    QAction* resetToFactoryAction = new QAction("Reset to factory settings", this);
    QAction* mACalculateAction = new QAction("Calibrate acc. misalignment", this);
    QAction* gyrMaCalculateAction = new QAction("Calibrate gyr. misalignment", this);
    QAction* magMaCalculateAction = new QAction("Calibrate mag. misalignment (HH-coils)", this);
    QAction* saveToFileAction = new QAction("Save calibration file", this);
    QAction* loadFromFileAction = new QAction("Load calibration file", this);
    QAction* timestampResetAction = new QAction("Reset Timestamp (All sensors)", this);
    //QAction* softwareSyncStartAction = new QAction("Software Sync", this);
    //QAction* softwareSyncStopAction = new QAction("Software Sync Stop", this);

    calibrationMenu->addAction(gyroAction);
    calibrationMenu->addAction(startMagAction);
    calibrationMenu->addSeparator();
    calibrationMenu->addAction(saveCalAction);
    calibrationMenu->addAction(saveToFileAction);
    calibrationMenu->addAction(loadFromFileAction);
    calibrationMenu->addSeparator();
    calibrationMenu->addAction(setOffsetAction);
    calibrationMenu->addAction(resetOffsetAction);
    calibrationMenu->addSeparator();
    calibrationMenu->addAction(timestampResetAction);
    //calibrationMenu->addAction(softwareSyncStartAction);
    //calibrationMenu->addAction(softwareSyncStopAction);
    calibrationMenu->addSeparator();
    calibrationMenu->addAction(resetToFactoryAction);

    viewMenu = menuBar()->addMenu("&View");
    QAction* graphAction = new QAction(QIcon(":/icons/rss_alt_32x32.png"), "Graph &window", this);
    QAction* orientationGraphAction = new QAction(QIcon(":/icons/target_32x32.png"), "Orien&tation window", this);
    QAction* pressureGraphAction = new QAction(QIcon(":/icons/eyedropper_32x32.png"), "Pressu&re window", this);
    QAction* threedAction = new QAction(QIcon(":/icons/share_32x32.png"), "3D &visualization", this);
    QAction* fieldMapAction = new QAction(QIcon(":/icons/sun_fill_32x32.png"), "&Magnetic field map", this);
    QAction* loadObjFileAction = new QAction("&Load object file", this);
    QAction* heaveMotionGraphAction = new QAction(QIcon(":/icons/bars_32x32.png"), "Heave motion window", this);
    xAxisAutoScrollingAction = new QAction("&X Axis Auto Scrolling", this);
    xAxisAutoScrollingAction->setCheckable(true);
    xAxisAutoScrollingAction->setChecked(false);
    yAxisAutoScalingAction = new QAction("&Y Axis Auto Scaling", this);
    yAxisAutoScalingAction->setCheckable(true);
    yAxisAutoScalingAction->setChecked(false);
    cubeMode1Action = new QAction("3D view mode &1", this);
    cubeMode1Action->setCheckable(true);
    cubeMode1Action->setChecked(true);
    cubeMode2Action = new QAction("3D view mode &2", this);
    cubeMode2Action->setCheckable(true);
    cubeMode4Action = new QAction("3D view mode &4", this);
    cubeMode4Action->setCheckable(true);

    viewMenu->addAction(graphAction);
    viewMenu->addAction(orientationGraphAction);
    viewMenu->addAction(pressureGraphAction);

    viewMenu->addAction(threedAction);
    viewMenu->addAction(fieldMapAction);
    viewMenu->addSeparator();
    viewMenu->addAction(cubeMode1Action);
    viewMenu->addAction(cubeMode2Action);
    viewMenu->addAction(cubeMode4Action);
    viewMenu->addSeparator();
    viewMenu->addAction(xAxisAutoScrollingAction);
    viewMenu->addAction(yAxisAutoScalingAction);
    viewMenu->addSeparator();
    viewMenu->addAction(loadObjFileAction);
    viewMenu->addSeparator();
    viewMenu->addAction(heaveMotionGraphAction);

    toolbar->addSeparator();

    QWidget *stretchWidget = new QWidget();
    stretchWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    toolbar->addWidget(stretchWidget);

    toolbar->addSeparator();
    toolbar->addAction(graphAction);
    toolbar->addAction(orientationGraphAction);
    toolbar->addAction(pressureGraphAction);
    toolbar->addAction(threedAction);
    toolbar->addAction(fieldMapAction);
    toolbar->addAction(heaveMotionGraphAction);

    QMenu* expertMenu = menuBar()->addMenu("&Advanced");
    QAction* firmwareAction = new QAction("Upload firmware", this);
    QAction* iapAction = new QAction("Upload IAP", this);
    QAction* latencyAction = new QAction("Measure latency", this);
    QAction* versionAction = new QAction("Version info", this);

    expertMenu->addAction(firmwareAction);
    expertMenu->addAction(iapAction);
    expertMenu->addSeparator();
    expertMenu->addAction(mACalculateAction);
    expertMenu->addAction(gyrMaCalculateAction);
    expertMenu->addAction(magMaCalculateAction);
    expertMenu->addSeparator();
    expertMenu->addAction(versionAction);

    connect(startMagAction, SIGNAL(triggered()), this, SLOT(calibrateMag()));
    connect(startAction, SIGNAL(triggered()), this, SLOT(startMeasurement()));
    connect(gyroAction, SIGNAL(triggered()), this, SLOT(recalibrate()));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(toggleRecordData()));
    connect(browseAction, SIGNAL(triggered()), this, SLOT(browseRecordFile()));
    connect(saveCalAction, SIGNAL(triggered()), this, SLOT(saveCalibration()));
    connect(connectAction, SIGNAL(triggered()), this, SLOT(openSensor()));
    connect(disconnectAction, SIGNAL(triggered()), this, SLOT(closeSensor()));
    connect(graphAction, SIGNAL(triggered()), this, SLOT(selectGraphWindow()));
    connect(orientationGraphAction, SIGNAL(triggered()), this, SLOT(selectGraph2Window()));
    connect(pressureGraphAction, SIGNAL(triggered()), this, SLOT(selectGraph3Window()));
    connect(threedAction, SIGNAL(triggered()), this, SLOT(selectThreeDWindow()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
    connect(firmwareAction, SIGNAL(triggered()), this, SLOT(uploadFirmware()));
    connect(iapAction, SIGNAL(triggered()), this, SLOT(uploadIap()));
    connect(latencyAction, SIGNAL(triggered()), this, SLOT(measureLatency()));
    connect(versionAction, SIGNAL(triggered()), this, SLOT(getVersionInfo()));
    connect(fieldMapAction, SIGNAL(triggered()), this, SLOT(selectFieldMapWindow()));
    connect(heaveMotionGraphAction, SIGNAL(triggered()), this, SLOT(selectHeaveMotionWindow()));
    connect(resetToFactoryAction, SIGNAL(triggered()), this, SLOT(resetToFactory()));
    connect(mACalculateAction, SIGNAL(triggered()), this, SLOT(misalignmentCal()));
    connect(gyrMaCalculateAction, SIGNAL(triggered()), this, SLOT(gyrMisalignmentCal()));
    connect(magMaCalculateAction, SIGNAL(triggered()), this, SLOT(magMisalignmentCal()));
    connect(loadFromFileAction, SIGNAL(triggered()), this, SLOT(loadCalibrationData()));
    connect(saveToFileAction, SIGNAL(triggered()), this, SLOT(saveCalibrationData()));
    connect(addRemoveAction, SIGNAL(triggered()), this, SLOT(addRemoveDevices()));
    connect(cubeMode1Action, SIGNAL(triggered()), this, SLOT(selectCubeMode1()));
    connect(cubeMode2Action, SIGNAL(triggered()), this, SLOT(selectCubeMode2()));
    connect(cubeMode4Action, SIGNAL(triggered()), this, SLOT(selectCubeMode4()));
    connect(replayAction, SIGNAL(triggered()), this, SLOT(startReplay()));
    connect(browseReplayAction, SIGNAL(triggered()), this, SLOT(browsePlaybackFile()));
    connect(setOffsetAction, SIGNAL(triggered()), this, SLOT(setOffset()));
    connect(resetOffsetAction, SIGNAL(triggered()), this, SLOT(resetOffset()));
    connect(timestampResetAction, SIGNAL(triggered()), this, SLOT(timestampReset()));
    connect(loadObjFileAction, SIGNAL(triggered()), this, SLOT(loadObjFile()));
    connect(xAxisAutoScrollingAction, SIGNAL(triggered()), this, SLOT(xAxisAutoScrolling()));
    connect(yAxisAutoScalingAction, SIGNAL(triggered()), this, SLOT(yAxisAutoScaling()));

    //connect(softwareSyncStartAction, SIGNAL(triggered()), this, SLOT(softwareSyncStart()));
    //connect(softwareSyncStopAction, SIGNAL(triggered()), this, SLOT(softwareSyncStop()));
    //connect(startFlashLogAction, SIGNAL(triggered()), this, SLOT(startFlashLog()));
    //connect(stopFlashLogAction, SIGNAL(triggered()), this, SLOT(stopFlashLog()));
    //connect(clearFlashLogAction, SIGNAL(triggered()), this, SLOT(clearFlashLog()));
    //connect(eraseFlashAction, SIGNAL(triggered()), this, SLOT(eraseFlash()));
    //connect(saveFlashLogAction, SIGNAL(triggered()), this, SLOT(saveFlashLog()));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (exitWindow() == true) {
        event->accept();
    }
    else {
        event->ignore();
    }
}

void MainWindow::selectGraphWindow(void)
{
    cubeWindowContainer->hide();
    fieldMapWindow->hide();

    graphWindow->setMode(GRAPH_MODE_RAW);
    graphWindow->show();

    mode = MODE_GRAPH_WIN;
}

void MainWindow::selectGraph2Window(void)
{
    cubeWindowContainer->hide();
    fieldMapWindow->hide();

    graphWindow->setMode(GRAPH_MODE_ORIENTATION);
    graphWindow->show();

    mode = MODE_GRAPH_WIN;
}

void MainWindow::selectGraph3Window(void)
{
    cubeWindowContainer->hide();
    fieldMapWindow->hide();

    graphWindow->setMode(GRAPH_MODE_PRESSURE);
    graphWindow->show();

    mode = MODE_GRAPH_WIN;
}

void MainWindow::selectHeaveMotionWindow(void)
{
    cubeWindowContainer->hide();
    fieldMapWindow->hide();

    graphWindow->setMode(GRAPH_MODE_HEAVEMOTION);
    graphWindow->show();

    mode = MODE_GRAPH_WIN;
}

void MainWindow::selectThreeDWindow(void)
{
    graphWindow->hide();
    fieldMapWindow->hide();

    cubeWindowContainer->update();
    cubeWindowContainer->show();

    mode = MODE_THREED_WIN;
}

void MainWindow::selectFieldMapWindow(void)
{
    graphWindow->hide();
    cubeWindowContainer->hide();

    fieldMapWindow->show();

    mode = MODE_FIELDMAP_WIN;
}

void MainWindow::selectCubeMode1(void)
{
    cubeMode1Action->setChecked(true);
    cubeMode2Action->setChecked(false);
    cubeMode4Action->setChecked(false);

    cubeWindowContainer->selectCube(CUBE_VIEW_MODE_1);
}

void MainWindow::selectCubeMode2(void)
{
    cubeMode1Action->setChecked(false);
    cubeMode2Action->setChecked(true);
    cubeMode4Action->setChecked(false);

    cubeWindowContainer->selectCube(CUBE_VIEW_MODE_2);
}

void MainWindow::selectCubeMode4(void)
{
    cubeMode1Action->setChecked(false);
    cubeMode2Action->setChecked(false);
    cubeMode4Action->setChecked(true);

    cubeWindowContainer->selectCube(CUBE_VIEW_MODE_4);
}

bool MainWindow::exitWindow(void)
{
    QMessageBox msgBox;

    closeSensor();

    return true;
}

void MainWindow::graphLoop()
{
    constexpr auto FRAME_TIME = std::chrono::milliseconds(10);
    while (!m_terminate)
    {
        const auto frameStart = std::chrono::high_resolution_clock::now();

        if (softwareSyncCount > 0)
            if (--softwareSyncCount == 0)
                softwareSyncStop();

        if (rePlayer->isPlaying())
        {
            const auto nData = rePlayer->hasImuData();
            for (auto i = 0; i < nData; ++i)
            {
                const auto imuData = rePlayer->getData();
                emit imuDataChanged(imuData.first, imuData.second);
            }
        }

        while (auto event = m_client.pollNextEvent())
        {
            if (const auto* container = findContainerByImu(lpmsList, event->sensor, event->component))
            {
                switch (event->eventType)
                {
                case ZenImuEvent_Sample:
                    handleImuEvent(*container, event->data.imuData);
                    break;
                }
            }
            else
            {
                switch (event->eventType)
                {
                case ZenSensorEvent_SensorFound:
                    rescanD->addDiscoveredDevice(event->data.sensorFound);
                    break;

                case ZenSensorEvent_SensorListingProgress:
                    rescanD->updateDiscoveryProgress(event->data.sensorListingProgress.progress);
                    break;
                }
            }
        }

        std::this_thread::sleep_until(frameStart + FRAME_TIME);
    }
}

int outputCount = 0;

void MainWindow::handleImuEvent(const SensorGuiContainerBase& container, const ZenImuData& imuData)
{
    if (!m_isVisualizing)
        return;

    if (currentLpms != &container)
        return;

    // if (container.getModelName().find("LPMS-IG1") != std::string::npos) return;

    if (m_accAlignCalibrating)
    {
        if (!m_accAlignInstructing)
        {
            const float deltaT = container.hasImuData() ? imuData.timestamp - container.lastImuData().timestamp : 0.f;
            if (lpCalibrateAccAlignCalibrator(m_accAlignHandle, m_accAlignStage, imuData.aRaw, deltaT) == LpCaliError_None)
                if (auto newStage = lpAccAlignCalibrationStage(m_accAlignHandle))
                    emit accAlignCaliStageChanged(newStage);
        }
    }
    else if (m_gyrAlignCalibrating)
    {
        if (!m_gyrAlignInstructing)
        {
            const float deltaT = container.hasImuData() ? imuData.timestamp - container.lastImuData().timestamp : 0.f;
            if (lpCalibrateGyrAlignCalibrator(m_gyrAlignHandle, m_gyrAlignStage, imuData.gRaw, deltaT) == LpCaliError_None)
                if (auto newStage = lpGyrAlignCalibrationStage(m_gyrAlignHandle))
                    emit gyrAlignCaliStageChanged(newStage);
        }
    }
    else if (m_magAlignCalibrating)
    {
        if (!m_magAlignInstructing)
        {
            const float deltaT = container.hasImuData() ? imuData.timestamp - container.lastImuData().timestamp : 0.f;
            if (lpCalibrateMagAlignCalibrator(m_magAlignHandle, m_magAlignStage, imuData.aRaw, imuData.bRaw, deltaT) == LpCaliError_None)
                if (auto newStage = lpMagAlignCalibrationStage(m_magAlignHandle))
                    emit magAlignCaliStageChanged(newStage);
        }
    }
    else if (m_magCalibrating)
    {
        if (!m_magInstructing)
        {
            const float deltaT = container.hasImuData() ? imuData.timestamp - container.lastImuData().timestamp : 0.f;
            if (lpCalibrateMagCalibrator(m_magHandle, imuData.a, imuData.bRaw, deltaT) == LpCaliError_None)
                if (lpIsMagCalibrationFinished(m_magHandle))
                    emit magCaliFinished();
        }
    }
    else if (m_magRefCalibrating)
    {
        if (!m_magRefInstructing)
        {
            const float deltaT = container.hasImuData() ? imuData.timestamp - container.lastImuData().timestamp : 0.f;
            if (lpCalibrateMagRefCalibrator(m_magRefHandle, imuData.a, imuData.b, deltaT) == LpCaliError_None)
                if (lpIsMagRefCalibrationFinished(m_magRefHandle))
                    emit magRefCaliFinished();
        }
    }

    if (outputCount < 2)
    {
        ++outputCount;
        return;
    }
    else {
        outputCount = 0;
        emit imuDataChanged(container.getOpenMatId(), imuData);
    }
}

void MainWindow::updateGraph(int openMatId, const ZenImuData& imuData)
{
    gb->setTitle("Data view (t: " + QString::number(imuData.timestamp, 'f', 1) + "s)");

    switch (mode)
    {
    case MODE_GRAPH_WIN:
        graphWindow->plotDataSet(imuData);
        break;

    case MODE_THREED_WIN:
        if (cubeWindowContainer->getMode() == CUBE_VIEW_MODE_1)
            cubeWindowContainer->getSelectedCube()->updateData(openMatId, imuData);
        break;
    }
}

void MainWindow::updateFieldmap(int, const ZenImuData& imuData)
{
    if (mode == MODE_FIELDMAP_WIN)
    {
        updateMagneticFieldMap(imuData);
        updateCurrentField(imuData);
    }
}

void MainWindow::recordImuData(int openMatId, const ZenImuData& imuData)
{
    if (m_recording)
    {
        if (m_firstRecord)
        {
            m_timestampOffset = imuData.timestamp;
            m_frameCountOffset = imuData.frameCount;
            m_firstRecord = false;
        }

        m_recordingStream
            << openMatId << ", "
            << std::fixed << std::setprecision(4) << (imuData.timestamp - m_timestampOffset) << std::fixed << std::setprecision(8) << ","
            << (imuData.frameCount - m_frameCountOffset) << ", "
            << imuData.a[0] << ", " << imuData.a[1] << ", " << imuData.a[2] << ", "
            << imuData.g[0] << ", " << imuData.g[1] << ", " << imuData.g[2] << ", "
            << imuData.b[0] << ", " << imuData.b[1] << ", " << imuData.b[2] << ", "
            << imuData.r[0] << ", " << imuData.r[1] << ", " << imuData.r[2] << ", "
            << imuData.q[0] << ", " << imuData.q[1] << ", " << imuData.q[2] << ", " << imuData.q[3] << ", "
            << imuData.linAcc[0] << ", " << imuData.linAcc[1] << ", " << imuData.linAcc[2] << ", "
            << imuData.pressure << ", "
            << imuData.altitude << ", "
            << imuData.temperature << ", "
            << imuData.hm.yHeave << std::endl;
    }
}

void MainWindow::playbackFinished()
{
    replayAction->setText("Playback data");
    replayAction->setIcon(QIcon(":/icons/loop_alt2_32x28.png"));
}

void MainWindow::updateBaudRate(int i)
{
    m_defaultBaudRate = baudRateList->itemData(i).toInt();
}

void MainWindow::updateCurrentLpms(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    int i = 0;

    if (lpmsTree->topLevelItemCount() == 0 || lpmsTree->currentItem() == NULL) {
        return;
    }

    QTreeWidgetItem *checkItem = lpmsTree->currentItem();
    int itemIndex = lpmsTree->indexOfTopLevelItem(checkItem);

    while (itemIndex == -1) {
        checkItem = checkItem->parent();
        itemIndex = lpmsTree->indexOfTopLevelItem(checkItem);
    }

    bool found = false;
    for (auto* container : lpmsList)
    {
        if (itemIndex == lpmsTree->indexOfTopLevelItem(container->getTreeItem()))
        {
            found = true;
            currentLpms = container;
            // currentLpms->updateData();
        }
    }

    if (!found)
    {
        if (lpmsTree->topLevelItemCount() > 0)
        {
            currentLpms = lpmsList.front();
            found = true;
        }
        else
        {
            currentLpms = nullptr;
            startButton->setStyleSheet("QPushButton { color: black; }");
            startButton->setText("Start measurement");
            m_isVisualizing = false;
        }
    }

    // [XXX] Change in logic so check to make sure
    if (found)
        if (currentLpms->hasImuData())
            updateMagneticFieldMap(currentLpms->lastImuData());

    //if (f == true) {
    //    currentLpms = *fit;

    //    graphWindow->setActiveLpms(currentLpms->getOpenMatId());
    //    updateMagneticFieldMap();

    //}
    //else {
    //    if (lpmsTree->topLevelItemCount() > 0) {
    //        currentLpms = lpmsList.front();
    //        updateMagneticFieldMap();
    //    }
    //    else {
    //        currentLpms = 0;
    //        startButton->setStyleSheet("QPushButton { color: black; }");
    //        startButton->setText("Start measurement");
    //        m_isVisualizing = false;
    //    }
    //}
    graphWindow->clearGraphs();

    //currentLpms->getSensor()->getConfigurationPrm(PRM_DEVICE_TYPE, &i);
    //if (i == DEVICE_LPMS_B2)
    //    enableFlashMenu();
    //else
    //    disableFlashMenu();
}

void MainWindow::openSensor(void)
{
    bool f;

    if (isConnecting == true) return;
    if (deviceList.size() == 0) return;
    auto& desc = deviceList.at(comboDeviceList->currentIndex());

    isConnecting = true;
    std::string deviceAddress = std::string(desc.name);

    f = false;
    std::list<SensorGuiContainerBase *>::iterator it;
    for (it = lpmsList.begin(); it != lpmsList.end(); ++it)
        if ((*it)->getSensor().equals(desc))
            f = true;

    if (f == true) {
        std::cout << "[LpmsControl] Device " << deviceAddress << " is already connected" << std::endl;
        isConnecting = false;
        return;
    }

    stopMeasurement();
    graphWindow->clearGraphs();

    // desc.baudRate = m_defaultBaudRate;
    auto [obtainError, sensor] = m_client.obtainSensor(desc);
    if (obtainError)
    {
        std::cout << "[LpmsControl] Device " << deviceAddress << " failed to connect" << std::endl;
        isConnecting = false;
        return;
    }

    std::optional<zen::ZenSensorComponent> imu;
    imu = sensor.getAnyComponentOfType(g_zenSensorType_Imu);

    auto sensorModelPair = sensor.getStringProperty(ZenSensorProperty_SensorModel);
    auto& sensorModelError = sensorModelPair.first;
    auto& sensorModelName = sensorModelPair.second;

    std::cout << "Sensor Model: " << sensorModelName << std::endl;

    if (sensorModelName.find("LPMS-IG1") != std::string::npos)
        currentLpms = new SensorGuiContainerIg1(static_cast<int>(lpmsList.size()), std::move(sensor), *imu, desc, sensorModelName, lpmsTree);
    else
        currentLpms = new SensorGuiContainer(static_cast<int>(lpmsList.size()), std::move(sensor), *imu, desc, sensorModelName, lpmsTree);

    lpmsList.push_back(currentLpms);

    lpmsTree->insertTopLevelItem(0, currentLpms->getTreeItem());
    lpmsTree->setCurrentItem(currentLpms->getTreeItem());

    currentLpms->getTreeItem()->setExpanded(true);
	currentLpms->updateData();

    isConnecting = false;
    startMeasurement();
}

void MainWindow::closeSensor(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    stopMeasurement();

    if (currentLpms) {
        SensorGuiContainerBase *temp = currentLpms;
        lpmsList.remove(temp);

        delete temp->getTreeItem();
        currentLpms = 0;

        delete temp;

        // Update openMat indices, as they are based on iteration
        int idx = 0;
        for (auto* container : lpmsList)
            container->setOpenMatId(idx++);

        updateCurrentLpms();
    }
    startMeasurement();
    gb->setTitle("Data view");
}

void MainWindow::startMeasurement(void)
{
    if (isConnecting == true) return;
    if (rePlayer->isPlaying()) return;

    if (!m_isVisualizing && lpmsList.size() > 0)
    {
        stopReplay();

        startAction->setText("Stop measurement");
        startAction->setIcon(QIcon(":/icons/pause_24x32.png"));

        graphWindow->clearGraphs();

        m_isVisualizing = true;
    }
    else
    {
        stopMeasurement();
    }
}

void MainWindow::stopMeasurement(void)
{
    startAction->setText("Start measurement");
    startAction->setIcon(QIcon(":/icons/play_24x32.png"));

    m_isVisualizing = false;
}

void MainWindow::setOffset(void)
{
    std::list<SensorGuiContainerBase *>::iterator it;
    int rm = 0;

    if (currentLpms == 0 || isConnecting == true) return;

    rm = resetMethodCombo->currentIndex();

    if (targetCombo->currentIndex() == 0)
    {
        for (auto* container : lpmsList)
            container->getImuComponent().setInt32Property(ZenImuProperty_OrientationOffsetMode, rm);
    }
    else
    {
        currentLpms->getImuComponent().setInt32Property(ZenImuProperty_OrientationOffsetMode, rm);
    }
}

void MainWindow::resetOffset(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    if (targetCombo->currentIndex() == 0)
    {
        for (auto* container : lpmsList)
            container->getImuComponent().executeProperty(ZenImuProperty_ResetOrientationOffset);
    }
    else
    {
        currentLpms->getImuComponent().executeProperty(ZenImuProperty_ResetOrientationOffset);
    }
}

void MainWindow::timestampReset(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    for (auto* container : lpmsList)
        container->getSensor().setInt32Property(ZenSensorProperty_TimeOffset, 0);
}

void MainWindow::softwareSyncStart(void)
{
    std::list<SensorGuiContainerBase *>::iterator it;

    if (currentLpms == 0 || isConnecting == true) return;

    for (it = lpmsList.begin(); it != lpmsList.end(); ++it) {
        //(*it)->getSensor()->startSync();
    }
    softwareSyncCount = 1000;
}


void MainWindow::softwareSyncStop(void)
{
    std::list<SensorGuiContainerBase *>::iterator it;

    if (currentLpms == 0 || isConnecting == true) return;

    for (it = lpmsList.begin(); it != lpmsList.end(); ++it) {
        //(*it)->getSensor()->stopSync();
    }
}

void MainWindow::recalibrate(void)
{
    if (currentLpms == 0 || isConnecting == true) {
        return;
    }

    if (auto error = currentLpms->getSensor().executeProperty(ZenImuProperty_CalibrateGyro))
    {
        std::cout << "Failed to trigger gyro calibration, due to error: " << std::to_string(error) << std::endl;
        return;
    }

    startWaitBar(30);
}

void MainWindow::uploadFirmware(void)
{
    if (currentLpms == 0 || isConnecting == true)
        return;

    QMessageBox msgBox;
    msgBox.setText("By uploading an invalid firmware file, the sensor can become "
        "in-operable. Are you sure that you understand what you are doing and "
        "would like to proceed?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::No)
        return;

    QString qfilename = QFileDialog::getOpenFileName(this, "Open firmware file", "./", "Binaries (*.bin)");
    if (qfilename.isEmpty())
        return;

    QFile file(qfilename);
    if (!file.open(QIODevice::ReadOnly))
        std::cout << "[MainWindow] Couldn't open firmware file." << std::endl;
    else if (file.size() < 10000 || file.size() > 100000)
    {
        file.close();
        std::cout << "[MainWindow] Bad firmware filesize: " << std::to_string(file.size()) << std::endl;
    }
    else
    {
        auto temp = file.readAll();
        file.close();

        const std::vector<unsigned char> firmware(temp.begin(), temp.end());
        if (currentLpms->getSensor().updateFirmwareAsync(firmware) != ZenAsync_Updating)
            return;

        uploadProgress = new QProgressDialog("Uploading data, please don't turn off your device...", "Cancel", 0, 200, this);
        uploadProgress->setWindowFlags(uploadProgress->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        uploadProgress->setWindowModality(Qt::WindowModal);
        uploadProgress->setMinimumWidth(400);
        uploadProgress->setAutoReset(false);
        uploadProgress->setCancelButton(nullptr);
        uploadProgress->show();

        uploadTimer = new QTimer(this);
        connect(uploadTimer, SIGNAL(timeout()), this, SLOT(uploadTimerUpdate()));

        m_uploadingFirmware = true;
        uploadTimer->start(500);
        return;
    }

    QMessageBox msgBox2;
    msgBox2.setText("Invalid firmware file. Please confirm that you selected the right file for upload.");
    msgBox2.setStandardButtons(QMessageBox::Ok);
    msgBox2.exec();
}

void MainWindow::uploadTimerUpdate()
{
    static int progress = 0;

    using namespace std::placeholders;
    std::function<ZenAsyncStatus(const std::vector<unsigned char>&)> uploadFn =
        std::bind(m_uploadingFirmware ? &zen::ZenSensor::updateFirmwareAsync : &zen::ZenSensor::updateIAPAsync, &currentLpms->getSensor(), _1);

    bool destroy = true;
    bool failed = true;
    if (auto status = uploadFn(std::vector<unsigned char>()))
    {
        if (status == ZenAsync_Updating)
        {
            uploadProgress->setValue(++progress);
            uploadProgress->show();
            destroy = false;
        }
        else
        {
            failed = true;
        }
    }
    else
    {
        failed = false;
    }

    if (destroy)
    {
        delete uploadTimer;
        delete uploadProgress;

        QMessageBox msgBox;
        msgBox.setText(QString(m_uploadingFirmware ? "Firmware" : "IAP") + " upload has " + (failed ? "failed" : "finished"));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void MainWindow::uploadIap(void)
{
    if (currentLpms == 0 || isConnecting == true)
        return;

    QMessageBox msgBox;
    msgBox.setText("By uploading an invalid IAP file, the sensor can become "
        "in-operable. Are you sure that you understand what you are doing and "
        "would like to proceed?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::No)
        return;

    QString qfilename = QFileDialog::getOpenFileName(this, "Open IAP file", "./", "Binaries (*.bin)");
    if (qfilename.isEmpty())
        return;

    QFile file(qfilename);
    if (file.open(QIODevice::ReadOnly))
    {
        auto temp = file.readAll();
        file.close();

        const auto iap = std::vector<unsigned char>(temp.begin(), temp.end());
        if (currentLpms->getSensor().updateIAPAsync(iap) != ZenAsync_Updating)
            return;

        uploadProgress = new QProgressDialog("Uploading data, please don't turn off your device...", "Cancel", 0, 200, this);
        uploadProgress->setWindowFlags(uploadProgress->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        uploadProgress->setWindowModality(Qt::WindowModal);
        uploadProgress->setMinimumWidth(400);
        uploadProgress->setAutoReset(false);
        uploadProgress->setCancelButton(nullptr);
        uploadProgress->show();

        uploadTimer = new QTimer(this);
        connect(uploadTimer, SIGNAL(timeout()), this, SLOT(uploadTimerUpdate()));

        m_uploadingFirmware = false;
        uploadTimer->start(500);
    }
    else
    {
        std::cout << "[MainWindow] Couldn't open IAP file." << std::endl;

        QMessageBox msgBox2;
        msgBox2.setText("Invalid IAP file. Please confirm that you selected the right file for upload.");
        msgBox2.setStandardButtons(QMessageBox::Ok);
        msgBox2.exec();
    }
}

void MainWindow::toggleRecordData(void)
{
    if (currentLpms == 0 || isConnecting == true) {
        return;
    }

    if (!recordFileSet)
    {
        browseRecordFile();
        if (!recordFileSet)
            return;
    }

    if (!m_recording)
    {
        if (globalRecordFile.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(m_recordingMutex);
            m_recordingStream.open(globalRecordFile, std::ios_base::out);
            m_recordingStream.rdbuf()->pubsetbuf(m_recordingStreamBuffer, 65536);
            if (!m_recordingStream.is_open())
                return;

            m_recordingStream << "SensorId, TimeStamp (s), FrameNumber, AccX (g), AccY (g), AccZ (g), GyroX (deg/s), GyroY (deg/s), GyroZ (deg/s), MagX (uT), MagY (uT), MagZ (uT), EulerX (deg), EulerY (deg), EulerZ (deg), QuatW, QuatX, QuatY, QuatZ, LinAccX (g), LinAccY (g), LinAccZ (g), Pressure (kPa), Altitude (m), Temperature (degC), HeaveMotion (m)" << std::endl;
        }

        stopMeasurement();

        saveAction->setText("Stop recording");
        saveAction->setIcon(QIcon(":/icons/x_alt_32x32.png"));
        m_firstRecord = true;
        m_recording = true;

        startMeasurement();
    }
    else
    {
        m_recording = false;

        {
            std::lock_guard<std::mutex> lock(m_recordingMutex);
            m_recordingStream.close();
        }
        saveAction->setText("Record data");
        saveAction->setIcon(QIcon(":/icons/layers_32x28.png"));
    }
}

void MainWindow::browseRecordFile(void)
{
    QString qFilename = QFileDialog::getSaveFileName(this, "Save sensor data", "./", "*.csv");

    if (!qFilename.isEmpty())
    {
        if (!qFilename.endsWith(".csv"))
            qFilename += ".csv";

        globalRecordFile = qFilename.toStdString();
        recordFileEdit->setText(globalRecordFile.c_str());
        recordFileSet = true;
    }
}

void MainWindow::saveCalibration(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    if (auto error = currentLpms->getSensor().executeProperty(ZenSensorProperty_StoreSettingsInFlash))
        std::cout << "Failed to store settings in flash, due to error: " << std::to_string(error) << std::endl;
}

void MainWindow::addRemoveDevices(void)
{
    stopMeasurement();

    rescanD->show();
}

void MainWindow::loadCalibrationData(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    QString qfn = QFileDialog::getOpenFileName(this, "Load calibration data", "./", "");
    std::string fn = qfn.toStdString();

    if (fn.empty())
        return;

    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_file(fn.c_str());

    if (!result)
    {
        std::cout << "Could not open configuration file." << std::endl;
        return;
    }

    const pugi::xml_node config = doc.child("LpmsControlConfiguration");
    if (!config)
    {
        std::cout << "Configuration file is corrupt." << std::endl;
        return;
    }

    auto optFieldRadius = deserialize<float>(config, "FieldEstimate");
    auto optHardIronOffset = deserialize<LpVector3f>(config, "HardIronOffset");
    auto optSoftIronMatrix = deserialize<LpMatrix3x3f>(config, "SoftIronMatrix");
    auto optAccAlignMatrix = deserialize<LpMatrix3x3f>(config, "MisalignmentMatrix");
    auto optAccAlignBias = deserialize<LpVector3f>(config, "AccelerometerBias");
    auto optGyrAlignMatrix = deserialize<LpMatrix3x3f>(config, "GyroMisalignmentMatrix");
    auto optGyrAlignBias = deserialize<LpVector3f>(config, "GyroMisalignmentBias");

    if (!optFieldRadius || !optHardIronOffset || !optSoftIronMatrix || !optAccAlignMatrix || !optAccAlignBias || !optGyrAlignMatrix || !optGyrAlignBias)
    {
        std::cout << "Configuration file is corrupt." << std::endl;
        return;
    }

    auto imu = currentLpms->getImuComponent();
    imu.setFloatProperty(ZenImuProperty_FieldRadius, *optFieldRadius);
    imu.setArrayProperty(ZenImuProperty_MagHardIronOffset, optHardIronOffset->data, 3);
    imu.setArrayProperty(ZenImuProperty_MagSoftIronMatrix, optSoftIronMatrix->data[0], 9);
    imu.setArrayProperty(ZenImuProperty_AccAlignment, optAccAlignMatrix->data[0], 9);
    imu.setArrayProperty(ZenImuProperty_AccBias, optAccAlignBias->data, 3);
    imu.setArrayProperty(ZenImuProperty_GyrAlignment, optGyrAlignMatrix->data[0], 9);
    imu.setArrayProperty(ZenImuProperty_GyrBias, optGyrAlignBias->data, 3);
}

void MainWindow::saveCalibrationData(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    const std::string sensorName = currentLpms->getSensorName();
    QString qfn = QFileDialog::getSaveFileName(this, "Save calibration data", QString::fromStdString(sensorName)+".xml", "");
    std::string fn = qfn.toStdString();

    if (fn.empty())
        return;

    pugi::xml_document doc;
    pugi::xml_node configuration = doc.append_child("LpmsControlConfiguration");

    auto imu = currentLpms->getImuComponent();

    auto [error, fieldRadius] = imu.getFloatProperty(ZenImuProperty_FieldRadius);
    if (error)
        return;

    float hardIronOffset[3];
    const auto result = imu.getArrayProperty(ZenImuProperty_MagHardIronOffset, hardIronOffset, 3);
    if (result.first)
        return;

    LpMatrix3x3f softIronMatrix;
    const auto result2 = imu.getArrayProperty(ZenImuProperty_MagSoftIronMatrix, softIronMatrix.data[0], 9);
    if (result2.first)
        return;

    LpMatrix3x3f accAlignmentMatrix;
    const auto result3 = imu.getArrayProperty(ZenImuProperty_AccAlignment, accAlignmentMatrix.data[0], 9);
    if (result3.first)
        return;

    float accAlignmentBias[3];
    const auto result4 = imu.getArrayProperty(ZenImuProperty_AccBias, accAlignmentBias, 3);
    if (result4.first)
        return;

    LpMatrix3x3f gyrAlignmentMatrix;
    const auto result5 = imu.getArrayProperty(ZenImuProperty_GyrAlignment, gyrAlignmentMatrix.data[0], 9);
    if (result5.first)
        return;

    float gyrAlignmentBias[3];
    const auto result6 = imu.getArrayProperty(ZenImuProperty_GyrBias, gyrAlignmentBias, 3);
    if (result6.first)
        return;

    serialize(configuration, "FieldEstimate", fieldRadius);

    LpVector3f temp;
    convertArrayToLpVector3f(hardIronOffset, &temp);
    serialize(configuration, "HardIronOffset", temp);
    serialize(configuration, "SoftIronMatrix", softIronMatrix);
    serialize(configuration, "MisalignmentMatrix", accAlignmentMatrix);   // Accelerometer

    convertArrayToLpVector3f(accAlignmentBias, &temp);
    serialize(configuration, "AccelerometerBias", temp);
    serialize(configuration, "GyroMisalignmentMatrix", gyrAlignmentMatrix);

    convertArrayToLpVector3f(gyrAlignmentBias, &temp);
    serialize(configuration, "GyroMisalignmentBias", temp);

    if (!doc.save_file(fn.c_str()))
        std::cout << "Error writing configuration file " << fn << std::endl;
}

void MainWindow::measureLatency(void)
{
    if (currentLpms == 0 || isConnecting == true) return;

    const auto start = std::chrono::high_resolution_clock::now();
    currentLpms->getSensor().getInt32Property(ZenSensorProperty_TimeOffset);
    const auto latencyInNs = std::chrono::high_resolution_clock::now() - start;
}

void MainWindow::getVersionInfo(void)
{
    QString openMATVersion(LPMS_CONTROL_VERSION);
    QMessageBox::about(this, "LP-RESEARCH - LpmsControl", QString("LP-RESEARCH - LpmsControl ")
        + openMATVersion
        + QString("\n(c) LP-Research\nhttp://www.lp-research.com"));
}

void MainWindow::resetToFactory(void)
{
    QMessageBox msgBox;
    int ret;

    std::list<SensorGuiContainerBase *>::iterator it;

    if (currentLpms == 0 || isConnecting == true) {
        return;
    }

    msgBox.setText("This command sets the current sensor parameter "
        "to their factory defaults. Would you like to proceed?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
        break;

    case QMessageBox::No:
        return;
        break;

    default:
        break;
    }

    if (auto error = currentLpms->getSensor().executeProperty(ZenSensorProperty_RestoreFactorySettings))
        std::cout << "Failed to reset factory settings." << std::endl;
}



void MainWindow::startWaitBar(int t)
{
    if (m_accAlignCalibrating || m_gyrAlignCalibrating || m_magAlignCalibrating)
        maWizard->hide();

    calProgress = new QProgressDialog("Calibrating LPMS. Please wait..", QString() /* "Cancel" */, 0, t, this);
    calProgress->setWindowFlags(calProgress->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    calProgress->setWindowModality(Qt::WindowModal);
    calProgress->setMinimumWidth(400);
    calProgress->setAutoReset(false);
    calProgress->show();

    calTimer = new QTimer(this);
    connect(calTimer, SIGNAL(timeout()), this, SLOT(calTimerUpdate()));
    calTimer->start(500);

    calMaxTime = t;
    calTime = 0;
}

void MainWindow::stopWaitBar()
{
    delete calProgress;
    delete calTimer;

    if (m_accAlignCalibrating)
    {
        m_accAlignInstructing = true;

        if (maWizard->currentId() == 6)
            maFinished();

        maWizard->show();
    }
    else if (m_gyrAlignCalibrating)
    {
        m_gyrAlignInstructing = true;

        if (maWizard->currentId() == 3)
            gyrMaFinished();

        maWizard->show();
    }
    else if (m_magAlignCalibrating)
    {
        m_magAlignInstructing = true;

        if (maWizard->currentId() == 12)
            magMaFinished();

        maWizard->show();
    }
    else if (m_magCalibrating)
    {
        m_magInstructing = true;

        if (magEllipsoidCalWizard->currentId() == 1)
        {
            if (auto error = prepareForMagnetometerReferenceCalibration(currentLpms->getImuComponent()))
            {
                m_magRefHandle = lpCreateMagRefCalibrator();
                m_magRefInstructing = true;
                m_magRefCalibrating = true;
            }

            onMagCaliFinished();
        }

        magEllipsoidCalWizard->show();
    }
    else if (m_magRefCalibrating)
    {
        m_magRefInstructing = true;

        if (magEllipsoidCalWizard->currentId() == 2)
            onMagRefCaliFinished();

        magEllipsoidCalWizard->show();
    }
}

void MainWindow::calTimerUpdate(void)
{
    ++calTime;

    if (calTime > calMaxTime)
        stopWaitBar();
    else
        calProgress->setValue(calTime);
}

void MainWindow::magMisalignmentCal(void)
{
    QMessageBox msgBox;
    int ret;

    if (currentLpms == 0 || isConnecting == true) return;

    msgBox.setText("Changing the magnetometer misalignment matrix can significantly "
        "affect the performance of your sensor. Before setting a new "
        "misalignment matrix, please make sure that you know what you "
        "are doing. Anyway, parameters will not be saved to the internal "
        "flash memory of the sensor until you choose to save calibration "
        "parameters. Also you can backup your current parameters by"
        "saving them to a file (see the Calibration menu).");

    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
        break;

    case QMessageBox::No:
        return;
        break;

    default:
        break;
    }

    if (auto error = prepareForMagnetometerAlignmentCalibration(currentLpms->getImuComponent()))
        return;

    m_magAlignHandle = lpCreateMagAlignCalibrator();
    m_magAlignStage = lpMagAlignCalibrationStage(m_magAlignHandle);

    maWizard = new QWizard(this);
    QList<QWizard::WizardButton> layout;
    layout << QWizard::Stretch << QWizard::NextButton << QWizard::CancelButton << QWizard::FinishButton;
    maWizard->setButtonLayout(layout);

    maWizard->addPage(maOrientationPage("Z-AXIS UP - Coils OFF", "Z-AXIS facing UPWARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("Z-AXIS UP - Coils ON", "Z-AXIS facing UPWARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(maOrientationPage("Z-AXIS DOWN - Coils OFF", "Z-AXIS facing DOWNARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("Z-AXIS DOWN - Coils ON", "Z-AXIS facing DOWNARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(maOrientationPage("X-AXIS UP - Coils OFF", "X-AXIS facing UPWARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("X-AXIS UP - Coils ON", "X-AXIS facing UPWARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(maOrientationPage("X-AXIS DOWN - Coils OFF", "X-AXIS facing DOWNARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("X-AXIS DOWN - Coils ON", "X-AXIS facing DOWNARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(maOrientationPage("Y-AXIS UP - Coils OFF", "Y-AXIS facing UPWARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("Y-AXIS UP - Coils ON", "Y-AXIS facing UPWARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(maOrientationPage("Y-AXIS DOWN - Coils OFF", "Y-AXIS facing DOWNWARDS. Switch Helmholtz coils OFF. "));
    maWizard->addPage(maOrientationPage("Y-AXIS DOWN - Coils ON", "Y-AXIS facing DOWNWARDS. Switch Helmholtz coils ON. "));

    maWizard->addPage(gyrMaFinishedPage());

    maWizard->setWindowTitle("Magnetometer Misalignment Calibration");

    m_magAlignInstructing = true;
    maWizard->show();

    connect(maWizard, SIGNAL(currentIdChanged(int)), this, SLOT(magMaNewPage(int)));

    m_magAlignCalibrating = true;
}

void MainWindow::magMaNewPage(int i)
{
    m_magAlignInstructing = false;
    startWaitBar(10);
}

void MainWindow::magMaFinished()
{
    if (lpMagAlignCalibrationStage(m_magAlignHandle) == LpMagAlignCali_Finished)
    {
        LpMagAlignCalibration cali;
        if (lpMagAlignCalibration(m_magAlignHandle, &cali) == LpCaliError_None)
        {
            auto imu = currentLpms->getImuComponent();

            imu.setArrayProperty(ZenImuProperty_AccAlignment, cali.alignmentMatrix.data[0], 9);
            imu.setArrayProperty(ZenImuProperty_AccBias, cali.accBias.data, 3);
        }
    }

    lpDestroyMagAlignCalibrator(m_magAlignHandle);
    m_magAlignCalibrating = false;
}

QWizardPage *MainWindow::magMaOrientationPage(const char* ts, const char* es)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle((std::string("Calibrating to orientation: ") + std::string(ts)).c_str());

    QLabel *label = new QLabel((std::string("Please put the selected LPMS on a horizontal surface, with the ") + std::string(es) + std::string("After aligning the sensor in this orientation press the next button. Please do not move the sensor for around 3s.")).c_str());
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

QWizardPage *MainWindow::magMaFinishedPage(void)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle("Calibration finished");

    QLabel *label = new QLabel("Misalignment calibration has been finished successfully.");
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

void MainWindow::gyrMisalignmentCal(void)
{
    QMessageBox msgBox;
    int ret;

    if (currentLpms == 0 || isConnecting == true) return;

    msgBox.setText("Changing the gyroscope misalignment matrix can significantly "
        "affect the performance of your sensor. Before setting a new "
        "misalignment matrix, please make sure that you know what you "
        "are doing. Anyway, parameters will not be saved to the internal "
        "flash memory of the sensor until you choose to save calibration "
        "parameters. Also you can backup your current parameters by"
        "saving them to a file (see the Calibration menu).");

    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
        break;

    case QMessageBox::No:
        return;
        break;

    default:
        break;
    }

    if (auto error = prepareForGyroAlignmentCalibration(currentLpms->getImuComponent()))
        return;

    m_gyrAlignHandle = lpCreateGyrAlignCalibrator();
    m_gyrAlignStage = lpGyrAlignCalibrationStage(m_gyrAlignHandle);

    maWizard = new QWizard(this);
    QList<QWizard::WizardButton> layout;
    layout << QWizard::Stretch << QWizard::NextButton << QWizard::CancelButton << QWizard::FinishButton;
    maWizard->setButtonLayout(layout);

    maWizard->addPage(gyrMaOrientationPage("X-AXIS", "right hand"));
    maWizard->addPage(gyrMaOrientationPage("Y-AXIS", "left hand"));
    maWizard->addPage(gyrMaOrientationPage("Z-AXIS", "right hand"));

    maWizard->addPage(gyrMaFinishedPage());

    maWizard->setWindowTitle("Gyroscope Misalignment Calibration");

    m_gyrAlignInstructing = true;
    maWizard->show();

    connect(maWizard, SIGNAL(currentIdChanged(int)), this, SLOT(gyrMaNewPage(int)));

    m_gyrAlignCalibrating = true;
}

void MainWindow::gyrMaNewPage(int i)
{
    m_gyrAlignInstructing = false;
    startWaitBar(10);
}

void MainWindow::gyrMaFinished()
{
    if (lpGyrAlignCalibrationStage(m_gyrAlignHandle) == LpGyrAlignCali_Finished)
    {
        LpGyrAlignCalibration cali;
        if (lpGyrAlignCalibration(m_gyrAlignHandle, &cali) == LpCaliError_None)
        {
            auto imu = currentLpms->getImuComponent();

            imu.setArrayProperty(ZenImuProperty_GyrAlignment, cali.alignmentMatrix.data[0], 9);
            imu.setArrayProperty(ZenImuProperty_GyrBias, cali.alignmentBias.data, 3);
        }
    }

    lpDestroyGyrAlignCalibrator(m_gyrAlignHandle);
    m_gyrAlignCalibrating = false;
}

QWizardPage *MainWindow::gyrMaOrientationPage(const char* ts, const char* es)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle((std::string("Rotation axis: ") + std::string(ts)).c_str());

    QLabel *label = new QLabel((std::string("Please LEFT-HAND rotate the sensor at a constant angular rate of 45 rpm, with its ") + std::string(ts) + std::string(" pointing UP.")).c_str());

    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

QWizardPage *MainWindow::gyrMaFinishedPage(void)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle("Calibration finished");

    QLabel *label = new QLabel("Misalignment calibration has been finished successfully.");
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

QWizardPage *MainWindow::magEllipsoidCalPage(int pageNumber)
{
    QWizardPage *page = new QWizardPage;
    QLabel *label;

    page->setTitle(std::string("Magnetic field map").c_str());

    switch (pageNumber) {
    case 0:
        page->setTitle(std::string("Magnetic field map").c_str());
        label = new QLabel((std::string("Please continuously rotate the selected LPMS around roll, pitch and yaw axis for 30s. The calibration algorithm will create a map of your environment magnetic field and use it to calculate hard / soft iron calibration parameters.")).c_str());
        break;

    case 1:
        page->setTitle(std::string("Alignment").c_str());
        label = new QLabel((std::string("Please hold the sensor in a motionless state for 10s. The alignment of the magnetometer inclination axis and the vertical axis (gravity vector) will be calculated.")).c_str());
        break;

    case 2:
        page->setTitle(std::string("Calibration finished").c_str());
        label = new QLabel((std::string("Magnetometer calibration has been finished. Please check the field map window if the number of acquired field points during the calibration process were enough to generate a good ellipsoid fit. See the manual for more information.")).c_str());
        break;
    }

    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

void MainWindow::magEllipsoidCalNewPage(int i)
{
    magEllipsoidCalWizard->hide();

    switch (i) {
    case 1:
        m_magInstructing = false;
        startWaitBar(45);
        break;

    case 2:
        m_magRefInstructing = false;
        startWaitBar(10);
        break;
    }
}

void MainWindow::calibrateMag(void)
{
    QList<QWizard::WizardButton> layout;

    if (currentLpms == 0 || isConnecting == true) return;

    if (auto error = prepareForMagnetometerCalibration(currentLpms->getImuComponent()))
        return;

    m_magHandle = lpCreateMagCalibrator();

    magEllipsoidCalWizard = new QWizard(this);

    layout << QWizard::Stretch << QWizard::NextButton << QWizard::CancelButton << QWizard::FinishButton;
    magEllipsoidCalWizard->setButtonLayout(layout);

    magEllipsoidCalWizard->addPage(magEllipsoidCalPage(0));
    magEllipsoidCalWizard->addPage(magEllipsoidCalPage(1));
    magEllipsoidCalWizard->addPage(magEllipsoidCalPage(2));

    magEllipsoidCalWizard->setWindowTitle("Magnetometer hard / soft iron calibration (ellipsoid)");
    connect(magEllipsoidCalWizard, SIGNAL(currentIdChanged(int)), this, SLOT(magEllipsoidCalNewPage(int)));

    m_magInstructing = true;
    magEllipsoidCalWizard->show();
    m_magCalibrating = true;
}

void MainWindow::onMagCaliFinished()
{
    if (lpIsMagCalibrationFinished(m_magHandle))
    {
        LpMagCalibration cali;
        if (lpMagCalibration(m_magHandle, &cali) == LpCaliError_None)
        {
            auto imu = currentLpms->getImuComponent();

            imu.setArrayProperty(ZenImuProperty_MagSoftIronMatrix, cali.softIronMatrix.data[0], 9);
            imu.setArrayProperty(ZenImuProperty_MagHardIronOffset, cali.hardIronOffset.data, 3);
            imu.setFloatProperty(ZenImuProperty_FieldRadius, cali.fieldRadius);

            currentLpms->setLastFieldMap(cali.fieldMap);
        }
    }

    lpDestroyMagCalibrator(m_magHandle);
    m_magCalibrating = false;
}

void MainWindow::onMagRefCaliFinished()
{
    if (lpIsMagRefCalibrationFinished(m_magRefHandle))
    {
        LpMagRefCalibration cali;
        if (lpMagRefCalibration(m_magRefHandle, &cali) == LpCaliError_None)
            currentLpms->getImuComponent().setArrayProperty(ZenImuProperty_MagReference, cali.reference.data, 3);
    }

    lpDestroyMagRefCalibrator(m_magRefHandle);
    m_magRefCalibrating = false;
}

QWizardPage *MainWindow::maOrientationPage(const char* ts, const char* es)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle((std::string("Calibrating to orientation: ") + std::string(ts)).c_str());

    QLabel *label = new QLabel((std::string("Please put the selected LPMS on a horizontal surface, with the ") + std::string(es) + std::string("After aligning the sensor in this orientation press the next button. Please do not move the sensor for around 3s.")).c_str());
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

QWizardPage *MainWindow::maFinishedPage(void)
{
    QWizardPage *page = new QWizardPage;

    page->setTitle("Calibration finished");

    QLabel *label = new QLabel("Misalignment calibration has been finished successfully.");
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    page->setLayout(layout);

    return page;
}

void MainWindow::misalignmentCal(void)
{
    QMessageBox msgBox;
    int ret;

    if (currentLpms == 0 || isConnecting == true) return;

    msgBox.setText("Changing the misalignment matrix can significantly "
        "affect the performance of your sensor. Before setting a new "
        "misalignment matrix, please make sure that you know what you "
        "are doing. Anyway, parameters will not be saved to the internal "
        "flash memory of the sensor until you choose to save calibration "
        "parameters. Also you can backup your current parameters by"
        "saving them to a file (see the Calibration menu).");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    msgBox.setDefaultButton(QMessageBox::No);
    ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
        break;

    case QMessageBox::No:
        return;
        break;

    default:
        break;
    }

    // Make sure we receive data that is necessary for calibration
    if (auto error = prepareForAccelerometerAlignmentCalibration(currentLpms->getImuComponent()))
        return;

    m_accAlignHandle = lpCreateAccAlignCalibrator();
    m_accAlignStage = lpAccAlignCalibrationStage(m_accAlignHandle);

    maWizard = new QWizard(this);
    QList<QWizard::WizardButton> layout;
    layout << QWizard::Stretch << QWizard::NextButton << QWizard::CancelButton << QWizard::FinishButton;
    maWizard->setButtonLayout(layout);

    maWizard->addPage(maOrientationPage("Z-AXIS UP", "Z-AXIS facing UPWARDS. "));
    maWizard->addPage(maOrientationPage("Z-AXIS DOWN", "Z-AXIS facing DOWNARDS. "));

    maWizard->addPage(maOrientationPage("X-AXIS UP", "X-AXIS facing UPWARDS. "));
    maWizard->addPage(maOrientationPage("X-AXIS DOWN", "X-AXIS facing DOWNARDS. "));

    maWizard->addPage(maOrientationPage("Y-AXIS UP", "Y-AXIS facing UPWARDS. "));
    maWizard->addPage(maOrientationPage("Y-AXIS DOWN", "Y-AXIS facing DOWNARDS. "));

    maWizard->addPage(maFinishedPage());

    maWizard->setWindowTitle("Accelerometer Misalignment Calibration");

    m_accAlignInstructing = true;
    maWizard->show();

    connect(maWizard, SIGNAL(currentIdChanged(int)), this, SLOT(maNewPage(int)));

    m_accAlignCalibrating = true;
}

void MainWindow::maNewPage(int i)
{
    m_accAlignInstructing = false;
    startWaitBar(5);
}

void MainWindow::maFinished()
{
    if (lpAccAlignCalibrationStage(m_accAlignHandle) == LpAccAlignCali_Finished)
    {
        LpAccAlignCalibration cali;
        if (lpAccAlignCalibration(m_accAlignHandle, &cali) == LpCaliError_None)
        {
            auto imu= currentLpms->getImuComponent();

            imu.setArrayProperty(ZenImuProperty_AccAlignment, cali.alignmentMatrix.data[0], 9);
            imu.setArrayProperty(ZenImuProperty_AccBias, cali.accBias.data, 3);
        }
    }

    lpDestroyAccAlignCalibrator(m_accAlignHandle);
    m_accAlignCalibrating = false;
}

void MainWindow::updateMagneticFieldMap(const ZenImuData& imuData)
{
    if (!currentLpms->hasFieldMap())
        return;

    auto imu = currentLpms->getImuComponent();
    auto[error, fieldRadius] = imu.getFloatProperty(ZenImuProperty_FieldRadius);
    if (error)
        return;

    float hardIronOffset[3];
    const auto result = imu.getArrayProperty(ZenImuProperty_MagHardIronOffset, hardIronOffset, 3);
    if (result.first)
        return;

    LpMatrix3x3f softIronMatrix;
    const auto result2 = imu.getArrayProperty(ZenImuProperty_MagSoftIronMatrix, softIronMatrix.data[0], 9);
    if (result2.first)
        return;

    float fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW][3];
    currentLpms->lastFieldMap(fieldMap);

    fieldMapWindow->updateFieldMap(fieldMap, hardIronOffset, softIronMatrix.data[0], fieldRadius, imuData.b);
}

void MainWindow::updateCurrentField(const ZenImuData& imuData)
{
    auto [error, fieldRadius] = currentLpms->getImuComponent().getFloatProperty(ZenImuProperty_FieldRadius);
    if (error)
        return;

    const float fieldNoise = std::abs(fieldRadius - std::sqrt(imuData.b[0] * imuData.b[0] + imuData.b[1] * imuData.b[1] + imuData.b[2] * imuData.b[2]));

    fieldMapWindow->updateCurrentField(fieldNoise, imuData.b);
}

void MainWindow::startReplay(void)
{
    if (!playbackFileSet)
    {
        browsePlaybackFile();
        if (!playbackFileSet)
            return;
    }

    if (!rePlayer->isPlaying())
    {
        if (globalPlaybackFile.empty())
            return;

        stopMeasurement();

        graphWindow->clearGraphs();

        rePlayer->readMotionDataFile(globalPlaybackFile);

        replayAction->setText("Stop playback");
        replayAction->setIcon(QIcon(":/icons/stop_32x32.png"));
    }
    else
        stopReplay();
}

void MainWindow::stopReplay(void)
{
    rePlayer->pause();
    playbackFinished();
}

void MainWindow::browsePlaybackFile(void)
{
    QString qFilename = QFileDialog::getOpenFileName(this, "Load sensor data", "./", "*.csv");

    if (!qFilename.isEmpty())
    {
        globalPlaybackFile = qFilename.toStdString();
        playbackFileEdit->setText(globalPlaybackFile.c_str());
        playbackFileSet = true;
    }
}

void MainWindow::loadObjFile(void)
{
    QString qFilename = QFileDialog::getOpenFileName(this, "Load 3D OBJ file", "./", "OBJ files (*.obj)");
    std::string objFilename = qFilename.toStdString();

    if (!(objFilename == "")) {
        cubeWindowContainer->getSelectedCube()->loadObjFile(objFilename);
    }
}


void MainWindow::xAxisAutoScrolling(void)
{
    if (xAxisAutoScrollingAction->isChecked())
    {
        graphWindow->setXAxisAutoScrolling(true);
        xAxisAutoScrollingAction->setChecked(true);
    }
    else
    {
        graphWindow->setXAxisAutoScrolling(false);
        xAxisAutoScrollingAction->setChecked(false);
    }
}

void MainWindow::yAxisAutoScaling(void)
{
    if (yAxisAutoScalingAction->isChecked())
    {
        graphWindow->setYAxisAutoScaling(true);
        yAxisAutoScalingAction->setChecked(true);
    }
    else
    {
        graphWindow->setYAxisAutoScaling(false);
        yAxisAutoScalingAction->setChecked(false);
    }
}


void MainWindow::startFlashLog(void)
{
    //currentLpms->getSensor()->startFlashLogging();
}

void MainWindow::stopFlashLog(void)
{
    //currentLpms->getSensor()->stopFlashLogging();

}

void MainWindow::clearFlashLog(void)
{
    //currentLpms->getSensor()->clearFlashLog();
}

void MainWindow::eraseFlash(void)
{
    //currentLpms->getSensor()->fullEraseFlash();
}

void MainWindow::saveFlashLog(void)
{
    //if (currentLpms == 0 || isConnecting == true) {
    //    return;
    //}

    //if (currentLpms->getSensor()->isDownloadingFlashLog())
    //    return;

    //QString qFilename = QFileDialog::getSaveFileName(this, "Save sensor data", "./", "*.csv");
    //std::string recordFilename = qFilename.toStdString();
    //if (recordFilename == "")
    //    return;

    //if (!qFilename.endsWith(".csv"))
    //    recordFilename += ".csv";

    //currentLpms->getSensor()->getFlashLog(recordFilename.c_str());
    //
    //uploadProgress = new QProgressDialog("Saving sensor flash data, please don't turn off your device...", "Cancel", 0, 200, this);
    //uploadProgress->setWindowFlags(uploadProgress->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    //uploadProgress->setWindowModality(Qt::WindowModal);
    //uploadProgress->setMinimumWidth(400);
    //uploadProgress->setAutoReset(false);
    //uploadProgress->setCancelButton(0);
    //uploadProgress->show();

    //uploadTimer = new QTimer(this);
    //connect(uploadTimer, SIGNAL(timeout()), this, SLOT(downloadFlashLogTimerUpdate()));
    //connect(uploadProgress, SIGNAL(canceled()), this, SLOT(cancelDownloadFlashLog()));
    //uploadTimer->start(500);
}


void MainWindow::downloadFlashLogTimerUpdate(void)
{
    //int p;

    //if (currentLpms->getSensor()->getDownloadFlashLogProgress(&p) == 1) {
    //    uploadProgress->setValue(p);
    //    uploadProgress->show();
    //}
    //else {
    //    QMessageBox msgBox;

    //    delete uploadTimer;
    //    delete uploadProgress;

    //    msgBox.setText("Flash log download finished.");
    //    msgBox.setStandardButtons(QMessageBox::Ok);
    //    msgBox.exec();
    //}
}

void MainWindow::cancelDownloadFlashLog()
{
    //currentLpms->getSensor()->cancelGetFlashLog();
    //QMessageBox msgBox;

    //delete uploadTimer;
    //delete uploadProgress;

    //msgBox.setText("Flash log download canceled.");
    //msgBox.setStandardButtons(QMessageBox::Ok);
    //msgBox.exec();
}

void MainWindow::enableFlashMenu()
{
    //startFlashLogAction->setEnabled(true);
    //stopFlashLogAction->setEnabled(true);
    //clearFlashLogAction->setEnabled(true);
    //eraseFlashAction->setEnabled(true);
    //saveFlashLogAction->setEnabled(true);
}

void MainWindow::disableFlashMenu()
{
    //startFlashLogAction->setEnabled(false);
    //stopFlashLogAction->setEnabled(false);
    //clearFlashLogAction->setEnabled(false);
    //eraseFlashAction->setEnabled(false);
    //saveFlashLogAction->setEnabled(false);
}
