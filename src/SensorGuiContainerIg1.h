#ifndef SENSOR_GUI_CONTAINER_IG1
#define SENSOR_GUI_CONTAINER_IG1 

#include <QTreeWidgetItem>
#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>

#include "LpMagnetometerCalibration.h"
#include "OpenZen.h"
#include "SensorGuiContainerBase.h"

// Container for GUI elements related to one sensor unit.
// Its contents are used to build the sensor list tree in the control window.
class SensorGuiContainerIg1 : public SensorGuiContainerBase
{
Q_OBJECT

public:
    QLabel* addressItem;
    QLabel* statusItem;
    QLabel* indexItem;
    QLabel* runningItem;
    QLabel* firmwareItem;
  
    QComboBox* samplingRateCombo;

    QComboBox* gyrRangeCombo;
    QComboBox* accRangeCombo;
    QComboBox* magRangeCombo;
    QComboBox* degradOutputCombo;

    // CAN
    QComboBox* canBaudrateCombo;
    QComboBox* canChannelModeCombo;
    QComboBox* canDataPrecisionCombo;
    QSpinBox*  canStartIdSpin;
    QComboBox* canHeartbeatCombo;

    QComboBox* canTpdoCombo[16];

    QComboBox* canTpdo1ACombo;
    QComboBox* canTpdo1BCombo;
    QComboBox* canTpdo2ACombo;
    QComboBox* canTpdo2BCombo;
    QComboBox* canTpdo3ACombo;
    QComboBox* canTpdo3BCombo;
    QComboBox* canTpdo4ACombo;
    QComboBox* canTpdo4BCombo;
    QComboBox* canTpdo5ACombo;
    QComboBox* canTpdo5BCombo;
    QComboBox* canTpdo6ACombo;
    QComboBox* canTpdo6BCombo;
    QComboBox* canTpdo7ACombo;
    QComboBox* canTpdo7BCombo;
    QComboBox* canTpdo8ACombo;
    QComboBox* canTpdo8BCombo;

    // Filter
    QComboBox* filterModeCombo;
    QComboBox* gyroAutoCalibrationCombo;
    QComboBox* offsetModeCombo;
    QPushButton* btnResetOffset;

    // GPS data
    QPushButton* saveGpsState;
    QPushButton* clearGpsState;

    QCheckBox* selectGpsTimestamp;
    QCheckBox* selectPVT_iTOW;
    QCheckBox* selectPVT_YMD;
    QCheckBox* selectPVT_HMS;
    QCheckBox* selectPVT_vaild;
    QCheckBox* selectPVT_tAcc;
    QCheckBox* selectPVT_nano;
    QCheckBox* selectPVT_fixType;
    QCheckBox* selectPVT_flags;
    QCheckBox* selectPVT_flags2;
    QCheckBox* selectPVT_longitude;
    QCheckBox* selectPVT_latitude;
    QCheckBox* selectPVT_numSV;
    QCheckBox* selectPVT_height;
    QCheckBox* selectPVT_hMSL;
    QCheckBox* selectPVT_hAcc;
    QCheckBox* selectPVT_vAcc;
    QCheckBox* selectPVT_velN;
    QCheckBox* selectPVT_velE;
    QCheckBox* selectPVT_velD;
    QCheckBox* selectPVT_gSpeed;
    QCheckBox* selectPVT_headMot;
    QCheckBox* selectPVT_sAcc;
    QCheckBox* selectPVT_headAcc;
    QCheckBox* selectPVT_pDOP;
    QCheckBox* selectPVT_headVeh;
    
    QCheckBox* selectATT_ITOW;
    QCheckBox* selectATT_version;
    QCheckBox* selectATT_roll;
    QCheckBox* selectATT_pitch;
    QCheckBox* selectATT_heading;
    QCheckBox* selectATT_accRoll;
    QCheckBox* selectATT_accPitch;
    QCheckBox* selectATT_accHeading;

    QCheckBox* selectESF_iTOW;
    QCheckBox* selectESF_version;
    QCheckBox* selectESF_initStatus1;
    QCheckBox* selectESF_initStatus2;
    QCheckBox* selectESF_fusionMode;
    QCheckBox* selectESF_numnSens;
    QCheckBox* selectESF_sensStatus;
    QCheckBox* selectGPS_udrStatus;
    QPushButton* btnSaveSelectGpsData;
    
    // Uart
    QComboBox* baudRateCombo;
    QComboBox* uartFormatCombo;
    QComboBox* uartDataPrecision;

    // Imu data
    QCheckBox* selectTimestamp;
    QCheckBox* selectAccRaw;
    QCheckBox* selectAccCalibrated;
    QCheckBox* selectGyr0Raw;
    QCheckBox* selectGyr1Raw;
    QCheckBox* selectGyr0BiasCalibrated;
    QCheckBox* selectGyr1BiasCalibrated;
    QCheckBox* selectGyr0AlignmentCalibrated;
    QCheckBox* selectGyr1AlignmentCalibrated;
    QCheckBox* selectMagRaw;
    QCheckBox* selectMagCalibrated;
    QCheckBox* selectAngularVelocity;
    QCheckBox* selectQuaternion;
    QCheckBox* selectEuler;
    QCheckBox* selectLinAcc;
    QCheckBox* selectPressure;
    QCheckBox* selectAltitude;
    QCheckBox* selectTemperature;
    QPushButton* btnSaveSelectImuData;

    SensorGuiContainerIg1(  
        int openMatId, 
        zen::ZenSensor sensor, 
        zen::ZenSensorComponent imu, 
        const ZenSensorDesc& sensorDesc,
        std::string modelName,
        QTreeWidget* tree);

    zen::ZenSensor& getSensor() { return m_sensor; }
    const zen::ZenSensor& getSensor() const { return m_sensor; }
    std::string getSensorName() const { return m_sensorDesc.name; }

    zen::ZenSensorComponent getImuComponent() { return m_imu; }
    const zen::ZenSensorComponent& getImuComponent() const { return m_imu; }

    bool hasImuData() const { return m_hasImuData; }
    const ZenImuData& lastImuData() const { return m_imuData; }
    void setLastImuData(const ZenImuData& imuData) { m_imuData = imuData; m_hasImuData = true; }

    int getOpenMatId() const { return m_openMatId; }
    void setOpenMatId(int id) { m_openMatId = id; }

    bool hasFieldMap() const { return m_hasFieldMap; }
    void lastFieldMap(float outFieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW][3]) const;
    void setLastFieldMap(const LpVector3f fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW]);

    void setupSensorGuiContainer();

public slots:
    void updateData();

    //ID/Sampling rate
    void updateOpenMATIndex(int i);
    void updatesamplingRate(int i);

    //Range
    void updateGyrRange(int i);
    void updateAccRange(int i);
    void updateMagRange(int i);

    void updatedegradOutputCombo(int i);

    //CAN
    void updateCanStartId(int i);
    void updateCanBaudrate(int i);
    void updateCanDataPrecision(int i);
    void updateCanChannelMode(int i);
    void updateCanMapping(int i);
    void updateCanHeartbeat(int i);
    
    //Filter
    void updateFilterMode(int i);
    void updateGyroAutoCalibration(int i);
    void updateOffsetMode(int i);
    void updateResetOffset();

    //GPS data
    void updateSaveGpsState();
    void updateClearGpsState();
    void saveSelectGpsData();

    //Uart
    void updateBaudRateIndex(int i);
    void updateUartFormatIndex(int i);
    void updateUartDataPrecision(int i);
    //Imu data
    void saveSelectImuData();

private:
    bool containerSetup;

    LpVector3f m_fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW];

    ZenImuData m_imuData;
    int m_openMatId;

    zen::ZenSensor m_sensor;
    zen::ZenSensorComponent m_imu;
    ZenSensorDesc m_sensorDesc;

    QComboBox* m_streamingCombo;

    std::atomic_bool m_hasFieldMap;
    std::atomic_bool m_hasImuData;
};

#endif