/***********************************************************************
** (c) LP-RESEARCH Inc.
** All rights reserved
** Contact: info@lp-research.com
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#ifndef LP_MAIN_WINDOW
#define LP_MAIN_WINDOW

#ifdef _WIN32
#define _MATH_DEFINES_DEFINED
#include "windows.h"
#endif

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QGroupBox>
#include <QListWidget>
#include <QMessageBox>
#include <QTreeWidget>
#include <QLineEdit>
#include <QGridLayout>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QButtonGroup>
#include <QMainWindow>
#include <QWizard>
#include <QWizardPage>
#include <QSpacerItem>
#include <QSizePolicy>

#include "Plot.h"
#include "GraphWindow.h"
#include "ThreeDWindow.h"
#include "SensorGuiContainer.h"
#include "SensorGuiContainerIg1.h"
#include "DiscoveryTree.h"
#include "FieldMapContainer.h"
#include "RescanDialog.h"
#include "CubeWindowContainer.h"
#include "PlayControl.h"

#include <atomic>
#include <string>
#include <list>

#include "LpAccelerometerAlignment.h"
#include "LpGyroAlignment.h"
#include "LpMagnetometerAlignment.h"
#include "LpMagnetometerCalibration.h"
#include "LpMagnetometerReference.h"

#define CALIBRATION_FILE "LpmsControlConfiguration.xml"

#define MODE_GRAPH_WIN 0
#define MODE_THREED_WIN 1
#define MODE_FIELDMAP_WIN 2
#define MODE_GAIT_TRACKING_WIN 3

#define LPMS_CONTROL_VERSION "2.0.0"

#define GRAPH_UPDATE_PERIOD 2000

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(zen::ZenClient client, QWidget *parent = 0);
    ~MainWindow();

public slots:
    QWidget *createDeviceList(void);
    QGroupBox *createGraphs(void);
    void createMenuAndToolbar(void);
    void closeEvent(QCloseEvent *event);

    void selectGraphWindow(void);
    void selectGraph2Window(void);
    void selectGraph3Window(void);
    void selectHeaveMotionWindow(void);
    void selectThreeDWindow(void);
    void selectFieldMapWindow(void);
    void selectCubeMode1(void);
    void selectCubeMode2(void);
    void selectCubeMode4(void);
    bool exitWindow(void);

    void updateBaudRate(int i);
    void updateCurrentLpms(QTreeWidgetItem *current = 0, QTreeWidgetItem *previous = 0);

    void openSensor(void);
    void closeSensor(void);
    void startMeasurement(void);
    void stopMeasurement(void);

    void setOffset(void);
    void resetOffset(void);
    void timestampReset(void);
    void softwareSyncStart(void);
    void softwareSyncStop(void);
    void recalibrate(void);

    void uploadFirmware(void);
    void uploadTimerUpdate(void);
    void uploadIap(void);
    void toggleRecordData(void);
    void browseRecordFile(void);

    void saveCalibration(void);
    void addRemoveDevices(void);
    void saveCalibrationData(void);
    void loadCalibrationData(void);

    void measureLatency(void);
    void getVersionInfo(void);
    void resetToFactory(void);

    void startWaitBar(int t);
    void stopWaitBar();
    void calTimerUpdate(void);

    void magMisalignmentCal(void);
    void magMaNewPage(int i);
    void magMaFinished();
    QWizardPage *magMaOrientationPage(const char* ts, const char* es);
    QWizardPage *magMaFinishedPage(void);

    void gyrMisalignmentCal(void);
    void gyrMaNewPage(int i);
    void gyrMaFinished();
    QWizardPage *gyrMaOrientationPage(const char* ts, const char* es);
    QWizardPage *gyrMaFinishedPage(void);

    QWizardPage *magEllipsoidCalPage(int pageNumber);
    void magEllipsoidCalNewPage(int i);
    void calibrateMag(void);
	void onMagCaliFinished();
	void onMagRefCaliFinished();

    QWizardPage *maOrientationPage(const char* ts, const char* es);
    QWizardPage *maFinishedPage(void);
    void misalignmentCal(void);
    void maNewPage(int i);
    void maFinished();

    void updateMagneticFieldMap(const ZenImuData& imuData);
	void updateCurrentField(const ZenImuData& imuData);
    void startReplay(void);
    void stopReplay(void);
    void browsePlaybackFile(void);
    void loadObjFile(void);
    void xAxisAutoScrolling(void);
    void yAxisAutoScaling(void);

    void startFlashLog(void);
    void stopFlashLog(void);
    void clearFlashLog(void);
    void eraseFlash(void);
    void saveFlashLog(void);
    void downloadFlashLogTimerUpdate(void);
    void cancelDownloadFlashLog();
    void enableFlashMenu();
    void disableFlashMenu();

    void updateGraph(int openMatId, const ZenImuData& imuData);
    void updateFieldmap(int openMatId, const ZenImuData& imuData);
    void recordImuData(int openMatId, const ZenImuData& imuData);

    void playbackFinished();

signals:
    void accAlignCaliStageChanged(LpAccAlignCaliStage newStage);
    void gyrAlignCaliStageChanged(LpGyrAlignCaliStage newStage);
    void magAlignCaliStageChanged(LpMagAlignCaliStage newStage);
	void magCaliFinished();
	void magRefCaliFinished();

    void imuDataChanged(int openMatId, const ZenImuData& imuData);

private:
    void graphLoop();

    void handleImuEvent(const SensorGuiContainerBase& container, const ZenImuData& imuData);

    zen::ZenClient m_client;

    QGroupBox *gb;
    QList<QTreeWidgetItem*> lpmsTreeItems;
    QTreeWidget *lpmsTree;
    GraphWindow *graphWindow;
    CubeWindowSelector *cubeWindowContainer;
    FieldMapContainer *fieldMapWindow;
    SensorGuiContainerBase *currentLpms;
    std::list<SensorGuiContainerBase*> lpmsList;
    QLabel *imuFpsLabel;
    QLabel *bluetoothStatusLabel;
    QLabel *lpmsStatusLabel;
    QLabel *xAccLabel;
    QLabel *yAccLabel;
    QLabel *zAccLabel;
    QLabel *xGyroLabel;
    QLabel *yGyroLabel;
    QLabel *zGyroLabel;
    QLabel *xMagLabel;
    QLabel *yMagLabel;
    QLabel *zMagLabel;
    QLabel *xAngleLabel;
    QLabel *yAngleLabel;
    QLabel *zAngleLabel;
    QLabel *xQuatLabel;
    QLabel *yQuatLabel;
    QLabel *zQuatLabel;
    QLabel *wQuatLabel;
    QLabel *frameCountLabel;
    QLabel *activeLpmsLabel;
    QLabel *magRangeLabel;
    QLabel *pressureLabel;
    QLabel *xLinAccLabel;
    QLabel *yLinAccLabel;
    QLabel *zLinAccLabel;
    QLineEdit *deviceAddressEdit;
    QLineEdit *ftdiDeviceEdit;
    QPushButton *recordDataButton;
    QPushButton *startButton;
    QPushButton *connectButton;
    QPushButton *calibrateMagButton;
    QPushButton *startServerButton;
    QPushButton *graphWindowButton;
    QPushButton *graphWindow2Button;
    QPushButton *graphWindow3Button;
    QPushButton *threeDWindowButton;
    QPushButton *fieldMapWindowButton;
    std::atomic_bool m_isVisualizing;
    bool isConnecting;
    bool calibratingMag;
    QSlider *accGainSl;
    QSlider *magGainSl;
    QSlider *accCovarSl;
    QSlider *magCovarSl;
    QLineEdit *csvFileEdit;
    QRadioButton* gyrOnlyBtn;
    QRadioButton* gyrAccBtn;
    QRadioButton* accMagBtn;
    QRadioButton* gyrAccMagNsBtn;
    QRadioButton* gyrAccMagSwBtn;
    QRadioButton* gTEnableBtn;
    QRadioButton* gTDisableBtn;
    QComboBox* deviceTypeSelector;
    QComboBox *addressSelector;
    QComboBox *recordingRateCombo;
    QAction* startAction;
    QAction* saveAction;
    QAction* cubeMode1Action;
    QAction* cubeMode2Action;
    QAction* cubeMode4Action;
    QAction* xAxisAutoScrollingAction;
    QAction* yAxisAutoScalingAction;
    //QAction* startFlashLogAction;
    //QAction* stopFlashLogAction;
    //QAction* clearFlashLogAction;
    //QAction* eraseFlashAction;
    //QAction* saveFlashLogAction;
    int textUpdateCounter;
    QTimer* uploadTimer;
    QProgressDialog* uploadProgress;
    QWizard* maWizard;
    int calMaxTime;
    int calTime;
    QProgressDialog *calProgress;
    QTimer *calTimer;
    RescanDialog* rescanD;
    int mode;
    QGridLayout *cubeLayout;
    QVBoxLayout* graphLayout;
    QComboBox *comboDeviceList;
    std::string globalRecordFile;
    QLineEdit *recordFileEdit;
    QComboBox *targetCombo;
    QComboBox *resetMethodCombo;
    bool recordFileSet;
    QComboBox *baudRateList;
    QToolBar *toolbar;
    QMenu* viewMenu;
    MotionPlayer *rePlayer;
    QAction *replayAction;
    std::string globalPlaybackFile;
    QLineEdit *playbackFileEdit;
    bool playbackFileSet;
    QWizard* magEllipsoidCalWizard;
    int softwareSyncCount;

    int m_defaultBaudRate;

    std::ofstream m_recordingStream;
    char m_recordingStreamBuffer[65536];
    std::mutex m_recordingMutex;
    float m_timestampOffset;
    int m_frameCountOffset;
    bool m_firstRecord;

    std::atomic_bool m_terminate;
    std::thread m_graphThread;

    LpAccAlignHandle m_accAlignHandle;
    LpAccAlignCaliStage m_accAlignStage;

    LpGyrAlignHandle m_gyrAlignHandle;
    LpGyrAlignCaliStage m_gyrAlignStage;

    LpMagAlignHandle m_magAlignHandle;
    LpMagAlignCaliStage m_magAlignStage;

	LpMagHandle m_magHandle;
	LpMagRefHandle m_magRefHandle;

    std::atomic_bool m_accAlignCalibrating;
    std::atomic_bool m_accAlignInstructing;
	std::atomic_bool m_gyrAlignCalibrating;
    std::atomic_bool m_gyrAlignInstructing;
	std::atomic_bool m_magAlignCalibrating;
    std::atomic_bool m_magAlignInstructing;
	std::atomic_bool m_magRefCalibrating;
	std::atomic_bool m_magRefInstructing;
	std::atomic_bool m_magCalibrating;
    std::atomic_bool m_magInstructing;
    std::atomic_bool m_recording;
    std::atomic_bool m_uploadingFirmware;
};

#endif
