/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (klaus@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#include "PlayControl.h"

#include <fstream>
#include <iostream>

#include <boost/tokenizer.hpp>

MotionPlayer::MotionPlayer(void) :
    m_playStarted(false),
    m_playPointer(0),
    m_elapsedTime(0)
{}

MotionPlayer::~MotionPlayer()
{
    m_playStarted = false;
    if (m_thread.joinable())
        m_thread.join();
}

bool MotionPlayer::updateJointsFromData(double t)
{
    if (m_playPointer >= m_dataList.size())
        return false;

    const std::pair<int, ZenImuData> d = m_dataList[m_playPointer];
    if (t > (d.second.timestamp * 1000.0f))
    {
        setCurrentData(d.first, d.second);
        ++m_playPointer;
    }

    return true;
}

bool MotionPlayer::readMotionDataFile(const std::string& fn) {
    int i;
    std::string line;
    LpVector4f q;
    LpMatrix3x3f m;

    std::ifstream dataFile(fn.c_str(), std::ios::in);

    m_dataList.clear();

    if (dataFile.is_open()) {
        getline(dataFile, line); // Header

        while (dataFile.good()) {
            std::pair<int, ZenImuData> d;

            getline(dataFile, line);

            boost::char_separator<char> sep(", ");
            boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
            boost::tokenizer<boost::char_separator<char>>::iterator ti;

            if (tokens.begin() == tokens.end()) break;

            // SensorId, TimeStamp (s), FrameNumber, AccX (g), AccY (g), AccZ (g), GyroX (deg/s), GyroY (deg/s), GyroZ (deg/s), MagX (uT), MagY (uT), MagZ (uT), EulerX (deg), EulerY (deg), EulerZ (deg), QuatX, QuatY, QuatZ, QuatW, LinAccX (m/s^2), LinAccY (m/s^2), LinAccZ (m/s^2), Pressure (hPa), Altitude (m), Temperature (degC), HeaveMotion (m)			

            ti = tokens.begin();
            d.first = std::stod(*ti); ++ti;
            d.second.timestamp = std::stod(*ti); ++ti;
            d.second.frameCount = std::stod(*ti); ++ti;
            for (i = 0; i < 3; ++i) {
                d.second.a[i] = std::stod(*ti); ++ti;
            }
            for (i = 0; i < 3; ++i) {
                d.second.g[i] = std::stod(*ti); ++ti;
            }
            for (i = 0; i < 3; ++i) {
                d.second.b[i] = std::stod(*ti); ++ti;
            }
            for (i = 0; i < 3; ++i) {
                d.second.r[i] = std::stod(*ti); ++ti;
            }
            for (i = 0; i < 4; ++i) {
                d.second.q[i] = std::stod(*ti); ++ti;
            }
            for (i = 0; i < 4; ++i) {
                d.second.linAcc[i] = std::stod(*ti); ++ti;
            }

            convertArrayToLpVector4f(d.second.q, &q);
            quaternionToMatrix(&q, &m);
            convertLpMatrixToArray(&m, d.second.rotationM);

            // printf("[PlayControl] Read t=%f, q0=%f, q1=%f, q2=%f, q3=%f\n", d.timeStamp, d.q[0], d.q[1], d.q[2], d.q[3]);

            m_dataList.push_back(d);
        }
        dataFile.close();

        std::cout << "[PlayControl] Playing motion data from " << fn.c_str() << std::endl;

        m_playStarted = false;
        reset();
    }
    else {
        std::cout << "[PlayControl] Could not open file " << fn << std::endl;
        return false;
    }
    return true;
}

void MotionPlayer::runPlay(void)
{
    auto start = std::chrono::high_resolution_clock::now();
    bool done = false;
    while (m_playStarted && !done)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        const auto end = std::chrono::high_resolution_clock::now();
        m_elapsedTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        start = end;

        done = !updateJointsFromData(m_elapsedTime.count());
    }

    m_playStarted = false;
    if (done)
        emit finished();
}

void MotionPlayer::play(void)
{
    if (!m_playStarted.exchange(true))
    {
        if (m_thread.joinable())
            m_thread.join();

        m_thread = std::thread(&MotionPlayer::runPlay, this);
    }
}

void MotionPlayer::pause(void)
{
    m_playStarted = false;
}

void MotionPlayer::reset(void)
{
    m_playPointer = 0;
    m_elapsedTime = std::chrono::milliseconds(0);
    play();
}

size_t MotionPlayer::hasImuData(void)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_dataQueue.size();
}

void MotionPlayer::setCurrentData(int openMatId, const ZenImuData& d)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    m_currentData.first = openMatId;
    m_currentData.second = d;

    if (m_dataQueue.size() >= 64)
        m_dataQueue.pop();

    m_dataQueue.push(m_currentData);
}

std::pair<int, ZenImuData> MotionPlayer::getData()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    if (m_dataQueue.size() > 0)
    {
        const std::pair<int, ZenImuData> d = m_dataQueue.front();
        m_dataQueue.pop();
        return d;
    }

    return m_currentData;
}