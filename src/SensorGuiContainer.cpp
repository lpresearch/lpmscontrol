/***********************************************************************
** Copyright (C) LP-RESEARCH Inc.
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#include "SensorGuiContainer.h"

#include <array>
#include <iostream>
#include <vector>

#include <rapidjson/document.h>

SensorGuiContainer::SensorGuiContainer( int openMatId, 
                                        zen::ZenSensor sensor, 
                                        zen::ZenSensorComponent imu, 
                                        const ZenSensorDesc& sensorDesc, 
                                        std::string modelName,
                                        QTreeWidget* tree)
    : containerSetup(false)
    , m_openMatId(openMatId)
    , m_sensor(std::move(sensor))
    , m_imu(imu)
    , m_sensorDesc(sensorDesc)
    , m_hasFieldMap(false)
    , m_hasImuData(false)
{
    setTreeItem(new QTreeWidgetItem(tree, QStringList(QString("Sensor"))));
    setModelName(modelName);

    QGridLayout* gl = new QGridLayout();
    QGridLayout* gl1 = new QGridLayout();
    QGridLayout* gl2 = new QGridLayout();
	QGridLayout* gl4 = new QGridLayout();
    QGridLayout* gl5 = new QGridLayout();
    QGridLayout* gl6 = new QGridLayout();
	selectedDataGl = new QGridLayout();

    unsigned int l = 0;
    gl5->addWidget(new QLabel("Connection:"), l, 0);
    gl5->addWidget(statusItem = new QLabel("n/a"), l++, 1);

    gl5->addWidget(new QLabel("Sensor status:"), l, 0);
    gl5->addWidget(runningItem = new QLabel("n/a"), l++, 1);

    gl5->addWidget(new QLabel("Device ID:"), l, 0);
    gl5->addWidget(addressItem = new QLabel("n/a"), l++, 1);

    gl5->addWidget(new QLabel("Firmware version:"), l, 0);
    gl5->addWidget(firmwareItem = new QLabel("n/a"), l++, 1);

    gl5->addWidget(new QLabel("Battery:"), l, 0);
    gl5->addWidget(batteryItem = new QLabel("n/a"), l++, 1);

    l = 0;
    gl->addWidget(new QLabel("IMU ID:"), l, 0);
    gl->addWidget(indexItem = new QLabel("n/a"), l++, 1);

    gl->addWidget(new QLabel("Transmission rate:"), l, 0);
    gl->addWidget(samplingRateCombo = new QComboBox(), l++, 1);

    l = 0;
	gl6->addWidget(new QLabel("Baud rate:"), l, 0);
	gl6->addWidget(baudRateCombo = new QComboBox(), l++, 1);

    gl6->addWidget(new QLabel("Data format:"), l, 0);
    gl6->addWidget(uartFormatCombo = new QComboBox(), l++, 1);

    l = 0;
    gl1->addWidget(new QLabel("GYR range:"), l, 0);
    gl1->addWidget(gyrRangeCombo = new QComboBox(), l++, 1);

    gl1->addWidget(new QLabel("ACC range:"), l, 0);
    gl1->addWidget(accRangeCombo = new QComboBox(), l++, 1);

    gl1->addWidget(new QLabel("MAG range:"), l, 0);
    gl1->addWidget(magRangeCombo = new QComboBox(), l++, 1);

    l = 0;
    gl2->addWidget(new QLabel("Filter mode:"), l, 0);
    gl2->addWidget(filterModeCombo = new QComboBox(), l++, 1);

    gl2->addWidget(new QLabel("MAG correction:"), l, 0);
    gl2->addWidget(magCovarSpinner = new QSpinBox(), l++, 1);

    gl2->addWidget(new QLabel("Lin. ACC correction:"), l, 0);
    gl2->addWidget(linAccCompRateSpinner = new QSpinBox(), l++, 1);

    gl2->addWidget(new QLabel("Rot. ACC correction:"), l, 0);
    gl2->addWidget(centriCompRateSpinner = new QSpinBox, l++, 1);

    gl2->addWidget(new QLabel("GYR threshhold:"), l, 0);
    gl2->addWidget(thresholdEnableCombo = new QComboBox(), l++, 1);

    gl2->addWidget(new QLabel("GYR autocalibration:"), l, 0);
    gl2->addWidget(gyrAutocalibrationCombo = new QComboBox(), l++, 1);

    connect(samplingRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updatesamplingRate(int)));

    QComboBox* syncCombo = new QComboBox();
    syncCombo->addItem(QString("On"));
    syncCombo->addItem(QString("Off"));

    magCovarSpinner->setRange(0, 1e6);
    magCovarSpinner->setSingleStep(1);
    connect(magCovarSpinner, SIGNAL(valueChanged(int)), this, SLOT(updateMagCovar(int)));

    linAccCompRateSpinner->setRange(0, 1e6);
    linAccCompRateSpinner->setSingleStep(1);
    connect(linAccCompRateSpinner, SIGNAL(valueChanged(int)), this, SLOT(updateLinAccCompRate(int)));

    centriCompRateSpinner->setRange(0, 1e6);
    centriCompRateSpinner->setSingleStep(1);
    connect(centriCompRateSpinner, SIGNAL(valueChanged(int)), this, SLOT(updateCentriCompRate(int)));

    //filterModeCombo->addItem(QString("Gyr only"));
    //filterModeCombo->addItem(QString("Gyr + Acc (Kalman)"));
    //filterModeCombo->addItem(QString("Gyr + Acc + Mag (Kalman)"));
    //filterModeCombo->addItem(QString("Gyr + Acc (DCM)"));
    //filterModeCombo->addItem(QString("Gyr + Acc + Mag (DCM)"));
    connect(filterModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateFilterMode(int)));

    thresholdEnableCombo->addItem(QString("Disable"));
    thresholdEnableCombo->addItem(QString("Enable"));
    connect(thresholdEnableCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyrThresholdEnable(int)));

    gyrAutocalibrationCombo->addItem(QString("Disable"));
    gyrAutocalibrationCombo->addItem(QString("Enable"));
    connect(gyrAutocalibrationCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyrAutocalibration(int)));

    connect(accRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateAccRange(int)));

	baudRateCombo->addItem(QString("19200 bps"));
	baudRateCombo->addItem(QString("38400 bps"));
	baudRateCombo->addItem(QString("57600 bps"));
	baudRateCombo->addItem(QString("115200 bps"));
	baudRateCombo->addItem(QString("230400 bps"));
	baudRateCombo->addItem(QString("256000 bps"));
	baudRateCombo->addItem(QString("460800 bps"));
	baudRateCombo->addItem(QString("921600 bps"));
	baudRateCombo->setCurrentIndex(3);
	connect(baudRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBaudRateIndex(int)));

	uartFormatCombo->addItem(QString("LPBUS (binary)"));
	uartFormatCombo->addItem(QString("ASCII (CSV)"));
	connect(uartFormatCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateUartFormatIndex(int)));

    l = 0;
    selectedDataGl->addWidget(new QLabel("Streaming:"), l, 0);
    selectedDataGl->addWidget(m_streamingCombo = new QComboBox(), l, 1); ++l;

    m_streamingCombo->addItem("Disable");
    m_streamingCombo->addItem("Enable");
    connect(m_streamingCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateStreaming(int)));

    selectedDataGl->addWidget(new QLabel("Enabled data:"), l, 0);
    selectedDataGl->addWidget(selectLowPrecision = new QCheckBox("Low data precision (16-bit integer)"), l, 1); ++l;
    selectedDataGl->addWidget(selectAcc = new QCheckBox("Raw accelerometer"), l, 1); ++l;
    selectedDataGl->addWidget(selectMag = new QCheckBox("Raw magnetometer"), l, 1); ++l;
    selectedDataGl->addWidget(selectGyro = new QCheckBox("Raw gyroscope"), l, 1); ++l;
    selectedDataGl->addWidget(selectAngularVelocity = new QCheckBox("Angular velocity"), l, 1); ++l;
    selectedDataGl->addWidget(selectQuaternion = new QCheckBox("Quaternion"), l, 1); ++l;
    selectedDataGl->addWidget(selectEuler = new QCheckBox("Euler angle"), l, 1); ++l;
    selectedDataGl->addWidget(selectLinAcc = new QCheckBox("Lin. acceleration"), l, 1); ++l;
    selectedDataGl->addWidget(selectPressure = new QCheckBox("Bar. pressure"), l, 1); ++l;
    selectedDataGl->addWidget(selectAltitude = new QCheckBox("Altitude"), l, 1); ++l;
    selectedDataGl->addWidget(selectTemperature = new QCheckBox("Temperature"), l, 1); ++l;
    selectedDataGl->addWidget(selectHeaveMotion = new QCheckBox("Heave motion"), selectedDataGl->rowCount() + 1, 1);

    connect(selectLowPrecision, SIGNAL(stateChanged(int)), this, SLOT(updateLpBusDataMode(int)));
    connect(selectAcc, SIGNAL(stateChanged(int)), this, SLOT(updateSelectAcc(int)));
    connect(selectMag, SIGNAL(stateChanged(int)), this, SLOT(updateSelectMag(int)));
    connect(selectGyro, SIGNAL(stateChanged(int)), this, SLOT(updateSelectGyro(int)));
    connect(selectQuaternion, SIGNAL(stateChanged(int)), this, SLOT(updateSelectQuaternion(int)));
    connect(selectEuler, SIGNAL(stateChanged(int)), this, SLOT(updateSelectEuler(int)));
    connect(selectLinAcc, SIGNAL(stateChanged(int)), this, SLOT(updateSelectLinAcc(int)));
    connect(selectPressure, SIGNAL(stateChanged(int)), this, SLOT(updateSelectPressure(int)));
    connect(selectAltitude, SIGNAL(stateChanged(int)), this, SLOT(updateSelectAltitude(int)));
    connect(selectTemperature, SIGNAL(stateChanged(int)), this, SLOT(updateSelectTemperature(int)));
    connect(selectAngularVelocity, SIGNAL(stateChanged(int)), this, SLOT(updateSelectAngularVelocity(int)));
    connect(selectHeaveMotion, SIGNAL(stateChanged(int)), this, SLOT(updateSelectHeaveMotion(int)));

    QWidget *w5 = new QWidget();
    w5->setLayout(gl5);
    QTreeWidgetItem* statusTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("Status")));
    QTreeWidgetItem* statusTreeWidget = new QTreeWidgetItem(statusTreeItem);
    statusTreeItem->setExpanded(true);
    tree->setItemWidget(statusTreeWidget, 0, w5);

    QWidget *w0 = new QWidget();
    w0->setLayout(gl);
    QTreeWidgetItem* connectionTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("ID / sampling rate")));
    QTreeWidgetItem* connectionTreeWidget = new QTreeWidgetItem(connectionTreeItem);
    tree->setItemWidget(connectionTreeWidget, 0, w0);

    QWidget *w1 = new QWidget();
    w1->setLayout(gl1);
    QTreeWidgetItem* rangeTreeItem = new QTreeWidgetItem(treeItem, QStringList(QString("Range")));
    QTreeWidgetItem* rangeTreeWidget = new QTreeWidgetItem(rangeTreeItem);
    tree->setItemWidget(rangeTreeWidget, 0, w1);

    QWidget *w2 = new QWidget();
    w2->setLayout(gl2);
    QTreeWidgetItem* filterTreeItem = new QTreeWidgetItem(treeItem, QStringList(QString("Filter")));
    QTreeWidgetItem* filterTreeWidget = new QTreeWidgetItem(filterTreeItem);
    tree->setItemWidget(filterTreeWidget, 0, w2);

	if (	std::string(m_sensorDesc.name).find("lpmscu") != std::string::npos || 
			std::string(m_sensorDesc.name).find("lpmsu") != std::string::npos) 
	{
    //if (deviceType != DEVICE_LPMS_B && deviceType != DEVICE_LPMS_B2 && deviceType != DEVICE_LPMS_BLE) {
		QWidget *w6 = new QWidget();
		w6->setLayout(gl6);
		QTreeWidgetItem* uartTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("UART (RS-232/TTL)")));
		QTreeWidgetItem* uartTreeWidget = new QTreeWidgetItem(uartTreeItem);
		tree->setItemWidget(uartTreeWidget, 0, w6);

		l = 0;

		gl4->addWidget(new QLabel("CAN baudrate"), l, 0);
		gl4->addWidget(canBaudrateCombo = new QComboBox(), l, 1); ++l;

		canBaudrateCombo->addItem(QString("10 KBit/s"));
		canBaudrateCombo->addItem(QString("20 KBit/s"));
		canBaudrateCombo->addItem(QString("50 KBit/s"));
		canBaudrateCombo->addItem(QString("125 KBit/s"));
		canBaudrateCombo->addItem(QString("250 KBit/s"));
		canBaudrateCombo->addItem(QString("500 KBit/s"));
		canBaudrateCombo->addItem(QString("800 KBit/s"));
		canBaudrateCombo->addItem(QString("1 MBit/s"));

		connect(canBaudrateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanBaudrate(int)));

		gl4->addWidget(new QLabel("Channel mode"), l, 0);
		gl4->addWidget(canChannelModeCombo = new QComboBox(), l, 1); ++l;

		canChannelModeCombo->addItem(QString("CANOpen"));
		canChannelModeCombo->addItem(QString("Sequential"));

		connect(canChannelModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanChannelMode(int)));

		gl4->addWidget(new QLabel("Value mode"), l, 0);
		gl4->addWidget(canPointModeCombo = new QComboBox(), l, 1); ++l;

		canPointModeCombo->addItem(QString("32-bit floating point"));
		canPointModeCombo->addItem(QString("16-bit fixed point"));

		connect(canPointModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanPointMode(int)));

		gl4->addWidget(new QLabel("Start ID"), l, 0);
		gl4->addWidget(canStartIdSpin = new QSpinBox(), l, 1); ++l;
		canStartIdSpin->setRange(0, 0xffff);

		connect(canStartIdSpin, SIGNAL(valueChanged(int)), this, SLOT(updateCanStartId(int)));

		gl4->addWidget(new QLabel("Heartbeat freq."), l, 0);
		gl4->addWidget(canHeartbeatCombo = new QComboBox(), l, 1); ++l;

		canHeartbeatCombo->addItem(QString("0.5s"));
		canHeartbeatCombo->addItem(QString("1.0s"));
		canHeartbeatCombo->addItem(QString("2.0s"));
		canHeartbeatCombo->addItem(QString("5.0s"));
		canHeartbeatCombo->addItem(QString("10.0s"));

		connect(canHeartbeatCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanHeartbeat(int)));

		for (int i = 0; i < 16; ++i)
		{
			gl4->addWidget(new QLabel((std::string("Channel ") + std::to_string(i)).c_str()), l, 0);
			gl4->addWidget(canTpdoCombo[i] = new QComboBox(), l, 1); ++l;
			canTpdoCombo[i]->addItem(QString("Gyroscope X"));
			canTpdoCombo[i]->addItem(QString("Gyroscope Y"));
			canTpdoCombo[i]->addItem(QString("Gyroscope Z"));
			canTpdoCombo[i]->addItem(QString("Euler angle X"));
			canTpdoCombo[i]->addItem(QString("Euler angle Y"));
			canTpdoCombo[i]->addItem(QString("Euler angle Z"));
			canTpdoCombo[i]->addItem(QString("Lin. acceleration X"));
			canTpdoCombo[i]->addItem(QString("Lin. acceleration Y"));
			canTpdoCombo[i]->addItem(QString("Lin. acceleration Z"));
			canTpdoCombo[i]->addItem(QString("Magnetometer X"));
			canTpdoCombo[i]->addItem(QString("Magnetometer Y"));
			canTpdoCombo[i]->addItem(QString("Magnetometer Z"));
			canTpdoCombo[i]->addItem(QString("Quaternion W"));
			canTpdoCombo[i]->addItem(QString("Quaternion X"));
			canTpdoCombo[i]->addItem(QString("Quaternion Y"));
			canTpdoCombo[i]->addItem(QString("Quaternion Z"));
			canTpdoCombo[i]->addItem(QString("Accelerometer X"));
			canTpdoCombo[i]->addItem(QString("Accelerometer Y"));
			canTpdoCombo[i]->addItem(QString("Accelerometer Z"));
			connect(canTpdoCombo[i], SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));
		}

		QWidget* w4 = new QWidget();
		w4->setLayout(gl4);
		QTreeWidgetItem* canTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("CAN bus")));
		QTreeWidgetItem* canTreeWidget = new QTreeWidgetItem(canTreeItem);
		tree->setItemWidget(canTreeWidget, 0, w4);
    }

    QWidget *w3 = new QWidget();
    w3->setLayout(selectedDataGl);
    QTreeWidgetItem* dataTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("Data")));
    QTreeWidgetItem* dataTreeWidget = new QTreeWidgetItem(dataTreeItem);
    tree->setItemWidget(dataTreeWidget, 0, w3);

    setupSensorGuiContainer();
}

void SensorGuiContainer::lastFieldMap(float outFieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW][3]) const
{
    for (int i = 0; i < ABSMAXPITCH; ++i)
        for (int j = 0; j < ABSMAXROLL; ++j)
            for (int k = 0; k < ABSMAXYAW; ++k)
                for (int l = 0; l < 3; ++l)
                    outFieldMap[i][j][k][l] = m_fieldMap[i][j][k].data[l];
}

void SensorGuiContainer::setLastFieldMap(const LpVector3f fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW])
{
    for (int i = 0; i < ABSMAXPITCH; ++i)
        for (int j = 0; j < ABSMAXROLL; ++j)
            for (int k = 0; k < ABSMAXYAW; ++k)
                    m_fieldMap[i][j][k] = fieldMap[i][j][k];

    m_hasFieldMap = true;
}

void SensorGuiContainer::setupSensorGuiContainer()
{
    if (containerSetup)
        return;

    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_AccSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_AccSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                accRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + "G", range);
    }
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_GyrSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_GyrSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                gyrRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + " dps", range);
    }
    connect(gyrRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyrRange(int)));
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_MagSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_MagSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                magRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + " Gauss", range);
    }
    connect(magRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateMagRange(int)));
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_SupportedSamplingRates, nullptr, 0);

        std::vector<int32_t> rates(bufferSize);
        auto [error, __] = m_imu.getArrayProperty(ZenImuProperty_SupportedSamplingRates, rates.data(), bufferSize);
        if (error == ZenError_None)
            for (int32_t rate : rates)
                samplingRateCombo->addItem(QString::fromStdString(std::to_string(rate)) + " Hz", rate);
    }
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<std::byte>(ZenImuProperty_SupportedFilterModes, nullptr, 0);

        std::vector<char> json(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_SupportedFilterModes, reinterpret_cast<std::byte*>(json.data()), bufferSize);
        if (!error)
        {
            rapidjson::Document doc;
            doc.Parse(json.data(), json.size());

            if (!doc.HasParseError() && doc.HasMember("config") && doc["config"].IsArray())
            {
                for (auto it = doc["config"].Begin(); it != doc["config"].End(); ++it)
                {
                    const auto& pair{ *it };
                    filterModeCombo->addItem(pair["key"].GetString(), pair["value"].GetInt());
                }
            }
        }
    }
    /* {
        auto [error, bufferSize] = m_sensor.getArrayProperty<int32_t>(ZenSensorProperty_SupportedBaudRates, nullptr, 0);
        if (error != ZenError_UnknownProperty)
        {
            std::vector<int32_t> rates(bufferSize);
            auto [error2, _] = m_sensor.getArrayProperty(ZenSensorProperty_SupportedBaudRates, rates.data(), bufferSize);
            if (!error2)
            {
                for (int32_t rate : rates)
                    baudRateCombo->addItem(QString::fromStdString(std::to_string(rate)) + " bits/s", rate);

                connect(baudRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBaudRateIndex(int)));
            }
        }
    } */

    containerSetup = true;
}

#define MAX_CAN_CHANNELS 16
void SensorGuiContainer::updateData(void) {
    if (!containerSetup)
        return;

    m_streamingCombo->blockSignals(true);
    thresholdEnableCombo->blockSignals(true);
    accRangeCombo->blockSignals(true);
    magRangeCombo->blockSignals(true);
    gyrRangeCombo->blockSignals(true);
    filterModeCombo->blockSignals(true);
    selectLowPrecision->blockSignals(true);
    selectQuaternion->blockSignals(true);
    selectEuler->blockSignals(true);
    selectLinAcc->blockSignals(true);
    selectGyro->blockSignals(true);
    selectAcc->blockSignals(true);
    selectMag->blockSignals(true);
    selectPressure->blockSignals(true);
    selectAltitude->blockSignals(true);
    selectTemperature->blockSignals(true);
    selectHeaveMotion->blockSignals(true);
    magCovarSpinner->blockSignals(true);
    linAccCompRateSpinner->blockSignals(true);
    centriCompRateSpinner->blockSignals(true);

    getTreeItem()->setText(0, QString(m_sensorDesc.name) + " (" + QString(m_sensorDesc.ioType) + QString(") ID=%1").arg(m_openMatId));

    addressItem->setText(QString(m_sensorDesc.name));
    indexItem->setText(QString::fromStdString(std::to_string(m_openMatId)));

	{
		std::array<std::byte, 16> name;
		auto [error, _] = m_sensor.getArrayProperty(ZenSensorProperty_DeviceName, name.data(), name.size());
		if (!error)
		{
			std::string nameStr(reinterpret_cast<const char*>(&name[0]), std::end(name)- std::begin(name));
		}
	}

	if (std::string(m_sensorDesc.name).find("lpmscu") != std::string::npos ||
		std::string(m_sensorDesc.name).find("lpmsu") != std::string::npos)
	{
		{
			std::vector<int32_t> channelSettings(MAX_CAN_CHANNELS);
			auto [error, _] = m_imu.getArrayProperty(ZenImuProperty_CanMapping, channelSettings.data(), channelSettings.size());
			if (!error)
			{
				for (int i = 0; i < MAX_CAN_CHANNELS; ++i)
				{
					std::cout << "can map get=" << i << " " << channelSettings[i] << std::endl;
					canTpdoCombo[i]->setCurrentIndex(channelSettings[i]);
				}
			}
			else
				std::cout << "error get can map: " << error << std::endl;
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanChannelMode);
			if (!error)
				canChannelModeCombo->setCurrentIndex(value);
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanPointMode);
			if (!error)
				canPointModeCombo->setCurrentIndex(value);
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanStartId);
			if (!error)
				canStartIdSpin->setValue(value);
		}
		{
			auto [error, value] = m_imu.getFloatProperty(ZenImuProperty_CanHeartbeat);
			if (!error)
				if (value <= 0.5)
					canHeartbeatCombo->setCurrentIndex(0);
				else if (value <= 1.0)
					canHeartbeatCombo->setCurrentIndex(1);
				else if (value <= 2.0)
					canHeartbeatCombo->setCurrentIndex(2);
				else if (value <= 5.0)
					canHeartbeatCombo->setCurrentIndex(3);
				else
					canHeartbeatCombo->setCurrentIndex(4);
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanBaudrate);
			if (!error)
				if (value <= 10000)
					canBaudrateCombo->setCurrentIndex(0);
				else if (value <= 20000)
					canBaudrateCombo->setCurrentIndex(1);
				else if (value <= 50000)
					canBaudrateCombo->setCurrentIndex(2);
				else if (value <= 125000)
					canBaudrateCombo->setCurrentIndex(3);
				else if (value <= 250000)
					canBaudrateCombo->setCurrentIndex(4);
				else if (value <= 500000)
					canBaudrateCombo->setCurrentIndex(5);
				else if (value <= 800000)
					canBaudrateCombo->setCurrentIndex(6);
				else
					canBaudrateCombo->setCurrentIndex(7);
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_UartBaudRate);
			if (!error)
				if (value <= 19200)
					baudRateCombo->setCurrentIndex(0);
				else if (value <= 38400)
					baudRateCombo->setCurrentIndex(1);
				else if (value <= 57600)
					baudRateCombo->setCurrentIndex(3);
				else if (value <= 115200)
					baudRateCombo->setCurrentIndex(4);
				else if (value <= 230400)
					baudRateCombo->setCurrentIndex(5);
				else if (value <= 256000)
					baudRateCombo->setCurrentIndex(6);
				else if (value <= 460800)
					baudRateCombo->setCurrentIndex(7);
				else
					baudRateCombo->setCurrentIndex(8);
		}
		{
			auto [error, value] = m_imu.getInt32Property(ZenImuProperty_UartFormat);
			if (!error)
				uartFormatCombo->setCurrentIndex(value);
		}
	}

	{
		std::array<int32_t, 3> version;
		auto [error, _] = m_sensor.getArrayProperty(ZenSensorProperty_FirmwareVersion, version.data(), version.size());
		if (!error)
		{
			const std::string firmwareStr = std::to_string(version[0]) + "." + std::to_string(version[1]) + "." + std::to_string(version[2]);
			firmwareItem->setText(QString::fromStdString(firmwareStr));
		}
	}
    {
        auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_GyrUseThreshold);
        if (!error)
            thresholdEnableCombo->setCurrentIndex(enabled ? 1 : 0);
    }
    {
        auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_GyrUseAutoCalibration);
        if (!error)
            gyrAutocalibrationCombo->setCurrentIndex(enabled ? 1 : 0);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_FilterMode);
        if (!error)
            filterModeCombo->setCurrentIndex(value);
    }

    runningItem->setText("<font color='green'>started</font>");

    //switch (m_sensor.getSensorStatus()) {
    //case 0:
    //    runningItem->setText("<font color='black'>stopped</font>");
    //    break;

    //case 1:
    //    runningItem->setText("<font color='green'>started</font>");
    //    break;

    //case 2:
    //    runningItem->setText("<font color='blue'>calibrating..</font>");
    //    break;

    //case 3:
    //    runningItem->setText("<font color='blue'>error..</font>");
    //    break;

    //case 4:
    //    runningItem->setText("<font color='blue'>uploading..</font>");
    //    break;
    //}

    statusItem->setText("<font color='green'>OK</font>");

    if (m_imu.type() == g_zenSensorType_Imu)
    {
        static unsigned int updateBatteryCount = 0;
        if (updateBatteryCount++ % 10000 == 0)
        {
            updateBatteryCount = 1;

            auto [error, batteryLevel] = m_sensor.getFloatProperty(ZenSensorProperty_BatteryLevel);
            if (error == ZenError_None)
                batteryItem->setText(QString::number(batteryLevel) + "%");
        }

        {
            auto [error, range] = m_imu.getInt32Property(ZenImuProperty_AccRange);
            if (!error)
            {
                const auto idx = accRangeCombo->findData(range);
                if (idx != -1)
                    accRangeCombo->setCurrentIndex(idx);
            }
        }
        {
            auto [error, range] = m_imu.getInt32Property(ZenImuProperty_GyrRange);
            if (!error)
            {
                const auto idx = gyrRangeCombo->findData(range);
                if (idx != -1)
                    gyrRangeCombo->setCurrentIndex(idx);
            }
        }
        {
            auto [error, range] = m_imu.getInt32Property(ZenImuProperty_MagRange);
            if (!error)
            {
                const auto idx = magRangeCombo->findData(range);
                if (idx != -1)
                    magRangeCombo->setCurrentIndex(idx);
            }
        }
        {
            auto [error, samplingRate] = m_imu.getInt32Property(ZenImuProperty_SamplingRate);
            if (!error)
            {
                const auto idx = samplingRateCombo->findData(samplingRate);
                if (idx != -1)
                    samplingRateCombo->setCurrentIndex(idx);
            }
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_StreamData);
            if (!error)
                m_streamingCombo->setCurrentIndex(enabled ? 1 : 0);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputLowPrecision);
            if (!error)
                selectLowPrecision->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputRawAcc);
            if (!error)
                selectAcc->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputRawMag);
            if (!error)
                selectMag->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputRawGyr);
            if (!error)
                selectGyro->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputQuat);
            if (!error)
                selectQuaternion->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputEuler);
            if (!error)
                selectEuler->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputLinearAcc);
            if (!error)
                selectLinAcc->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputPressure);
            if (!error)
                selectPressure->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputAltitude);
            if (!error)
                selectAltitude->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputTemperature);
            if (!error)
                selectTemperature->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputAngularVel);
            if (!error)
                selectAngularVelocity->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_OutputHeaveMotion);
            if (!error)
                selectHeaveMotion->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);
        }
        {
            auto [error, value] = m_imu.getFloatProperty(ZenImuProperty_FilterPreset); 
            if (!error)
                magCovarSpinner->setValue(std::round(value));
        }
        {
            auto [error, value] = m_imu.getFloatProperty(ZenImuProperty_LinearCompensationRate);
            if (!error)
                linAccCompRateSpinner->setValue(std::round(value));
        }
        {
            auto [error, value] = m_imu.getFloatProperty(ZenImuProperty_CentricCompensationRate);
            if (!error)
                centriCompRateSpinner->setValue(std::round(value));
        }
    }

    m_streamingCombo->blockSignals(false);
    selectLowPrecision->blockSignals(false);
    selectAcc->blockSignals(false);
    selectMag->blockSignals(false);
    selectQuaternion->blockSignals(false);
    selectEuler->blockSignals(false);
    selectLinAcc->blockSignals(false);
    selectGyro->blockSignals(false);
    thresholdEnableCombo->blockSignals(false);
    accRangeCombo->blockSignals(false);
    magRangeCombo->blockSignals(false);
    gyrRangeCombo->blockSignals(false);
    filterModeCombo->blockSignals(false);
    selectPressure->blockSignals(false);
    selectAltitude->blockSignals(false);
    selectTemperature->blockSignals(false);
    selectHeaveMotion->blockSignals(false);
    magCovarSpinner->blockSignals(false);
    linAccCompRateSpinner->blockSignals(false);
    centriCompRateSpinner->blockSignals(false);
}

void SensorGuiContainer::updateStreaming(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_StreamData, m_streamingCombo->currentIndex() == 1))
        std::cout << "Failed to set property: " << ZenImuProperty_StreamData << " due to error code: " << error;
}

void SensorGuiContainer::updateGyrThresholdEnable(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_GyrUseThreshold, thresholdEnableCombo->currentIndex() == 1))
        std::cout << "Failed to set property: " << ZenImuProperty_GyrUseThreshold << " due to error code: " << error;
}

void SensorGuiContainer::updateGyrAutocalibration(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_GyrUseAutoCalibration, gyrAutocalibrationCombo->currentIndex() == 1))
        std::cout << "Failed to set property: " << ZenImuProperty_GyrUseAutoCalibration << " due to error code: " << error;
}

void SensorGuiContainer::updateMagCovar(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setFloatProperty(ZenImuProperty_FilterPreset, static_cast<float>(i)))
        std::cout << "Failed to set property: " << ZenImuProperty_FilterPreset << " due to error code: " << error;
}

void SensorGuiContainer::updateFilterMode(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setInt32Property(ZenImuProperty_FilterMode, i))
        std::cout << "Failed to set property: " << ZenImuProperty_FilterMode << " due to error code: " << error;
}

void SensorGuiContainer::updateGyrRange(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setInt32Property(ZenImuProperty_GyrRange, gyrRangeCombo->currentData().toInt()))
        std::cout << "Failed to set property: " << ZenImuProperty_GyrRange << " due to error code: " << error;
}

void SensorGuiContainer::updateAccRange(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setInt32Property(ZenImuProperty_AccRange, accRangeCombo->currentData().toInt()))
        std::cout << "Failed to set property: " << ZenImuProperty_AccRange << " due to error code: " << error;
}

void SensorGuiContainer::updateMagRange(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setInt32Property(ZenImuProperty_MagRange, magRangeCombo->currentData().toInt()))
        std::cout << "Failed to set property: " << ZenImuProperty_MagRange << " due to error code: " << error;
}

void SensorGuiContainer::updatesamplingRate(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setInt32Property(ZenImuProperty_SamplingRate, samplingRateCombo->currentData().toInt()))
        std::cout << "Failed to set property: " << ZenImuProperty_SamplingRate << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectGyro(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputRawGyr, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputRawGyr << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectQuaternion(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputQuat, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputQuat << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectEuler(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputEuler, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputEuler << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectLinAcc(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputLinearAcc, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputLinearAcc << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectAcc(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputRawAcc, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputRawAcc << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectMag(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputRawMag, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputRawMag << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectPressure(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputPressure, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputPressure << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectAltitude(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputAltitude, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputAltitude << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectTemperature(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputTemperature, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputTemperature << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectAngularVelocity(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputAngularVel, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputAngularVel << " due to error code: " << error;
}

void SensorGuiContainer::updateSelectHeaveMotion(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputHeaveMotion, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputHeaveMotion << " due to error code: " << error;
}

void SensorGuiContainer::updateLinAccCompRate(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setFloatProperty(ZenImuProperty_LinearCompensationRate, static_cast<float>(i)))
        std::cout << "Failed to set property: " << ZenImuProperty_LinearCompensationRate << " due to error code: " << error;
}

void SensorGuiContainer::updateCentriCompRate(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setFloatProperty(ZenImuProperty_CentricCompensationRate, static_cast<float>(i)))
        std::cout << "Failed to set property: " << ZenImuProperty_CentricCompensationRate << " due to error code: " << error;
}

void SensorGuiContainer::updateLpBusDataMode(int i)
{
    if (!containerSetup)
        return;

    if (auto error = m_imu.setBoolProperty(ZenImuProperty_OutputLowPrecision, i == Qt::Checked))
        std::cout << "Failed to set property: " << ZenImuProperty_OutputLowPrecision << " due to error code: " << error;
}

void SensorGuiContainer::updateUartFormatIndex(int i)
{
	if (!containerSetup)
		return;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_UartFormat, i))
		std::cout << "Failed to set property: " << ZenImuProperty_UartFormat << " due to error code: " << error;
}

void SensorGuiContainer::updateBaudRateIndex(int i)
{
	if (!containerSetup)
		return;

	int32_t toImu;
	if (i <= 0)
		toImu = 19200;
	else if (i <= 1)
		toImu = 38400;
	else if (i <= 2)
		toImu = 57600;
	else if (i <= 3)
		toImu = 115200;
	else if (i <= 4)
		toImu = 230400;
	else if (i <= 5)
		toImu = 256000;
	else if (i <= 6)
		toImu = 460800;
	else
		toImu = 921600;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_UartBaudRate, toImu))
		std::cout << "Failed to set property: " << ZenImuProperty_UartBaudRate << " due to error code: " << error;
}

void SensorGuiContainer::updateCanMapping(int i)
{
	std::vector<int32_t> channelSettings(MAX_CAN_CHANNELS);

	for (int j = 0; j < MAX_CAN_CHANNELS; ++j)
	{
		channelSettings[j] = canTpdoCombo[j]->currentIndex();
		std::cout << "can map set=" << j << " " << channelSettings[j] << std::endl;
	}

	m_imu.setArrayProperty(ZenImuProperty_CanMapping, channelSettings.data(), channelSettings.size());
}

void SensorGuiContainer::updateCanHeartbeat(int i)
{
	if (!containerSetup)
		return;

	float toImu;
	if (i <= 0)
		toImu = 0.5;
	else if (i <= 1)
		toImu = 1.0;
	else if (i <= 2)
		toImu = 2.0;
	else if (i <= 3)
		toImu = 5.0;
	else
		toImu = 10.0;

	if (auto error = m_imu.setFloatProperty(ZenImuProperty_CanHeartbeat, toImu))
		std::cout << "Failed to set property: " << ZenImuProperty_CanHeartbeat << " due to error code: " << error;
}

void SensorGuiContainer::updateCanBaudrate(int i)
{
	if (!containerSetup)
		return;

	int32_t toImu;
	if (i <= 0)
		toImu = 10000;
	else if (i <= 1)
		toImu = 20000;
	else if (i <= 2)
		toImu = 50000;
	else if (i <= 3)
		toImu = 125000;
	else if (i <= 4)
		toImu = 250000;
	else if (i <= 5)
		toImu = 500000;
	else if (i <= 6)
		toImu = 800000;
	else
		toImu = 1000000;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_CanBaudrate, toImu))
		std::cout << "Failed to set property: " << ZenImuProperty_CanBaudrate << " due to error code: " << error;
}

void SensorGuiContainer::updateCanStartId(int i)
{
	if (!containerSetup)
		return;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_CanStartId, i))
		std::cout << "Failed to set property: " << ZenImuProperty_CanStartId << " due to error code: " << error;
}

void SensorGuiContainer::updateCanPointMode(int i)
{
	if (!containerSetup)
		return;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_CanPointMode, i))
		std::cout << "Failed to set property: " << ZenImuProperty_CanPointMode << " due to error code: " << error;
}

void SensorGuiContainer::updateCanChannelMode(int i)
{
	if (!containerSetup)
		return;

	if (auto error = m_imu.setInt32Property(ZenImuProperty_CanChannelMode, i))
		std::cout << "Failed to set property: " << ZenImuProperty_CanChannelMode << " due to error code: " << error;
}
