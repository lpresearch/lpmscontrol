#include "SensorGuiContainerIg1.h"

#include <array>
#include <iostream>
#include <vector>
#include <string>

#include <rapidjson/document.h>

SensorGuiContainerIg1::SensorGuiContainerIg1(
    int openMatId, 
    zen::ZenSensor sensor, 
    zen::ZenSensorComponent imu, 
    const ZenSensorDesc& sensorDesc,
    std::string modelName,
    QTreeWidget* tree)
    : containerSetup(false)
    , m_openMatId(openMatId)
    , m_sensor(std::move(sensor))
    , m_imu(imu)
    , m_sensorDesc(sensorDesc)
    , m_hasFieldMap(false)
    , m_hasImuData(false)
{
    int l;

    setTreeItem(new QTreeWidgetItem(tree, QStringList(QString("Sensor"))));
    setModelName(modelName);

    // ID/Sampling rate
    QGridLayout* glIdSamplingRate = new QGridLayout();

    // ID
    l = 0;
    glIdSamplingRate->addWidget(new QLabel("Connection:"), l, 0);
    glIdSamplingRate->addWidget(statusItem = new QLabel("n/a"), l++, 1);

    glIdSamplingRate->addWidget(new QLabel("Sensor status:"), l, 0);
    glIdSamplingRate->addWidget(runningItem = new QLabel("n/a"), l++, 1);

    glIdSamplingRate->addWidget(new QLabel("Device ID:"), l, 0);
    glIdSamplingRate->addWidget(addressItem = new QLabel("n/a"), l++, 1);

    glIdSamplingRate->addWidget(new QLabel("Firmware version:"), l, 0);
    glIdSamplingRate->addWidget(firmwareItem = new QLabel("n/a"), l++, 1);

    glIdSamplingRate->addWidget(new QLabel("IMU ID:"), l, 0);
    glIdSamplingRate->addWidget(indexItem = new QLabel("n/a"), l++, 1);

    // Sampling rate
    glIdSamplingRate->addWidget(new QLabel("Transmission rate:"), l, 0);
    glIdSamplingRate->addWidget(samplingRateCombo = new QComboBox(), l++, 1);

    samplingRateCombo->addItem(QString("5 Hz"));
    samplingRateCombo->addItem(QString("10 Hz"));
    samplingRateCombo->addItem(QString("50 Hz"));
    samplingRateCombo->addItem(QString("100 Hz"));
    samplingRateCombo->addItem(QString("250 Hz"));
    samplingRateCombo->addItem(QString("500 Hz"));
    connect(samplingRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updatesamplingRate(int)));

    // Range
    QGridLayout* glRange = new QGridLayout();
    l = 0;
    glRange->addWidget(new QLabel("GYR range:"), l, 0); ++l;
    glRange->addWidget(new QLabel("ACC range:"), l, 0); ++l;
    glRange->addWidget(new QLabel("MAG range:"), l, 0); ++l;
    glRange->addWidget(new QLabel("Deg/Rad:"), l, 0); ++l;
    l = 0;
    glRange->addWidget(gyrRangeCombo = new QComboBox(), l, 1); ++l;
    glRange->addWidget(accRangeCombo = new QComboBox(), l, 1); ++l;
    glRange->addWidget(magRangeCombo = new QComboBox(), l, 1); ++l;
    glRange->addWidget(degradOutputCombo = new QComboBox(), l, 1); ++l;

    gyrRangeCombo->addItem(QString("400 dps"));
    gyrRangeCombo->addItem(QString("1000 dps"));
    gyrRangeCombo->addItem(QString("2000 dps"));

    accRangeCombo->addItem(QString("2 g"));
    accRangeCombo->addItem(QString("4 g"));
    accRangeCombo->addItem(QString("8 g"));
    accRangeCombo->addItem(QString("16 g"));

    magRangeCombo->addItem(QString("2 Gauss"));
    magRangeCombo->addItem(QString("8 Gauss"));

    degradOutputCombo->addItem(QString("Degree"));
    degradOutputCombo->addItem(QString("Radian"));

    connect(gyrRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyrRange(int)));
    connect(accRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateAccRange(int)));
    connect(magRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateMagRange(int)));
    connect(degradOutputCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updatedegradOutputCombo(int)));

    //CAN Settings
    l = 0;
    QGridLayout* glCAN = new QGridLayout();

    glCAN->addWidget(new QLabel("CAN baudrate"), l, 0);
    glCAN->addWidget(canBaudrateCombo = new QComboBox(), l, 1); ++l;

    canBaudrateCombo->addItem(QString("1 MBit/s"));
    canBaudrateCombo->addItem(QString("800 KBit/s"));
    canBaudrateCombo->addItem(QString("500 KBit/s"));
    canBaudrateCombo->addItem(QString("250 KBit/s"));
    canBaudrateCombo->addItem(QString("125 KBit/s"));

    connect(canBaudrateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanBaudrate(int)));

    glCAN->addWidget(new QLabel("Channel mode"), l, 0);
    glCAN->addWidget(canChannelModeCombo = new QComboBox(), l, 1); ++l;

    canChannelModeCombo->addItem(QString("CANOpen"));
    canChannelModeCombo->addItem(QString("Sequential"));

    connect(canChannelModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanChannelMode(int)));

    glCAN->addWidget(new QLabel("Precision"), l, 0);
    glCAN->addWidget(canDataPrecisionCombo = new QComboBox(), l, 1); ++l;

    canDataPrecisionCombo->addItem(QString("16-bit fixed point"));
    canDataPrecisionCombo->addItem(QString("32-bit floating point"));

    connect(canDataPrecisionCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanDataPrecision(int)));

    glCAN->addWidget(new QLabel("Start ID"), l, 0);
    glCAN->addWidget(canStartIdSpin = new QSpinBox(), l, 1); ++l;
    canStartIdSpin->setRange(0, 0xffff);

    connect(canStartIdSpin, SIGNAL(valueChanged(int)), this, SLOT(updateCanStartId(int)));

    glCAN->addWidget(new QLabel("Heartbeat freq."), l, 0);
    glCAN->addWidget(canHeartbeatCombo = new QComboBox(), l, 1); ++l;

    canHeartbeatCombo->addItem(QString("0.5s"));
    canHeartbeatCombo->addItem(QString("1.0s"));
    canHeartbeatCombo->addItem(QString("2.0s"));
    canHeartbeatCombo->addItem(QString("5.0s"));
    canHeartbeatCombo->addItem(QString("10.0s"));

    connect(canHeartbeatCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanHeartbeat(int)));
    
    //CAN bus Mapping String lists
    QStringList canBusMappingStringLists;
    canBusMappingStringLists.append(QString("Disable"));
    canBusMappingStringLists.append(QString("Acc Raw X"));
    canBusMappingStringLists.append(QString("Acc Raw Y"));
    canBusMappingStringLists.append(QString("Acc Raw Z"));

    canBusMappingStringLists.append(QString("Acc Calibrated X"));
    canBusMappingStringLists.append(QString("Acc Calibrated Y"));
    canBusMappingStringLists.append(QString("Acc Calibrated Z"));

    canBusMappingStringLists.append(QString("GyroI Raw X"));
    canBusMappingStringLists.append(QString("GyroI Raw Y"));
    canBusMappingStringLists.append(QString("GyroI Raw Z"));

    canBusMappingStringLists.append(QString("GyroII Raw X"));
    canBusMappingStringLists.append(QString("GyroII Raw Y"));
    canBusMappingStringLists.append(QString("GyroII Raw Z"));

    canBusMappingStringLists.append(QString("GyroI Bias Calibrated X"));
    canBusMappingStringLists.append(QString("GyroI Bias Calibrated Y"));
    canBusMappingStringLists.append(QString("GyroI Bias Calibrated Z"));

    canBusMappingStringLists.append(QString("GyroII Bias Calibrated X"));
    canBusMappingStringLists.append(QString("GyroII Bias Calibrated Y"));
    canBusMappingStringLists.append(QString("GyroII Bias Calibrated Z"));

    canBusMappingStringLists.append(QString("GyroI Align. Calibrated X"));
    canBusMappingStringLists.append(QString("GyroI Align. Calibrated Y"));
    canBusMappingStringLists.append(QString("GyroI Align. Calibrated Z"));

    canBusMappingStringLists.append(QString("GyroII Align. Calibrated X"));
    canBusMappingStringLists.append(QString("GyroII Align. Calibrated Y"));
    canBusMappingStringLists.append(QString("GyroII Align. Calibrated Z"));

    canBusMappingStringLists.append(QString("Mag Raw X"));
    canBusMappingStringLists.append(QString("Mag Raw Y"));
    canBusMappingStringLists.append(QString("Mag Raw Z"));

    canBusMappingStringLists.append(QString("Mag Calibrated X"));
    canBusMappingStringLists.append(QString("Mag Calibrated Y"));
    canBusMappingStringLists.append(QString("Mag Calibrated Z"));

    canBusMappingStringLists.append(QString("Angular Vel X"));
    canBusMappingStringLists.append(QString("Angular Vel Y"));
    canBusMappingStringLists.append(QString("Angular Vel Z"));

    canBusMappingStringLists.append(QString("Quat W"));
    canBusMappingStringLists.append(QString("Quat X"));
    canBusMappingStringLists.append(QString("Quat Y"));
    canBusMappingStringLists.append(QString("Quat Z"));

    canBusMappingStringLists.append(QString("Euler X"));
    canBusMappingStringLists.append(QString("Euler Y"));
    canBusMappingStringLists.append(QString("Euler Z"));

    canBusMappingStringLists.append(QString("Lin. Acc. X"));
    canBusMappingStringLists.append(QString("Lin. Acc. Y"));
    canBusMappingStringLists.append(QString("Lin. Acc. Z"));

    canBusMappingStringLists.append(QString("Pressure"));
    canBusMappingStringLists.append(QString("Temperature"));


    glCAN->addWidget(new QLabel("Channel 1"), l, 0);
    glCAN->addWidget(canTpdo1ACombo = new QComboBox(), l, 1); ++l;
    canTpdo1ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo1ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 2"), l, 0);
    glCAN->addWidget(canTpdo1BCombo = new QComboBox(), l, 1); ++l;
    canTpdo1BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo1BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 3"), l, 0);
    glCAN->addWidget(canTpdo2ACombo = new QComboBox(), l, 1); ++l;
    canTpdo2ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo2ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 4"), l, 0);
    glCAN->addWidget(canTpdo2BCombo = new QComboBox(), l, 1); ++l;
    canTpdo2BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo2BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 5"), l, 0);
    glCAN->addWidget(canTpdo3ACombo = new QComboBox(), l, 1); ++l;
    canTpdo3ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo3ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 6"), l, 0);
    glCAN->addWidget(canTpdo3BCombo = new QComboBox(), l, 1); ++l;
    canTpdo3BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo3BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 7"), l, 0);
    glCAN->addWidget(canTpdo4ACombo = new QComboBox(), l, 1); ++l;
    canTpdo4ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo4ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 8"), l, 0);
    glCAN->addWidget(canTpdo4BCombo = new QComboBox(), l, 1); ++l;
    canTpdo4BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo4BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 9"), l, 0);
    glCAN->addWidget(canTpdo5ACombo = new QComboBox(), l, 1); ++l;
    canTpdo5ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo5ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 10"), l, 0);
    glCAN->addWidget(canTpdo5BCombo = new QComboBox(), l, 1); ++l;
    canTpdo5BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo5BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 11"), l, 0);
    glCAN->addWidget(canTpdo6ACombo = new QComboBox(), l, 1); ++l;
    canTpdo6ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo6ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 12"), l, 0);
    glCAN->addWidget(canTpdo6BCombo = new QComboBox(), l, 1); ++l;
    canTpdo6BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo6BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 13"), l, 0);
    glCAN->addWidget(canTpdo7ACombo = new QComboBox(), l, 1); ++l;
    canTpdo7ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo7ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 14"), l, 0);
    glCAN->addWidget(canTpdo7BCombo = new QComboBox(), l, 1); ++l;
    canTpdo7BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo7BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 15"), l, 0);
    glCAN->addWidget(canTpdo8ACombo = new QComboBox(), l, 1); ++l;
    canTpdo8ACombo->addItems(canBusMappingStringLists);
    connect(canTpdo8ACombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    glCAN->addWidget(new QLabel("Channel 16"), l, 0);
    glCAN->addWidget(canTpdo8BCombo = new QComboBox(), l, 1); ++l;
    canTpdo8BCombo->addItems(canBusMappingStringLists);
    connect(canTpdo8BCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCanMapping(int)));

    // Fliter
    QGridLayout* glFilter = new QGridLayout();
    l = 0;
    glFilter->addWidget(new QLabel("Filter mode:"), l, 0);
    glFilter->addWidget(filterModeCombo = new QComboBox(), l, 1); ++l;
    filterModeCombo->addItem(QString("Gyr only"));
    filterModeCombo->addItem(QString("Gyr + Acc (Kalman)"));
    filterModeCombo->addItem(QString("Gyr + Acc + Mag (Kalman)"));
    filterModeCombo->addItem(QString("Gyr + Acc (DCM)"));
    filterModeCombo->addItem(QString("Gyr + Acc + Mag (DCM)"));
    connect(filterModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateFilterMode(int)));

    glFilter->addWidget(new QLabel("GYR AutoCalibration:"), l, 0);
    glFilter->addWidget(gyroAutoCalibrationCombo = new QComboBox(), l, 1); ++l;
    gyroAutoCalibrationCombo->addItem(QString("Disable"));
    gyroAutoCalibrationCombo->addItem(QString("Enable"));
    connect(gyroAutoCalibrationCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyroAutoCalibration(int)));

    glFilter->addWidget(new QLabel("Offset Mode:"), l, 0);
    glFilter->addWidget(offsetModeCombo = new QComboBox(), l, 1); ++l;
    offsetModeCombo->addItem(QString("Object reset"));
    offsetModeCombo->addItem(QString("Heading reset"));
    offsetModeCombo->addItem(QString("Alignment reset"));
    connect(offsetModeCombo, SIGNAL(activated(int)), this, SLOT(updateOffsetMode(int)));

    glFilter->addWidget(btnResetOffset = new QPushButton(), l, 1); ++l;
    btnResetOffset->setText(QString("Reset Offset"));
    connect(btnResetOffset, SIGNAL(clicked()), this, SLOT(updateResetOffset()));

    //Uart
    QGridLayout* glUart = new QGridLayout();
    l = 0;
    glUart->addWidget(new QLabel("Baud rate:"), l, 0);
    glUart->addWidget(baudRateCombo = new QComboBox(), l, 1); ++l;
    baudRateCombo->addItem(QString("115200 bps"));
    baudRateCombo->addItem(QString("230400 bps"));
    baudRateCombo->addItem(QString("256000 bps"));
    baudRateCombo->addItem(QString("460800 bps"));
    baudRateCombo->addItem(QString("921600 bps"));
    connect(baudRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBaudRateIndex(int)));

    glUart->addWidget(new QLabel("Data format:"), l, 0);
    glUart->addWidget(uartFormatCombo = new QComboBox(), l, 1); ++l;
    uartFormatCombo->addItem(QString("LPBUS (binary)"));
    uartFormatCombo->addItem(QString("ASCII (CSV)"));
    connect(uartFormatCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateUartFormatIndex(int)));

    //GPS Data
    QGridLayout* glGpsData = new QGridLayout();
    l = 0;
    glGpsData->addWidget(new QLabel("Save GPS to flash:"), l, 0);
    glGpsData->addWidget(saveGpsState = new QPushButton("Save GPS State"), l, 1); ++l;
    connect(saveGpsState, SIGNAL(clicked()), this, SLOT(updateSaveGpsState()));

    glGpsData->addWidget(clearGpsState = new QPushButton("Clear GPS State"), l, 1); ++l;
    connect(clearGpsState, SIGNAL(clicked()), this, SLOT(updateClearGpsState()));

    glGpsData->addWidget(new QLabel("Gps data:"), l, 0);
    glGpsData->addWidget(selectGpsTimestamp = new QCheckBox("Timestamp"), l, 1); ++l; selectGpsTimestamp->setEnabled(false);
    glGpsData->addWidget(selectPVT_iTOW = new QCheckBox("PVT iTOW(ms)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_YMD = new QCheckBox("PVT YMD"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_HMS = new QCheckBox("PVT HMS"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_vaild = new QCheckBox("PVT valid"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_tAcc = new QCheckBox("PVT tAcc(ns)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_nano = new QCheckBox("PVT nano(ns)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_fixType = new QCheckBox("PVT fixType"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_flags = new QCheckBox("PVT flags"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_flags2 = new QCheckBox("PVT flags2"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_numSV = new QCheckBox("PVT numSV"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_longitude = new QCheckBox("PVT longitude"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_latitude = new QCheckBox("PVT latitude"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_height = new QCheckBox("PVT height(mm)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_hMSL = new QCheckBox("PVT hMSL(mm)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_hAcc = new QCheckBox("PVT hAcc(mm)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_vAcc = new QCheckBox("PVT vAcc(mm)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_velN = new QCheckBox("PVT velN(mm/s)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_velE = new QCheckBox("PVT velE(mm/s)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_velD = new QCheckBox("PVT velD(mm/s)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_gSpeed = new QCheckBox("PVT gSpeed(mm/s)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_headMot = new QCheckBox("PVT headMot(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_sAcc = new QCheckBox("PVT sAcc(mm/s)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_headAcc = new QCheckBox("PVT headAcc(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_pDOP = new QCheckBox("PVT pDOP"), l, 1); ++l;
    glGpsData->addWidget(selectPVT_headVeh = new QCheckBox("PVT headVeh(deg)"), l, 1); ++l;

    glGpsData->addWidget(selectATT_ITOW = new QCheckBox("ATT iTOW(ms)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_version = new QCheckBox("ATT version"), l, 1); ++l;
    glGpsData->addWidget(selectATT_roll = new QCheckBox("ATT roll(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_pitch = new QCheckBox("ATT pitch(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_heading = new QCheckBox("ATT heading(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_accRoll = new QCheckBox("ATT accRoll(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_accPitch = new QCheckBox("ATT accPitch(deg)"), l, 1); ++l;
    glGpsData->addWidget(selectATT_accHeading = new QCheckBox("ATT accHeading(deg)"), l, 1); ++l;

    glGpsData->addWidget(selectESF_iTOW= new QCheckBox("ESF iTOW(ms)"), l, 1); ++l;
    glGpsData->addWidget(selectESF_version = new QCheckBox("ESF version"), l, 1); ++l;
    glGpsData->addWidget(selectESF_initStatus1 = new QCheckBox("ESF initStatus1"), l, 1); ++l;
    glGpsData->addWidget(selectESF_initStatus2 = new QCheckBox("ESF initStatus2"), l, 1); ++l;
    glGpsData->addWidget(selectESF_fusionMode = new QCheckBox("ESF fusionMode"), l, 1); ++l;
    glGpsData->addWidget(selectESF_numnSens = new QCheckBox("ESF numSens"), l, 1); ++l;
    glGpsData->addWidget(selectESF_sensStatus = new QCheckBox("ESF sensStatus"), l, 1); ++l;

    glGpsData->addWidget(selectGPS_udrStatus = new QCheckBox("GPS udrStatus"), l, 1); ++l;

    glGpsData->addWidget(btnSaveSelectGpsData = new QPushButton(this), l, 1); ++l;
    btnSaveSelectGpsData->setText("Set GPS Data");
    connect(btnSaveSelectGpsData, SIGNAL(clicked()), this, SLOT(saveSelectGpsData()));

    // IMU Data
    QGridLayout* glImuData = new QGridLayout();
    l = 0;

    glImuData->addWidget(new QLabel("Data precision:"), l, 0);
    glImuData->addWidget(uartDataPrecision = new QComboBox(), l, 1); ++l;
    uartDataPrecision->addItem(QString("16-bit integer"));
    uartDataPrecision->addItem(QString("32-bit floating point"));
    connect(uartDataPrecision, SIGNAL(currentIndexChanged(int)), this, SLOT(updateUartDataPrecision(int)));

    glImuData->addWidget(new QLabel("Enabled data:"), l, 0);
    glImuData->addWidget(selectTimestamp = new QCheckBox("Timestamp"), l, 1); ++l; selectTimestamp->setEnabled(false);
    glImuData->addWidget(selectAccRaw = new QCheckBox("Acc Raw"), l, 1); ++l;
    glImuData->addWidget(selectAccCalibrated = new QCheckBox("Acc Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectGyr0Raw = new QCheckBox("GyroI Raw"), l, 1); ++l;
    glImuData->addWidget(selectGyr1Raw = new QCheckBox("GyroII Raw"), l, 1); ++l;
    glImuData->addWidget(selectGyr0BiasCalibrated = new QCheckBox("GyroI Bias Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectGyr1BiasCalibrated = new QCheckBox("GyroII Bias Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectGyr0AlignmentCalibrated = new QCheckBox("GyroI Align. Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectGyr1AlignmentCalibrated = new QCheckBox("GyroII Align. Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectMagRaw = new QCheckBox("Mag Raw"), l, 1); ++l;
    glImuData->addWidget(selectMagCalibrated = new QCheckBox("Mag Calibrated"), l, 1); ++l;
    glImuData->addWidget(selectAngularVelocity = new QCheckBox("Angular Vel."), l, 1); ++l;
    glImuData->addWidget(selectQuaternion = new QCheckBox("Quaternion"), l, 1); ++l;
    glImuData->addWidget(selectEuler = new QCheckBox("Euler"), l, 1); ++l;
    glImuData->addWidget(selectLinAcc = new QCheckBox("Linear Acc."), l, 1); ++l;
    glImuData->addWidget(selectPressure = new QCheckBox("Pressure"), l, 1); ++l;
    glImuData->addWidget(selectAltitude = new QCheckBox("Altitude"), l, 1); ++l; selectAltitude->setEnabled(false);
    glImuData->addWidget(selectTemperature = new QCheckBox("Temperature"), l, 1); ++l;
    glImuData->addWidget(btnSaveSelectImuData = new QPushButton(this), l, 1); ++l;
    btnSaveSelectImuData->setText("Save");
    connect(btnSaveSelectImuData, SIGNAL(clicked()), this, SLOT(saveSelectImuData()));

    //////////////////////
    // widget sampling rate
    QWidget *widgetIDSamplingRate = new QWidget();
    widgetIDSamplingRate->setLayout(glIdSamplingRate);
    QTreeWidgetItem* connectionTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("ID / sampling rate")));
    QTreeWidgetItem* connectionTreeWidget = new QTreeWidgetItem(connectionTreeItem);
    connectionTreeItem->setExpanded(true);
    tree->setItemWidget(connectionTreeWidget, 0, widgetIDSamplingRate);

    // widget range
    QWidget *widgetRange = new QWidget();
    widgetRange->setLayout(glRange);
    QTreeWidgetItem* rangeTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("Range")));
    QTreeWidgetItem* rangeTreeWidget = new QTreeWidgetItem(rangeTreeItem);
    rangeTreeItem->setExpanded(true);
    tree->setItemWidget(rangeTreeWidget, 0, widgetRange);

    //CAN bus
    QWidget *widgetCAN = new QWidget();
    widgetCAN->setLayout(glCAN);
    QTreeWidgetItem* canTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("CAN bus")));
    QTreeWidgetItem* canTreeWidget = new QTreeWidgetItem(canTreeItem);
    tree->setItemWidget(canTreeWidget, 0, widgetCAN);

    //Filter Mode
    QWidget *widgetFilter = new QWidget();
    widgetFilter->setLayout(glFilter);
    QTreeWidgetItem* filterTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("Filter Mode")));
    QTreeWidgetItem* filterTreeWidget = new QTreeWidgetItem(filterTreeItem);
    tree->setItemWidget(filterTreeWidget, 0, widgetFilter);

    //Uart
    QWidget *widgetUart = new QWidget();
    widgetUart->setLayout(glUart);
    QTreeWidgetItem* uartTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("UART")));
    QTreeWidgetItem* uartTreeWidget = new QTreeWidgetItem(uartTreeItem);
    //uartTreeItem->setExpanded(true);
    tree->setItemWidget(uartTreeWidget, 0, widgetUart);

    //GPS Data
    QWidget *widgetGpsData = new QWidget();
    widgetGpsData->setLayout(glGpsData);
    QTreeWidgetItem* gpsDataTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("GPS data")));
    QTreeWidgetItem* gpsDataTreeWidget = new QTreeWidgetItem(gpsDataTreeItem);
    //gpsDataTreeWidget->setExpanded(true);
    tree->setItemWidget(gpsDataTreeWidget, 0, widgetGpsData);

    //IMU Data
    QWidget *widgetData = new QWidget();
    widgetData->setLayout(glImuData);
    QTreeWidgetItem* imuDataTreeItem = new QTreeWidgetItem(getTreeItem(), QStringList(QString("Imu data")));
    QTreeWidgetItem* imuDataTreeWidget = new QTreeWidgetItem(imuDataTreeItem);
    imuDataTreeItem->setExpanded(true);
    tree->setItemWidget(imuDataTreeWidget, 0, widgetData);
    containerSetup = true;

    setupSensorGuiContainer();
}

void SensorGuiContainerIg1::lastFieldMap(float outFieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW][3]) const
{
    for (int i = 0; i < ABSMAXPITCH; ++i)
        for (int j = 0; j < ABSMAXROLL; ++j)
            for (int k = 0; k < ABSMAXYAW; ++k)
                for (int l = 0; l < 3; ++l)
                    outFieldMap[i][j][k][l] = m_fieldMap[i][j][k].data[l];
}

void SensorGuiContainerIg1::setLastFieldMap(const LpVector3f fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW])
{
    for (int i = 0; i < ABSMAXPITCH; ++i)
        for (int j = 0; j < ABSMAXROLL; ++j)
            for (int k = 0; k < ABSMAXYAW; ++k)
                m_fieldMap[i][j][k] = fieldMap[i][j][k];

    m_hasFieldMap = true;
}

void SensorGuiContainerIg1::setupSensorGuiContainer()
{
    /* if (containerSetup)
        return;

    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_AccSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_AccSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                accRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + "G", range);
    }
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_GyrSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_GyrSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                gyrRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + " dps", range);
    }
    connect(gyrRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGyrRange(int)));
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_MagSupportedRanges, nullptr, 0);

        std::vector<int32_t> ranges(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_MagSupportedRanges, ranges.data(), bufferSize);
        if (!error)
            for (int32_t range : ranges)
                magRangeCombo->addItem(QString::fromStdString(std::to_string(range)) + " Gauss", range);
    }
    connect(magRangeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateMagRange(int)));
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<int32_t>(ZenImuProperty_SupportedSamplingRates, nullptr, 0);

        std::vector<int32_t> rates(bufferSize);
        auto [error, __] = m_imu.getArrayProperty(ZenImuProperty_SupportedSamplingRates, rates.data(), bufferSize);
        if (error == ZenError_None)
            for (int32_t rate : rates)
                samplingRateCombo->addItem(QString::fromStdString(std::to_string(rate)) + " Hz", rate);
    }
    {
        auto [_, bufferSize] = m_imu.getArrayProperty<std::byte>(ZenImuProperty_SupportedFilterModes, nullptr, 0);

        std::vector<char> json(bufferSize);
        auto [error, length] = m_imu.getArrayProperty(ZenImuProperty_SupportedFilterModes, reinterpret_cast<std::byte*>(json.data()), bufferSize);
        if (!error)
        {
            rapidjson::Document doc;
            doc.Parse(json.data(), json.size());

            if (!doc.HasParseError() && doc.HasMember("config") && doc["config"].IsArray())
            {
                for (auto it = doc["config"].Begin(); it != doc["config"].End(); ++it)
                {
                    const auto& pair{ *it };
                    filterModeCombo->addItem(pair["key"].GetString(), pair["value"].GetInt());
                }
            }
        }
    } */
    /* {
        auto [error, bufferSize] = m_sensor.getArrayProperty<int32_t>(ZenSensorProperty_SupportedBaudRates, nullptr, 0);
        if (error != ZenError_UnknownProperty)
        {
            std::vector<int32_t> rates(bufferSize);
            auto [error2, _] = m_sensor.getArrayProperty(ZenSensorProperty_SupportedBaudRates, rates.data(), bufferSize);
            if (!error2)
            {
                for (int32_t rate : rates)
                    baudRateCombo->addItem(QString::fromStdString(std::to_string(rate)) + " bits/s", rate);

                connect(baudRateCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBaudRateIndex(int)));
            }
        }
    } */

    containerSetup = true;
}

#define MAX_CAN_CHANNELS 16
void SensorGuiContainerIg1::updateData()
{
    bool b = true;
    indexItem->blockSignals(b);
    samplingRateCombo->blockSignals(b);
    gyrRangeCombo->blockSignals(b);
    accRangeCombo->blockSignals(b);
    magRangeCombo->blockSignals(b);
    degradOutputCombo->blockSignals(b);

    canBaudrateCombo->blockSignals(b);
    canChannelModeCombo->blockSignals(b);
    canDataPrecisionCombo->blockSignals(b);
    canStartIdSpin->blockSignals(b);
    canHeartbeatCombo->blockSignals(b);
    canTpdo1ACombo->blockSignals(b);
    canTpdo1BCombo->blockSignals(b);
    canTpdo2ACombo->blockSignals(b);
    canTpdo2BCombo->blockSignals(b);
    canTpdo3ACombo->blockSignals(b);
    canTpdo3BCombo->blockSignals(b);
    canTpdo4ACombo->blockSignals(b);
    canTpdo4BCombo->blockSignals(b);
    canTpdo5ACombo->blockSignals(b);
    canTpdo5BCombo->blockSignals(b);
    canTpdo6ACombo->blockSignals(b);
    canTpdo6BCombo->blockSignals(b);
    canTpdo7ACombo->blockSignals(b);
    canTpdo7BCombo->blockSignals(b);
    canTpdo8ACombo->blockSignals(b);
    canTpdo8BCombo->blockSignals(b);

    filterModeCombo->blockSignals(b);
    gyroAutoCalibrationCombo->blockSignals(b);
    offsetModeCombo->blockSignals(b);
    btnResetOffset->blockSignals(b);

    baudRateCombo->blockSignals(b);
    uartFormatCombo->blockSignals(b);
    uartDataPrecision->blockSignals(b);

    getTreeItem()->setText(0, QString(m_sensorDesc.name) + " (" + QString(m_sensorDesc.ioType) + QString(") ID=%1").arg(m_openMatId));

    addressItem->setText(QString(m_sensorDesc.name));
    indexItem->setText(QString::fromStdString(std::to_string(m_openMatId)));

    {
        std::array<std::byte, 16> name;
        auto [error, _] = m_sensor.getArrayProperty(ZenSensorProperty_DeviceName, name.data(), name.size());
        if (!error)
        {
            std::string nameStr(reinterpret_cast<const char*>(&name[0]), std::end(name) - std::begin(name));
        }
    }



    /* indexItem->setCurrentIndex(settings.sensorId);

    switch(settings.dataStreamFrequency)
    {
    case DATA_STREAM_FREQ_5HZ:
        samplingRateCombo->setCurrentIndex(0);
        break;
    case DATA_STREAM_FREQ_10HZ:
        samplingRateCombo->setCurrentIndex(1);
        break;
    case DATA_STREAM_FREQ_50HZ:
        samplingRateCombo->setCurrentIndex(2);
        break;
    case DATA_STREAM_FREQ_100HZ:
        samplingRateCombo->setCurrentIndex(3);
        break;
    case DATA_STREAM_FREQ_250HZ:
        samplingRateCombo->setCurrentIndex(4);
        break;
    case DATA_STREAM_FREQ_500HZ:
        samplingRateCombo->setCurrentIndex(5);
        break;
    default:
        samplingRateCombo->setCurrentIndex(3);
        break;
    }
    
    switch (settings.gyroRange) 
    {
    case GYR_RANGE_400DPS:
        gyrRangeCombo->setCurrentIndex(0);
        break;
    case GYR_RANGE_1000DPS:
        gyrRangeCombo->setCurrentIndex(1);
        break;
    case GYR_RANGE_2000DPS:
        gyrRangeCombo->setCurrentIndex(2);
        break;
    default:
        gyrRangeCombo->setCurrentIndex(0);
        break;
    }

    switch (settings.accRange)
    {
    case ACC_RANGE_2G:
        accRangeCombo->setCurrentIndex(0);
        break;
    case ACC_RANGE_4G:
        accRangeCombo->setCurrentIndex(1);
        break;
    case ACC_RANGE_8G:
        accRangeCombo->setCurrentIndex(2);
        break;
    case ACC_RANGE_16G:
        accRangeCombo->setCurrentIndex(3);
        break;
    default:
        accRangeCombo->setCurrentIndex(0);
        break;
    }

    switch (settings.magRange)
    {
    case MAG_RANGE_2GUASS:
        magRangeCombo->setCurrentIndex(0);
        break;
    case MAG_RANGE_8GUASS:
        magRangeCombo->setCurrentIndex(1);
        break;
    default:
        magRangeCombo->setCurrentIndex(0);
        break;
    } */

    /* if (settings.useRadianOutput) {
        degradOutputCombo->setCurrentIndex(1);
    }
    else {
        degradOutputCombo->setCurrentIndex(0);
    } */

    {
        std::vector<int32_t> channelSettings(MAX_CAN_CHANNELS);
        auto [error, _] = m_imu.getArrayProperty(ZenImuProperty_CanMapping, channelSettings.data(), channelSettings.size());
        if (!error)
        {
            for (int i = 0; i < MAX_CAN_CHANNELS; ++i)
            {
                std::cout << "can map get=" << i << " " << channelSettings[i] << std::endl;
                canTpdoCombo[i]->setCurrentIndex(channelSettings[i]);
            }
        }
        else
            std::cout << "error get can map: " << error << std::endl;
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanChannelMode);
        if (!error)
            canChannelModeCombo->setCurrentIndex(value);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanPointMode);
        if (!error)
            canDataPrecisionCombo->setCurrentIndex(value);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanStartId);
        if (!error)
            canStartIdSpin->setValue(value);
    }
    {
        auto [error, value] = m_imu.getFloatProperty(ZenImuProperty_CanHeartbeat);
        if (!error)
            if (value <= 0.5)
                canHeartbeatCombo->setCurrentIndex(0);
            else if (value <= 1.0)
                canHeartbeatCombo->setCurrentIndex(1);
            else if (value <= 2.0)
                canHeartbeatCombo->setCurrentIndex(2);
            else if (value <= 5.0)
                canHeartbeatCombo->setCurrentIndex(3);
            else
                canHeartbeatCombo->setCurrentIndex(4);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_CanBaudrate);
        if (!error)
            if (value <= 10000)
                canBaudrateCombo->setCurrentIndex(0);
            else if (value <= 20000)
                canBaudrateCombo->setCurrentIndex(1);
            else if (value <= 50000)
                canBaudrateCombo->setCurrentIndex(2);
            else if (value <= 125000)
                canBaudrateCombo->setCurrentIndex(3);
            else if (value <= 250000)
                canBaudrateCombo->setCurrentIndex(4);
            else if (value <= 500000)
                canBaudrateCombo->setCurrentIndex(5);
            else if (value <= 800000)
                canBaudrateCombo->setCurrentIndex(6);
            else
                canBaudrateCombo->setCurrentIndex(7);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_UartBaudRate);
        if (!error)
            if (value <= 19200)
                baudRateCombo->setCurrentIndex(0);
            else if (value <= 38400)
                baudRateCombo->setCurrentIndex(1);
            else if (value <= 57600)
                baudRateCombo->setCurrentIndex(3);
            else if (value <= 115200)
                baudRateCombo->setCurrentIndex(4);
            else if (value <= 230400)
                baudRateCombo->setCurrentIndex(5);
            else if (value <= 256000)
                baudRateCombo->setCurrentIndex(6);
            else if (value <= 460800)
                baudRateCombo->setCurrentIndex(7);
            else
                baudRateCombo->setCurrentIndex(8);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_UartFormat);
        if (!error)
            uartFormatCombo->setCurrentIndex(value);
    }

    {
        std::array<int32_t, 3> version;
        auto [error, _] = m_sensor.getArrayProperty(ZenSensorProperty_FirmwareVersion, version.data(), version.size());
        if (!error)
        {
            const std::string firmwareStr = std::to_string(version[0]) + "." + std::to_string(version[1]) + "." + std::to_string(version[2]);
            firmwareItem->setText(QString::fromStdString(firmwareStr));
        }
    }
    {
        auto [error, enabled] = m_imu.getBoolProperty(ZenImuProperty_GyrUseAutoCalibration);
        if (!error)
            gyroAutoCalibrationCombo->setCurrentIndex(enabled ? 1 : 0);
    }
    {
        auto [error, value] = m_imu.getInt32Property(ZenImuProperty_FilterMode);
        if (!error)
            filterModeCombo->setCurrentIndex(value);
    }

    runningItem->setText("<font color='green'>started</font>");

    /* //CAN Bus
    // CAN Baudrate
    switch (settings.canBaudrate)
    {
    case LPMS_CAN_BAUDRATE_1M:
        canBaudrateCombo->setCurrentIndex(0);
        break;
    case LPMS_CAN_BAUDRATE_800K:
        canBaudrateCombo->setCurrentIndex(1);
        break;
    case LPMS_CAN_BAUDRATE_500K:
        canBaudrateCombo->setCurrentIndex(2);
        break;
    case LPMS_CAN_BAUDRATE_250K:
        canBaudrateCombo->setCurrentIndex(3);
        break;
    case LPMS_CAN_BAUDRATE_125K:
        canBaudrateCombo->setCurrentIndex(4);
        break;

    default:
        canBaudrateCombo->setCurrentIndex(0);
        break;
    }

    // CAN Channel mode
    if (settings.canMode < 2)
        canChannelModeCombo->setCurrentIndex(settings.canMode);


    // CAN Data Precision
    if (settings.canDataPrecision < 2)
        canDataPrecisionCombo->setCurrentIndex(settings.canDataPrecision);

    // CAN Start ID
    canStartIdSpin->setValue(settings.canStartId);

    // CAN Mapping
    canTpdo1ACombo->setCurrentIndex(settings.canMapping[0]);
    canTpdo1BCombo->setCurrentIndex(settings.canMapping[1]);
    canTpdo2ACombo->setCurrentIndex(settings.canMapping[2]);
    canTpdo2BCombo->setCurrentIndex(settings.canMapping[3]);
    canTpdo3ACombo->setCurrentIndex(settings.canMapping[4]);
    canTpdo3BCombo->setCurrentIndex(settings.canMapping[5]);
    canTpdo4ACombo->setCurrentIndex(settings.canMapping[6]);
    canTpdo4BCombo->setCurrentIndex(settings.canMapping[7]);
    canTpdo5ACombo->setCurrentIndex(settings.canMapping[8]);
    canTpdo5BCombo->setCurrentIndex(settings.canMapping[9]);
    canTpdo6ACombo->setCurrentIndex(settings.canMapping[10]);
    canTpdo6BCombo->setCurrentIndex(settings.canMapping[11]);
    canTpdo7ACombo->setCurrentIndex(settings.canMapping[12]);
    canTpdo7BCombo->setCurrentIndex(settings.canMapping[13]);
    canTpdo8ACombo->setCurrentIndex(settings.canMapping[14]);
    canTpdo8BCombo->setCurrentIndex(settings.canMapping[15]);


    // CAN HeartBeat
    switch (settings.canHeartbeatTime)
    {
    case LPMS_CAN_HEARTBEAT_005:
        canHeartbeatCombo->setCurrentIndex(0);
        break;

    case LPMS_CAN_HEARTBEAT_010:
        canHeartbeatCombo->setCurrentIndex(1);
        break;

    case LPMS_CAN_HEARTBEAT_020:
        canHeartbeatCombo->setCurrentIndex(2);
        break;

    case LPMS_CAN_HEARTBEAT_050:
        canHeartbeatCombo->setCurrentIndex(3);
        break;

    case LPMS_CAN_HEARTBEAT_100:
        canHeartbeatCombo->setCurrentIndex(4);
        break;

    default:
        canHeartbeatCombo->setCurrentIndex(1);
        break;
    }

    //Filter Mode
    if (settings.canDataPrecision < 5)
         filterModeCombo->setCurrentIndex(settings.filterMode);

    if(settings.enableGyroAutocalibration <2)
        gyroAutoCalibrationCombo->setCurrentIndex(settings.enableGyroAutocalibration);

    if (settings.offsetMode < 3)
        offsetModeCombo->setCurrentIndex(settings.offsetMode);

    //Uart communication
    switch (settings.uartBaudrate)
    {
    case LPMS_UART_BAUDRATE_115200:
        baudRateCombo->setCurrentIndex(0);
        break;

    case LPMS_UART_BAUDRATE_230400:
        baudRateCombo->setCurrentIndex(1);
        break;

    case LPMS_UART_BAUDRATE_256000:
        baudRateCombo->setCurrentIndex(2);
        break;

    case LPMS_UART_BAUDRATE_460800:
        baudRateCombo->setCurrentIndex(3);
        break;

    case LPMS_UART_BAUDRATE_921600:
        baudRateCombo->setCurrentIndex(4);
        break;

    default:
        baudRateCombo->setCurrentIndex(4);
        break;
    }

    if (settings.uartDataFormat < 2)
        uartFormatCombo->setCurrentIndex(settings.uartDataFormat);


    //GPS Data
    selectGpsTimestamp->setChecked(true);
    selectPVT_iTOW->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_ITOW_ENABLE);
    selectPVT_YMD->setChecked((settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_YEAR_ENABLE) ||
                              (settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_MONTH_ENABLE) || 
                              (settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_DAY_ENABLE));

    selectPVT_HMS->setChecked((settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HOUR_ENABLE) || 
                              (settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_MIN_ENABLE) || 
                              (settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_SEC_ENABLE));

    selectPVT_vaild->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_VALID_ENABLE);
    selectPVT_tAcc->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_TACC_ENABLE);
    selectPVT_nano->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_NANO_ENABLE);
    selectPVT_fixType->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_FIXTYPE_ENABLE);
    selectPVT_flags->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_FLAGS_ENABLE);
    selectPVT_flags2->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_FLAGS2_ENABLE);
    selectPVT_numSV->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_NUMSV_ENABLE);
    selectPVT_longitude->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_LONGITUDE_ENABLE);
    selectPVT_latitude->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_LATITUDE_ENABLE);
    selectPVT_height->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HEIGHT_ENABLE);
    selectPVT_hMSL->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HMSL_ENABLE);
    selectPVT_hAcc->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HACC_ENABLE);   
    selectPVT_vAcc->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_VACC_ENABLE);   
    selectPVT_velN->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_VELN_ENABLE);   
    selectPVT_velE->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_VELE_ENABLE);   
    selectPVT_velD->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_VELD_ENABLE);   
    selectPVT_gSpeed->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_GSPEED_ENABLE);
    selectPVT_headMot->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HEADMOT_ENABLE);
    selectPVT_sAcc->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_SACC_ENABLE);
    selectPVT_headAcc->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HEADACC_ENABLE);
    selectPVT_pDOP->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_PDOP_ENABLE);
    selectPVT_headVeh->setChecked(settings.gpsTransmitDataConfig[0] & GPS_NAV_PVT_HEADVEH_ENABLE);

    selectATT_ITOW->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_ITOW_ENABLE);
    selectATT_version->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_VERSION_ENABLE);
    selectATT_roll->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_ROLL_ENABLE);
    selectATT_pitch->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_PITCH_ENABLE);
    selectATT_heading->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_HEADING_ENABLE);
    selectATT_accRoll->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_ACCROLL_ENABLE);
    selectATT_accPitch->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_ACCPITCH_ENABLE);
    selectATT_accHeading->setChecked(settings.gpsTransmitDataConfig[1] & GPS_NAV_ATT_ACCHEADING_ENABLE);

    selectESF_iTOW->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_ITOW_ENABLE);
    selectESF_version->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_VERSION_ENABLE);
    selectESF_initStatus1->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_INITSTATUS1_ENABLE);
    selectESF_initStatus2->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_INITSTATUS2_ENABLE);
    selectESF_fusionMode->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_FUSIONMODE_ENABLE);
    selectESF_numnSens->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_NUMSENS_ENABLE);
    selectESF_sensStatus->setChecked(settings.gpsTransmitDataConfig[1] & GPS_ESF_STATUS_SENSSTATUS_ENABLE);

    selectGPS_udrStatus->setChecked(settings.gpsTransmitDataConfig[1] & GPS_UDR_STATUS_ENABLE);

    // Imu uart
    if (settings.uartDataPrecision < 2)
         uartDataPrecision->setCurrentIndex(settings.uartDataPrecision);

	// Imu Data
    selectTimestamp->setChecked(true);
    selectAccRaw->setChecked(settings.transmitDataConfig & TDR_ACC_RAW_OUTPUT_ENABLED);
    selectAccCalibrated->setChecked(settings.transmitDataConfig & TDR_ACC_CALIBRATED_OUTPUT_ENABLED);
    selectGyr0Raw->setChecked(settings.transmitDataConfig & TDR_GYR0_RAW_OUTPUT_ENABLED);
    selectGyr1Raw->setChecked(settings.transmitDataConfig & TDR_GYR1_RAW_OUTPUT_ENABLED);
    selectGyr0BiasCalibrated->setChecked(settings.transmitDataConfig & TDR_GYR0_BIAS_CALIBRATED_OUTPUT_ENABLED);
    selectGyr1BiasCalibrated->setChecked(settings.transmitDataConfig & TDR_GYR1_BIAS_CALIBRATED_OUTPUT_ENABLED);
    selectGyr0AlignmentCalibrated->setChecked(settings.transmitDataConfig & TDR_GYR0_ALIGN_CALIBRATED_OUTPUT_ENABLED);
    selectGyr1AlignmentCalibrated->setChecked(settings.transmitDataConfig & TDR_GYR1_ALIGN_CALIBRATED_OUTPUT_ENABLED);
    selectMagRaw->setChecked(settings.transmitDataConfig & TDR_MAG_RAW_OUTPUT_ENABLED);
    selectMagCalibrated->setChecked(settings.transmitDataConfig & TDR_MAG_CALIBRATED_OUTPUT_ENABLED);
    selectAngularVelocity->setChecked(settings.transmitDataConfig & TDR_ANGULAR_VELOCITY_OUTPUT_ENABLED);
    selectQuaternion->setChecked(settings.transmitDataConfig & TDR_QUAT_OUTPUT_ENABLED);
    selectEuler->setChecked(settings.transmitDataConfig & TDR_EULER_OUTPUT_ENABLED);
    selectLinAcc->setChecked(settings.transmitDataConfig & TDR_LINACC_OUTPUT_ENABLED);
    selectPressure->setChecked(settings.transmitDataConfig & TDR_PRESSURE_OUTPUT_ENABLED);
    selectAltitude->setChecked(settings.transmitDataConfig & TDR_ALTITUDE_OUTPUT_ENABLED);
    selectTemperature->setChecked(settings.transmitDataConfig & TDR_TEMPERATURE_OUTPUT_ENABLED); */


    b = false;
    indexItem->blockSignals(b);
    samplingRateCombo->blockSignals(b);
    gyrRangeCombo->blockSignals(b);
    accRangeCombo->blockSignals(b);
    magRangeCombo->blockSignals(b);
    degradOutputCombo->blockSignals(b);

    canBaudrateCombo->blockSignals(b);
    canChannelModeCombo->blockSignals(b);
    canDataPrecisionCombo->blockSignals(b);
    canStartIdSpin->blockSignals(b);
    canHeartbeatCombo->blockSignals(b);
    canTpdo1ACombo->blockSignals(b);
    canTpdo1BCombo->blockSignals(b);
    canTpdo2ACombo->blockSignals(b);
    canTpdo2BCombo->blockSignals(b);
    canTpdo3ACombo->blockSignals(b);
    canTpdo3BCombo->blockSignals(b);
    canTpdo4ACombo->blockSignals(b);
    canTpdo4BCombo->blockSignals(b);
    canTpdo5ACombo->blockSignals(b);
    canTpdo5BCombo->blockSignals(b);
    canTpdo6ACombo->blockSignals(b);
    canTpdo6BCombo->blockSignals(b);
    canTpdo7ACombo->blockSignals(b);
    canTpdo7BCombo->blockSignals(b);
    canTpdo8ACombo->blockSignals(b);
    canTpdo8BCombo->blockSignals(b);

    filterModeCombo->blockSignals(b);
    gyroAutoCalibrationCombo->blockSignals(b);
    offsetModeCombo->blockSignals(b);
    btnResetOffset->blockSignals(b);

    baudRateCombo->blockSignals(b);
    uartFormatCombo->blockSignals(b);
    uartDataPrecision->blockSignals(b);
}

void SensorGuiContainerIg1::updateOpenMATIndex(int i)
{
    if (!containerSetup)
        return;
    //currentSensor->commandSetSensorID(i);
}

void SensorGuiContainerIg1::updatesamplingRate(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = DATA_STREAM_FREQ_5HZ;
        break;

    case 1:
        i = DATA_STREAM_FREQ_10HZ;
        break;

    case 2:
        i = DATA_STREAM_FREQ_50HZ;
        break;

    case 3:
        i = DATA_STREAM_FREQ_100HZ;
        break;

    case 4:
        i = DATA_STREAM_FREQ_250HZ;
        break;

    case 5:
        i = DATA_STREAM_FREQ_500HZ;
        break;

    default:
        i = DATA_STREAM_FREQ_100HZ;
        break;
    }
    currentSensor->commandSetSensorFrequency(i);*/
}


void SensorGuiContainerIg1::updateGyrRange(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = GYR_RANGE_400DPS;
        break;
    case 1:
        i = GYR_RANGE_1000DPS;
        break;
    case 2:
        i = GYR_RANGE_2000DPS;
        break;
    }
    currentSensor->commandSetSensorGyroRange(i);*/
}

void SensorGuiContainerIg1::updateAccRange(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = ACC_RANGE_2G;
        break;
    case 1:
        i = ACC_RANGE_4G;
        break;
    case 2:
        i = ACC_RANGE_8G;
        break;
    case 3:
        i = ACC_RANGE_16G;
        break;
    }
    currentSensor->commandSetSensorAccRange(i);*/
}

void SensorGuiContainerIg1::updateMagRange(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = MAG_RANGE_2GUASS;
        break;
    case 1:
        i = MAG_RANGE_8GUASS;
        break;
    }
    currentSensor->commandSetSensorMagRange(i);*/
}

void SensorGuiContainerIg1::updatedegradOutputCombo(int i) {
    if (!containerSetup)
        return;
    /*switch (i) {
    case 0:
        i = 0;
        break;
    case 1:
        i = 1;
        break;
    }
    currentSensor->commandSetSensorUseRadianOutput(i);*/
}

void SensorGuiContainerIg1::updateCanStartId(int i)
{
    if (!containerSetup)
        return;
    //currentSensor->commandSetCanStartId(i);
}

void SensorGuiContainerIg1::updateCanBaudrate(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = LPMS_CAN_BAUDRATE_1M;
        break;
    case 1:
        i = LPMS_CAN_BAUDRATE_800K;
        break;
    case 2:
        i = LPMS_CAN_BAUDRATE_500K;
        break;
    case 3:
        i = LPMS_CAN_BAUDRATE_250K;
        break;
    case 4:
        i = LPMS_CAN_BAUDRATE_125K;
        break;
    }
    currentSensor->commandSetCanBaudrate(i);*/
}

void SensorGuiContainerIg1::updateCanChannelMode(int i)
{
    if (!containerSetup)
        return;
    //currentSensor->commandSetCanChannelMode(i);
}

void SensorGuiContainerIg1::updateCanDataPrecision(int i)
{
    if (!containerSetup)
        return;
    //currentSensor->commandSetCanDataPrecision(i);
}


void SensorGuiContainerIg1::updateCanMapping(int i)
{
    if (!containerSetup)
        return;
    /*uint32_t map[16];
    int j = 0;
    map[j++] = canTpdo1ACombo->currentIndex();
    map[j++] = canTpdo1BCombo->currentIndex();
    map[j++] = canTpdo2ACombo->currentIndex();
    map[j++] = canTpdo2BCombo->currentIndex();
    map[j++] = canTpdo3ACombo->currentIndex();
    map[j++] = canTpdo3BCombo->currentIndex();
    map[j++] = canTpdo4ACombo->currentIndex();
    map[j++] = canTpdo4BCombo->currentIndex();
    map[j++] = canTpdo5ACombo->currentIndex();
    map[j++] = canTpdo5BCombo->currentIndex();
    map[j++] = canTpdo6ACombo->currentIndex();
    map[j++] = canTpdo6BCombo->currentIndex();
    map[j++] = canTpdo7ACombo->currentIndex();
    map[j++] = canTpdo7BCombo->currentIndex();
    map[j++] = canTpdo8ACombo->currentIndex();
    map[j++] = canTpdo8BCombo->currentIndex();

    currentSensor->commandSetCanMapping(map);*/
}

void SensorGuiContainerIg1::updateCanHeartbeat(int i)
{
    if (!containerSetup)
        return;
    /*switch (i)
    {
    case 0:
        i = LPMS_CAN_HEARTBEAT_005;
        break;
    case 1:
        i = LPMS_CAN_HEARTBEAT_010;
        break;
    case 2:
        i = LPMS_CAN_HEARTBEAT_020;
        break;
    case 3:
        i = LPMS_CAN_HEARTBEAT_050;
        break;
    case 4:
        i = LPMS_CAN_HEARTBEAT_100;
        break;
    }
    currentSensor->commandSetCanHeartbeat(i);*/
}

void SensorGuiContainerIg1::updateFilterMode(int i)
{
    if (!containerSetup)
        return;
    /*switch (i) {
    case 0:
        i = LPMS_FILTER_GYR;
        break;
    case 1:
        i = LPMS_FILTER_KALMAN_GYR_ACC;
        break;
    case 2:
        i = LPMS_FILTER_KALMAN_GYR_ACC_MAG;
        break;
    case 3:
        i = LPMS_FILTER_DCM_GYR_ACC;
        break;
    case 4:
        i = LPMS_FILTER_DCM_GYR_ACC_MAG;
        break;
    default:
        i = LPMS_FILTER_KALMAN_GYR_ACC;
        break;
    }
    currentSensor->commandSetFilterMode(i);*/
}

void SensorGuiContainerIg1::updateGyroAutoCalibration(int i)
{
    if (!containerSetup)
        return;
    //currentSensor->commandSetGyroAutoCalibration(i);
}

void SensorGuiContainerIg1::updateOffsetMode(int i)
{
    if (!containerSetup)
        return;
    /*switch (i) {
    case 0:
        i = LPMS_OFFSET_MODE_OBJECT;
        break;
    case 1:
        i = LPMS_OFFSET_MODE_HEADING;
        break;
    case 2:
        i = LPMS_OFFSET_MODE_ALIGNMENT;
        break;
    default:
        i = LPMS_OFFSET_MODE_OBJECT;
        break;
    }
    currentSensor->commandSetOffsetMode(i);*/
}

void SensorGuiContainerIg1::updateResetOffset()
{
    if (!containerSetup)
        return;
    //currentSensor->commandResetOffsetMode();
}

void SensorGuiContainerIg1::updateSaveGpsState()
{
    if (!containerSetup)
        return;
    //currentSensor->commandSaveGPSState();
}

void SensorGuiContainerIg1::updateClearGpsState()
{
    if (!containerSetup)
        return;
    //currentSensor->commandClearGPSState();
}

void SensorGuiContainerIg1::saveSelectGpsData()
{
    if (!containerSetup)
        return;
    /*uint32_t config = 0;
    int i = 0;
    if (selectPVT_iTOW->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_YMD->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_YMD->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_YMD->isChecked()) config = config | (1 << i); i++;
    
    if (selectPVT_HMS->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_HMS->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_HMS->isChecked()) config = config | (1 << i); i++;

    if (selectPVT_vaild->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_tAcc->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_nano->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_fixType->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_flags->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_flags2->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_numSV->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_longitude->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_latitude->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_height->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_hMSL->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_hAcc->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_vAcc->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_velN->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_velE->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_velD->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_gSpeed->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_headMot->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_sAcc->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_headAcc->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_pDOP->isChecked()) config = config | (1 << i); i++;
    if (selectPVT_headVeh->isChecked()) config = config | (1 << i); i++;
    
    uint32_t config1 = 0;
    i = 0;
    if (selectATT_ITOW->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_version->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_roll->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_pitch->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_heading->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_accRoll->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_accPitch->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectATT_accHeading->isChecked()) config1 = config1 | (1 << i); i++;
    
    if (selectESF_iTOW->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_version->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_initStatus1->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_initStatus2->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_fusionMode->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_numnSens->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectESF_sensStatus->isChecked()) config1 = config1 | (1 << i); i++;
    if (selectGPS_udrStatus->isChecked()) config1 = config1 | (1 << i); i++;

    currentSensor->commandSetGpsTransmitData(config, config1);*/
}


void SensorGuiContainerIg1::updateBaudRateIndex(int i)
{
	if (!containerSetup)
		return;
	/*switch (i)
	{
	case 0:
		i = LPMS_UART_BAUDRATE_115200;
		break;
	case 1:
		i = LPMS_UART_BAUDRATE_230400;
		break;
	case 2:
		i = LPMS_UART_BAUDRATE_256000;
		break;
	case 3:
		i = LPMS_UART_BAUDRATE_460800;
		break;
	case 4:
		i = LPMS_UART_BAUDRATE_921600;
		break;
	default:
		i = LPMS_UART_BAUDRATE_921600;
		break;
	}
	currentSensor->commandSetUartBaudRate(i);*/
}

void SensorGuiContainerIg1::updateUartFormatIndex(int i)
{
	if (!containerSetup)
		return;
	/*switch (i)
	{
	case 0:
		i = LPMS_UART_DATA_FORMAT_LPBUS;
		break;
	case 1:
		i = LPMS_UART_DATA_FORMAT_ASCII;
		break;
	}
	currentSensor->commandSetUartDataFormat(i);*/
}

void SensorGuiContainerIg1::updateUartDataPrecision(int i)
{
    if (!containerSetup)
        return;
	/*switch (i)
	{
	case 0:
		i = LPMS_UART_DATA_PRECISION_FIXED_POINT;
		break;
	case 1:
		i = LPMS_UART_DATA_PRECISION_FLOATING_POINT;
		break;
	}
    currentSensor->commandSetUartDataPrecision(i);*/
}

void SensorGuiContainerIg1::saveSelectImuData()
{
    if (!containerSetup)
        return;
    /*uint32_t config = 0;
    int i = 0;
    if (selectAccRaw->isChecked())
        config = config | (1 << i);
    i++;
    if (selectAccCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr0Raw->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr1Raw->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr0BiasCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr1BiasCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr0AlignmentCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectGyr1AlignmentCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectMagRaw->isChecked())
        config = config | (1 << i);
    i++;
    if (selectMagCalibrated->isChecked())
        config = config | (1 << i);
    i++;
    if (selectAngularVelocity->isChecked())
        config = config | (1 << i);
    i++;
    if (selectQuaternion->isChecked())
        config = config | (1 << i);
    i++;
    if (selectEuler->isChecked())
        config = config | (1 << i);
    i++;
    if (selectLinAcc->isChecked())
        config = config | (1 << i);
    i++;
    if (selectPressure->isChecked())
        config = config | (1 << i);
    i++;
    if (selectAltitude->isChecked())
        config = config | (1 << i);
    i++;
    if (selectTemperature->isChecked())
        config = config | (1 << i);
    i++;

    currentSensor->commandSetTransmitData(config);*/
}