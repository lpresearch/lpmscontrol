/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#ifndef DISCOVERY_TREE
#define DISCOVERY_TREE

#include <QComboBox>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QProgressDialog>
#include <QTimer>

#ifdef _WIN32
#include "windows.h"
#endif

#include "OpenZen.h"

#include "DiscoveryItem.h"

#include <atomic>
#include <thread>
#include <list>
#include <string>
#include <iostream>
#include <fstream>

#define PREFERRED_DEVICES_FILE "LpmsControlPreferredDevices.txt"

class DiscoveryTree : public QTreeWidget {
    Q_OBJECT

public:
    DiscoveryTree(zen::ZenClient& client, std::string title, QWidget *mainWin);

    void tryToAddDevice(const ZenSensorDesc& desc) noexcept;
    void addDiscoveredDevice(const ZenSensorDesc& desc) noexcept;

    const ZenSensorDesc& getCurrentSensorDesc() const;
    void writeToFile(std::string fn);
    void readFromFile(std::string fn);
    void removeCurrentDevice(void);
    void copy(DiscoveryTree* dt);
    void copyTo(QComboBox *cb);
    void copyTo(std::vector<ZenSensorDesc>& ldl);

    std::vector<ZenSensorDesc> deviceVector;
    QProgressDialog* discoverProgress;

    QWidget* mainWin;
    bool scan_serial_ports_;

public slots:
    void startDiscoverDevices(bool scan_serial_ports);
    void updateDevices(void);
    void updateProgress(float progress);

signals:
    void devicesChanged(void);
    void progressUpdated(float progress);

private:
    void stopDiscoverDevices() noexcept;

    void addDevice(const ZenSensorDesc& desc) noexcept;

    zen::ZenClient& m_client;

    std::atomic_bool m_isDiscovering;
    std::vector<ZenSensorDesc> m_discoveredDevices;
};

#endif