/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#include "DiscoveryItem.h"

#include <QGridLayout>

DiscoveryItem::DiscoveryItem(QTreeWidget* tree, const ZenSensorDesc& desc)
    : desc(desc)
{
    QGridLayout* gl = new QGridLayout();

    gl->addWidget(new QLabel("Interface type:"), 1, 0);
    gl->addWidget(new QLabel("Device ID:"), 2, 0);

    gl->addWidget(deviceIdItem = new QLabel(desc.name), 2, 1);
    gl->addWidget(interfaceTypeItem = new QLabel(desc.ioType), 1, 1);
    treeItem = new QTreeWidgetItem(tree, QStringList(QString(desc.ioType) + " (" + QString(desc.name) + ")"));

    QTreeWidgetItem* subTreeItem = new QTreeWidgetItem(treeItem);

    setLayout(gl);

    tree->setItemWidget(subTreeItem, 0, this);
}