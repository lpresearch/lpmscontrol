/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#include "DiscoveryTree.h"

#include <boost/tokenizer.hpp>

#include "ZenTypes.h"

namespace
{
    constexpr const int MAXIMUM_PROGRESS = 100;
}

DiscoveryTree::DiscoveryTree(zen::ZenClient& client, std::string title, QWidget *mainWin)
    : QTreeWidget()
    , mainWin(mainWin)
    , m_client(client)
    , m_isDiscovering(false)
	, scan_serial_ports_(true)
{
    setColumnCount(1);
    setHeaderLabel(QString(title.c_str()));

    selectionModel()->setCurrentIndex(QModelIndex(), QItemSelectionModel::Clear);
    setVerticalScrollMode(QTreeView::ScrollPerPixel);

    connect(this, SIGNAL(progressUpdated(float)), this, SLOT(updateProgress(float)), Qt::BlockingQueuedConnection);
    // connect(this, SIGNAL(devicesChanged()), this, SLOT(updateDevices()));
}

void DiscoveryTree::addDiscoveredDevice(const ZenSensorDesc& desc) noexcept
{
    if (m_isDiscovering)
        m_discoveredDevices.push_back(desc);
}

void DiscoveryTree::startDiscoverDevices(bool scan_serial_ports) {
    if (m_isDiscovering.exchange(true))
        return;

    scan_serial_ports_ = scan_serial_ports;

    discoverProgress = new QProgressDialog("Discovering LPMS devices...", QString(), 0, MAXIMUM_PROGRESS, mainWin);
    discoverProgress->setWindowFlags(discoverProgress->windowFlags() & ~Qt::WindowContextHelpButtonHint & ~Qt::WindowCloseButtonHint);
    discoverProgress->setWindowModality(Qt::WindowModal);
    discoverProgress->setMinimumWidth(400);
    discoverProgress->setAutoReset(false);
    discoverProgress->show();

    m_discoveredDevices.clear();
    if (auto error = m_client.listSensorsAsync())
        stopDiscoverDevices();
}

void DiscoveryTree::updateProgress(float progress)
{
    if (m_isDiscovering)
    {
        discoverProgress->setValue(progress * MAXIMUM_PROGRESS);
        discoverProgress->show();

        if (progress == 1.0f)
        {
            stopDiscoverDevices();
            updateDevices();
        }
    }
}

void DiscoveryTree::stopDiscoverDevices() noexcept
{
    discoverProgress->close();
    delete discoverProgress;
    discoverProgress = nullptr;
    m_isDiscovering = false;
}

void DiscoveryTree::updateDevices()
{
    clear();

    for (const ZenSensorDesc& desc : m_discoveredDevices)
        addDevice(desc);

    update();
}

void DiscoveryTree::writeToFile(std::string fn)
{
    std::ofstream f(fn.c_str(), std::ios::trunc);

    if (f.is_open()) {
        for (int i = 0; i < topLevelItemCount(); i++) {
            try {
                auto* item = (DiscoveryItem*) itemWidget(topLevelItem(i)->child(0), 0);
                f << item->desc.name << "," << item->desc.serialNumber << "," << item->desc.ioType << "," << item->desc.identifier << "," << item->desc.baudRate << std::endl;
            }
            catch (std::ofstream::failure e) {
                break;
            }
        }
        f.close();
    }
}

void DiscoveryTree::readFromFile(std::string fn)
{
    std::string serialNumber;
    std::string ioType;

    std::ifstream f(fn.c_str());

    if (f.is_open()) {
        std::string line;
        boost::char_separator<char> sep(",", 0, boost::keep_empty_tokens);
        while (std::getline(f, line))
        {
            boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
            if (std::distance(tokens.begin(), tokens.end()) != 5)
                continue;

            try
            {
                auto it = tokens.begin();
                const std::string& name = it.current_token(); it++;
                const std::string& serialNumber = it.current_token(); it++;
                const std::string& ioType = it.current_token(); it++;
                const std::string& identifier = it.current_token(); it++;
                const uint32_t baudRate = std::stoul(it.current_token());

                ZenSensorDesc desc;
                std::memcpy(desc.name, name.c_str(), name.size());
                desc.name[name.size()] = '\0';
                std::memcpy(desc.serialNumber, serialNumber.c_str(), serialNumber.size());
                desc.serialNumber[serialNumber.size()] = '\0';
                std::memcpy(desc.ioType, ioType.c_str(), ioType.size());
                desc.ioType[ioType.size()] = '\0';
                std::memcpy(desc.identifier, identifier.c_str(), identifier.size());
                desc.identifier[identifier.size()] = '\0';
                desc.baudRate = baudRate;

                tryToAddDevice(desc);
            }
            catch (...)
            {
                continue;
            }
        }

        f.close();
    }
}

void DiscoveryTree::addDevice(const ZenSensorDesc& desc) noexcept
{
    const std::string_view ioType(desc.ioType);
    if (ioType.rfind("WindowsDevice") != std::string::npos && scan_serial_ports_ == false) return;
    
    const std::string_view name(desc.name);
    if (name.rfind("lpms") == std::string::npos && name.rfind("LPMS") == std::string::npos && ioType != "WindowsDevice") return;
    
    DiscoveryItem *c = new DiscoveryItem(this, desc);

    addTopLevelItem(c->treeItem);
    setCurrentItem(c->treeItem);
    c->treeItem->setExpanded(true);
}

void DiscoveryTree::tryToAddDevice(const ZenSensorDesc& desc) noexcept
{
    const std::string_view name(desc.name);
    const std::string_view serialNumber(desc.serialNumber);
    const std::string_view ioType(desc.ioType);

    for (int i = 0; i < topLevelItemCount(); i++)
    {
        auto* item = (DiscoveryItem*)itemWidget(topLevelItem(i)->child(0), 0);
        if (item->desc.name == name &&
            item->desc.serialNumber == serialNumber &&
            item->desc.ioType == ioType)
            return;
    }

    addDevice(desc);
}

void DiscoveryTree::removeCurrentDevice(void)
{
    if (topLevelItemCount() > 0) {
        // std::cout << indexOfTopLevelItem(currentItem()) << std::endl;
        if (indexOfTopLevelItem(currentItem()) == -1) {
            takeTopLevelItem(indexOfTopLevelItem(currentItem()->parent()));
        }
        else {
            takeTopLevelItem(indexOfTopLevelItem(currentItem()));
        }
    }
}

const ZenSensorDesc& DiscoveryTree::getCurrentSensorDesc() const
{
    DiscoveryItem *di;

    QTreeWidgetItem *wi = currentItem();
    if (wi->childCount() > 0) {
        QTreeWidgetItem *si = wi->child(0);
        di = (DiscoveryItem*)itemWidget(si, 0);
    }
    else {
        di = (DiscoveryItem*)itemWidget(wi, 0);
    }

    return di->desc;
}

void DiscoveryTree::copy(DiscoveryTree* dt)
{
    clear();

    for (int i = 0; i < dt->topLevelItemCount(); i++) {
        auto* item = (DiscoveryItem*)dt->itemWidget(dt->topLevelItem(i)->child(0), 0);
        tryToAddDevice(item->desc);
        std::cout << "copy" << std::endl;
    }

    dt->update();
}

void DiscoveryTree::copyTo(QComboBox *cb)
{
    cb->clear();

    for (int i = 0; i < topLevelItemCount(); i++) {
        auto* item = (DiscoveryItem*) itemWidget(topLevelItem(i)->child(0), 0);
        cb->addItem(QString(item->desc.ioType) + " (" + QString(item->desc.name) + ")");
    }

    cb->update();
}

void DiscoveryTree::copyTo(std::vector<ZenSensorDesc>& ldl)
{
    for (int i = 0; i < topLevelItemCount(); ++i)
    {
        auto* item = (DiscoveryItem*)itemWidget(topLevelItem(i)->child(0), 0);
        ldl.push_back(item->desc);
    }
}