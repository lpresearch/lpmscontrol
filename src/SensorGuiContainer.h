#ifndef SENSOR_GUI_CONTAINER
#define SENSOR_GUI_CONTAINER 

#include <atomic>

#include <QTreeWidgetItem>
#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QCheckBox>
#include <QSpinBox>

#include "LpMagnetometerCalibration.h"
#include "OpenZen.h"
#include "SensorGuiContainerBase.h"

class SensorGuiContainer : public SensorGuiContainerBase
{
Q_OBJECT

public:
    QLabel* addressItem;
    QLabel* statusItem;
    QLabel* indexItem;
    QLabel* runningItem;
    QLabel* firmwareItem;
    QLabel* batteryItem;
    QComboBox* filterModeCombo;
    QComboBox* thresholdEnableCombo;
    QComboBox* gyrRangeCombo;
    QComboBox* accRangeCombo;
    QComboBox* magRangeCombo;
    QComboBox* gyrAutocalibrationCombo;
    QComboBox* samplingRateCombo;
    QComboBox* magThreshEnableCombo;
    QCheckBox* selectAngularVelocity;
    QCheckBox* selectQuaternion;
    QCheckBox* selectEuler;
    QCheckBox* selectLinAcc;
    QCheckBox* selectGyro;
    QCheckBox* selectAcc;
    QCheckBox* selectMag;
    QCheckBox* selectPressure;
    QCheckBox* selectAltitude;
    QCheckBox* selectTemperature;
    QCheckBox* selectHeaveMotion;
	QCheckBox* selectLowPrecision;
    QSpinBox* linAccCompRateSpinner;
    QSpinBox* centriCompRateSpinner;
    QSpinBox* magCovarSpinner;
    QComboBox* baudRateCombo;
    QComboBox* uartFormatCombo;
    QGridLayout* selectedDataGl;

	QComboBox* canTpdoCombo[16];

	QComboBox* canTpdo1ACombo;
	QComboBox* canTpdo1BCombo;
	QComboBox* canTpdo2ACombo;
	QComboBox* canTpdo2BCombo;
	QComboBox* canTpdo3ACombo;
	QComboBox* canTpdo3BCombo;
	QComboBox* canTpdo4ACombo;
	QComboBox* canTpdo4BCombo;
	QComboBox* canTpdo5ACombo;
	QComboBox* canTpdo5BCombo;
	QComboBox* canTpdo6ACombo;
	QComboBox* canTpdo6BCombo;
	QComboBox* canTpdo7ACombo;
	QComboBox* canTpdo7BCombo;
	QComboBox* canTpdo8ACombo;
	QComboBox* canTpdo8BCombo;
	QComboBox* canHeartbeatCombo;
	QComboBox* canChannelModeCombo;
	QComboBox* canPointModeCombo;
	QComboBox* canProtocolCombo;
	QComboBox* canBaudrateCombo;
	QSpinBox* canStartIdSpin;
    int deviceType;

    SensorGuiContainer( int openMatId, 
                        zen::ZenSensor sensor, 
                        zen::ZenSensorComponent imu, 
                        const ZenSensorDesc& sensorDesc, 
                        std::string modelName,
                        QTreeWidget* tree);

    zen::ZenSensor& getSensor() { return m_sensor; }
    const zen::ZenSensor& getSensor() const { return m_sensor; }
    std::string getSensorName() const { return m_sensorDesc.name; }

    zen::ZenSensorComponent getImuComponent() { return m_imu; }
    const zen::ZenSensorComponent& getImuComponent() const { return m_imu; }

	bool hasImuData() const { return m_hasImuData; }
    const ZenImuData& lastImuData() const { return m_imuData; }
	void setLastImuData(const ZenImuData& imuData) { m_imuData = imuData; m_hasImuData = true; }

    int getOpenMatId() const { return m_openMatId; }
    void setOpenMatId(int id) { m_openMatId = id; }

	bool hasFieldMap() const { return m_hasFieldMap; }
	void lastFieldMap(float outFieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW][3]) const;
	void setLastFieldMap(const LpVector3f fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW]);

    void setupSensorGuiContainer();

public slots:
    void updateStreaming(int i);
    void updateData(void);
    void updateGyrThresholdEnable(int i);
    void updateMagCovar(int i);
    void updateFilterMode(int i);
    void updateGyrRange(int i);
    void updateAccRange(int i);
    void updateMagRange(int i);
    void updateGyrAutocalibration(int i);
    void updatesamplingRate(int i);
    void updateSelectGyro(int i);
    void updateSelectQuaternion(int i);
    void updateSelectEuler(int i);
    void updateSelectLinAcc(int i);
    void updateSelectAcc(int i);
    void updateSelectMag(int i);
    void updateSelectPressure(int i);
    void updateSelectAltitude(int i);
    void updateSelectTemperature(int i);
    void updateSelectAngularVelocity(int i);
    void updateSelectHeaveMotion(int i);
    void updateLinAccCompRate(int i);
    void updateCentriCompRate(int i);
    void updateLpBusDataMode(int i);
    void updateBaudRateIndex(int i);
    void updateUartFormatIndex(int i);
	void updateCanMapping(int i);
	void updateCanHeartbeat(int i);
	void updateCanBaudrate(int i);
	void updateCanStartId(int i);
	void updateCanPointMode(int i);
	void updateCanChannelMode(int i);

private:
    bool containerSetup;

	LpVector3f m_fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW];

    ZenImuData m_imuData;
    int m_openMatId;

    zen::ZenSensor m_sensor;
    zen::ZenSensorComponent m_imu;
    ZenSensorDesc m_sensorDesc;

    QComboBox* m_streamingCombo;

	std::atomic_bool m_hasFieldMap;
	std::atomic_bool m_hasImuData;
};

#endif