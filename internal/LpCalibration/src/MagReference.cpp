#include "MagReference.h"

#include <unordered_map>

#include "CalcMagnetometer.h"

namespace
{
    uintptr_t g_counter = 1;
    std::unordered_map<uintptr_t, lp::MagRefCalibrator> g_map;
}

LpMagRefHandle lpCreateMagRefCalibrator()
{
    auto pair = g_map.emplace(g_counter++, lp::MagRefCalibrator());
    return pair.first->first;
}

LpCalibrationError lpDestroyMagRefCalibrator(LpMagRefHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    g_map.erase(it);
    return LpCaliError_None;
}

LpCalibrationError lpResetMagRefCalibrator(LpMagRefHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    it->second.reset();
    return LpCaliError_None;
}

LpCalibrationError lpCalibrateMagRefCalibrator(LpMagRefHandle handle, const float acc[3], const float mag[3], float deltaT)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    return it->second.calibrate(acc, mag, deltaT);
}

bool lpIsMagRefCalibrationFinished(LpMagRefHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return false;

    return it->second.finished();
}

LpCalibrationError lpMagRefCalibration(LpMagRefHandle handle, LpMagRefCalibration* const outValue)
{
    if (outValue == nullptr)
        return LpCaliError_MissingArgument;

    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    *outValue = it->second.calibration();
    return LpCaliError_None;
}

namespace lp
{
    MagRefCalibrator::MagRefCalibrator()
        : m_elapsedTime(0.f)
		, m_nSamples(0)
        , m_finished(false)
    {
		vectZero3x1(&m_accum);
	}

    LpCalibrationError MagRefCalibrator::calibrate(const float acc[3], const float mag[3], float deltaT)
    {
        if (m_finished)
            return LpCaliError_None;

        float inc;
        LpVector3f a, m, temp;

        convertArrayToLpVector3f(acc, &a);
        convertArrayToLpVector3f(mag, &m);
		getReferenceYZ(m, a, &temp, &inc);

		m_accum.data[0] += inc;
		m_accum.data[1] += temp.data[1];
		m_accum.data[2] += temp.data[2];

        if (m_elapsedTime >= CALIBRATION_DURATION)
            m_finished = true;

        return LpCaliError_None;
    }

    void MagRefCalibrator::reset()
    {
		vectZero3x1(&m_accum);
        m_elapsedTime = 0.f;
		m_nSamples = 0;
        m_finished = false;
    }

    LpMagRefCalibration MagRefCalibrator::calibration() const
    {
        LpMagRefCalibration cali;

        if (!m_finished)
        {
			vectZero3x1(&cali.reference);
            return cali;
        }

		const float inv = 1.f / m_nSamples;

		cali.reference.data[0] = 0.f;
		cali.reference.data[1] *= inv;
		cali.reference.data[2] *= inv;

		const float n = vect3x1Norm(cali.reference);
		scalarVectMult3x1(n, &cali.reference, &cali.reference);

        return cali;
    }
}
