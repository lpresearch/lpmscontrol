#ifndef LP_CALIBRATION_API_ALIGNMENTCALIBRATION_H_
#define LP_CALIBRATION_API_ALIGNMENTCALIBRATION_H_

#include "LpMagnetometerAlignment.h"
#include "LpMatrix.h"

namespace lp
{
    class MagAlignCalibrator
    {
        constexpr static float CALIBRATION_DURATION = 2000.f;
        constexpr static unsigned int ITERATIONS = 12;

    public:
        MagAlignCalibrator();

        LpCalibrationError calibrate(LpMagAlignCaliStage stage, const float acc[3], const float mag[3], float deltaT);
        void reset();

        LpMagAlignCalibration calibration() const;

        LpMagAlignCaliStage stage() const { return m_stage; }

    private:
        LpVector3f m_a[ITERATIONS];
        LpVector3f m_b[ITERATIONS];

        float m_elapsedTime;

        unsigned int m_nSamples;
        LpMagAlignCaliStage m_stage;
    };
}
#endif
