#ifndef LP_CALIBRATION_MAGCALIBRATION_H_
#define LP_CALIBRATION_MAGCALIBRATION_H_

#include "LpMagnetometerCalibration.h"
#include "LpMatrix.h"

namespace lp
{
    class MagCalibrator
    {
        constexpr static float CALIBRATION_DURATION = 20000.f;

    public:
        MagCalibrator();

        LpCalibrationError calibrate(const float acc[3], const float mag[3], float deltaT);
        void reset();

        LpMagCalibration calibration() const;

        bool finished() const { return m_finished; }

    private:
        float m_elapsedTime;
        bool m_finished;
    };
}
#endif
