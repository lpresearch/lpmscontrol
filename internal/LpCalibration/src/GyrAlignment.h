#ifndef LP_CALIBRATION_API_GYROALIGNMENTCALIBRATION_H_
#define LP_CALIBRATION_API_GYROALIGNMENTCALIBRATION_H_

#include "LpGyroAlignment.h"
#include "LpMatrix.h"

namespace lp
{
    class GyrAlignCalibrator
    {
        constexpr static float CALIBRATION_DURATION = 2000.f;
        constexpr static unsigned int ITERATIONS = 3;

    public:
        GyrAlignCalibrator();

        LpCalibrationError calibrate(LpGyrAlignCaliStage stage, const float gyr[3], float deltaT);
        void reset();

        LpGyrAlignCalibration calibration() const;

        LpGyrAlignCaliStage stage() const { return m_stage; }

    private:
        LpVector3f m_a[ITERATIONS];
        LpVector3f m_b[ITERATIONS];

        float m_elapsedTime;

        unsigned int m_nSamples;
        LpGyrAlignCaliStage m_stage;
    };
}

#endif
