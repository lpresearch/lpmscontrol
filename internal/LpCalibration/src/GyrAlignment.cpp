#include "GyrAlignment.h"

#define _USE_MATH_DEFINES
#include <cmath>
#undef _USE_MATH_DEFINES

#include <unordered_map>

#include "CalcMisalignment.h"

namespace
{
    uintptr_t g_counter = 1;
    std::unordered_map<uintptr_t, lp::GyrAlignCalibrator> g_map;
}

LpGyrAlignHandle lpCreateGyrAlignCalibrator()
{
    auto pair = g_map.emplace(g_counter++, lp::GyrAlignCalibrator());
    return pair.first->first;
}

LpCalibrationError lpDestroyGyrAlignCalibrator(LpGyrAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    g_map.erase(it);
    return LpCaliError_None;
}

LpCalibrationError lpResetGyrAlignCalibrator(LpGyrAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    it->second.reset();
    return LpCaliError_None;
}

LpCalibrationError lpCalibrateGyrAlignCalibrator(LpGyrAlignHandle handle, LpGyrAlignCaliStage stage, const float gyr[3], float deltaT)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    return it->second.calibrate(stage, gyr, deltaT);
}

LpGyrAlignCaliStage lpGyrAlignCalibrationStage(LpGyrAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpGyrAlignCali_Invalid;

    return it->second.stage();
}

LpCalibrationError lpGyrAlignCalibration(LpGyrAlignHandle handle, LpGyrAlignCalibration* const outValue)
{
    if (outValue == nullptr)
        return LpCaliError_MissingArgument;

    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    *outValue = it->second.calibration();
    return LpCaliError_None;
}

namespace lp
{
    GyrAlignCalibrator::GyrAlignCalibrator()
        : m_elapsedTime(0.f)
        , m_nSamples(0)
        , m_stage(LpGyrAlignCali_XAxis)
    {
        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpCalibrationError GyrAlignCalibrator::calibrate(LpGyrAlignCaliStage stage, const float gyr[3], float deltaT)
    {
        if (m_stage == LpGyrAlignCali_Finished)
            return LpCaliError_None;

        if (m_stage != stage)
            return LpCaliError_WrongStage;

        const unsigned int idx = m_stage - 1;

        for (int i = 0; i < 3; ++i)
            m_a[idx].data[i] += gyr[i];

        m_elapsedTime += deltaT;
        ++m_nSamples;

        if (m_elapsedTime >= CALIBRATION_DURATION)
        {
            m_stage = LpGyrAlignCaliStage(m_stage + 1);
            if (m_stage == LpGyrAlignCali_Finished)
            {
                const float invSamples = 1.f / m_nSamples;
                for (auto i = 0; i < ITERATIONS; ++i)
                {
                    scalarVectMult3x1(invSamples, &m_a[i], &m_a[i]);

                    for (int j = 0; j < 3; ++j)
                    {
                        if (m_a[i].data[j] > 50.f)
                            m_b[i].data[j] = 270.f;
                        else if (m_a[i].data[j] < -50.f)
                            m_b[i].data[j] = -270.f;
                        else
                            m_b[i].data[j] = 0.f;
                    }
                }
            }
        }

        return LpCaliError_None;
    }

    void GyrAlignCalibrator::reset()
    {
        m_elapsedTime = 0.f;
        m_nSamples = 0;
        m_stage = LpGyrAlignCali_XAxis;

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpGyrAlignCalibration GyrAlignCalibrator::calibration() const
    {
        LpGyrAlignCalibration cali;

        if (m_stage != LpGyrAlignCali_Finished)
        {
            createIdentity3x3(&cali.alignmentMatrix);
            return cali;
        }

        const float* aPtrs[ITERATIONS * 2];
        const float* bPtrs[ITERATIONS * 2];
        for (auto i = 0; i < ITERATIONS; ++i)
        {
            aPtrs[i] = m_a[i].data;
            bPtrs[i] = m_b[i].data;
        }

        LpVector3f na[ITERATIONS];
        for (auto i = 0; i < ITERATIONS; ++i)
            scalarVectMult3x1(-1.f, &m_a[i], &na[i]);

        LpVector3f nb[ITERATIONS];
        for (auto i = 0; i < ITERATIONS; ++i)
            scalarVectMult3x1(-1.f, &m_b[i], &nb[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
        {
            aPtrs[3 + i] = na[i].data;
            bPtrs[3 + i] = nb[i].data;
        }

        maCalCalcGyrMisalignment(aPtrs, bPtrs, &cali.alignmentMatrix, &cali.alignmentBias, 2 * ITERATIONS);
        scalarVectMult3x1(float(M_PI) / 180.f, &cali.alignmentBias, &cali.alignmentBias);

        return cali;
    }
}
