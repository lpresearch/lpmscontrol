/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#include "CalcMisalignment.h"

float maCalPythag(float a, float b)
{
	return 0.0f;
}

void maCalCalcSVD(float **mat, int m, int n, float **w, float **v, int maxCalElements)
{
}

void maCalCalcGyrMisalignment(const float **misalignmentAData,
	const float **misalignmentBData, LpMatrix3x3f *R, LpVector3f *t,
	int nEquations)
{
}

void maCalCalcAccMisalignment(const float **misalignmentAData,
	const float **misalignmentBData, LpMatrix3x3f *R, LpVector3f *t,
	int nEquations)
{
}

void maCalCalcMagMisalignment(const float **misalignmentAData, const float **misalignmentBData, LpMatrix3x3f *R, LpVector3f *t, int nEquations)
{
}
