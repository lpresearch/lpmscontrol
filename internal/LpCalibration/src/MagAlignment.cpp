#include "MagAlignment.h"

#include <unordered_map>

#include "CalcMisalignment.h"

namespace
{
    uintptr_t g_counter = 1;
    std::unordered_map<uintptr_t, lp::MagAlignCalibrator> g_map;
}

LpMagAlignHandle lpCreateMagAlignCalibrator()
{
    auto pair = g_map.emplace(g_counter++, lp::MagAlignCalibrator());
    return pair.first->first;
}

LpCalibrationError lpDestroyMagAlignCalibrator(LpMagAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    g_map.erase(it);
    return LpCaliError_None;
}

LpCalibrationError lpResetMagAlignCalibrator(LpMagAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    it->second.reset();
    return LpCaliError_None;
}

LpCalibrationError lpCalibrateMagAlignCalibrator(LpMagAlignHandle handle, LpMagAlignCaliStage stage, const float acc[3], const float mag[3], float deltaT)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    return it->second.calibrate(stage, acc, mag, deltaT);
}

LpMagAlignCaliStage lpMagAlignCalibrationStage(LpMagAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpMagAlignCali_Invalid;

    return it->second.stage();
}

LpCalibrationError lpMagAlignCalibration(LpMagAlignHandle handle, LpMagAlignCalibration* const outValue)
{
    if (outValue == nullptr)
        return LpCaliError_MissingArgument;

    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    *outValue = it->second.calibration();
    return LpCaliError_None;
}

namespace lp
{
    MagAlignCalibrator::MagAlignCalibrator()
        : m_elapsedTime(0.f)
        , m_nSamples(0)
        , m_stage(LpMagAlignCali_ZUpNoCoil)
    {
        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpCalibrationError MagAlignCalibrator::calibrate(LpMagAlignCaliStage stage, const float acc[3], const float mag[3], float deltaT)
    {
        if (m_stage == LpMagAlignCali_Finished)
            return LpCaliError_None;

        if (m_stage != stage)
            return LpCaliError_WrongStage;

        const unsigned int idx = m_stage - 1;

        for (int i = 0; i < 3; ++i)
            m_a[idx].data[i] += mag[i];

        for (int i = 0; i < 3; ++i)
        {
            if (acc[i] > 50.f)
                m_b[idx].data[i] = 100.f;
            else if (acc[i] < -50.f)
                m_b[idx].data[i] = -100.f;
            else
                m_b[idx].data[i] = 0.f;
        }

        m_elapsedTime += deltaT;
        ++m_nSamples;

        if (m_elapsedTime >= CALIBRATION_DURATION)
        {
            m_stage = LpMagAlignCaliStage(m_stage + 1);
            if (m_stage == LpMagAlignCali_Finished)
            {
                const float invSamples = 1.f / m_nSamples;
                for (auto i = 0; i < ITERATIONS; ++i)
                {
                    scalarVectMult3x1(invSamples, &m_a[i], &m_a[i]);
                    if (i % 2 == 1)
                        vectSub3x1(&m_a[i], &m_a[i], &m_a[i - 1]);
                }
            }
        }

        return LpCaliError_None;
    }

    void MagAlignCalibrator::reset()
    {
        m_elapsedTime = 0.f;
        m_nSamples = 0;
        m_stage = LpMagAlignCali_ZUpNoCoil;

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpMagAlignCalibration MagAlignCalibrator::calibration() const
    {
        LpMagAlignCalibration cali;

        if (m_stage != LpMagAlignCali_Finished)
        {
            createIdentity3x3(&cali.alignmentMatrix);
            return cali;
        }

        const float* aPtrs[ITERATIONS / 2];
        const float* bPtrs[ITERATIONS / 2];
        for (auto i = 0; i < ITERATIONS / 2; ++i)
        {
            aPtrs[i] = m_a[i * 2 + 1].data;
            bPtrs[i] = m_b[i * 2 + 1].data;
        }

        maCalCalcMagMisalignment(aPtrs, bPtrs, &cali.alignmentMatrix, &cali.accBias, ITERATIONS);
        return cali;
    }
}
