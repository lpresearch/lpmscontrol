#ifndef LP_CALIBRATION_ACCALIGNMENT_H_
#define LP_CALIBRATION_ACCALIGNMENT_H_

#include "LpAccelerometerAlignment.h"
#include "LpMatrix.h"

namespace lp
{
    class AccAlignCalibrator
    {
        constexpr static float CALIBRATION_DURATION = 1000.f;
        constexpr static unsigned int ITERATIONS = 6;

    public:
        AccAlignCalibrator();

        LpCalibrationError calibrate(LpAccAlignCaliStage stage, const float acc[3], float deltaT);
        void reset();

        LpAccAlignCalibration calibration() const;

        LpAccAlignCaliStage stage() const { return m_stage; }

    private:
        LpVector3f m_a[ITERATIONS];
        LpVector3f m_b[ITERATIONS];

        float m_elapsedTime;

        unsigned int m_nSamples;
        LpAccAlignCaliStage m_stage;
    };
}
#endif
