#include "MagCalibration.h"

#include <unordered_map>

#include "CalcMagnetometer.h"

namespace
{
    uintptr_t g_counter = 1;
    std::unordered_map<uintptr_t, lp::MagCalibrator> g_map;
}

LpMagHandle lpCreateMagCalibrator()
{
    auto pair = g_map.emplace(g_counter++, lp::MagCalibrator());
    return pair.first->first;
}

LpCalibrationError lpDestroyMagCalibrator(LpMagHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    g_map.erase(it);
    return LpCaliError_None;
}

LpCalibrationError lpResetMagCalibrator(LpMagHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    it->second.reset();
    return LpCaliError_None;
}

LpCalibrationError lpCalibrateMagCalibrator(LpMagHandle handle, const float acc[3], const float mag[3], float deltaT)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    return it->second.calibrate(acc, mag, deltaT);
}

bool lpIsMagCalibrationFinished(LpMagHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return false;

    return it->second.finished();
}

LpCalibrationError lpMagCalibration(LpMagHandle handle, LpMagCalibration* const outValue)
{
    if (outValue == nullptr)
        return LpCaliError_MissingArgument;

    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    *outValue = it->second.calibration();
    return LpCaliError_None;
}

namespace lp
{
    MagCalibrator::MagCalibrator()
        : m_elapsedTime(0.f)
        , m_finished(false)
    {}

    LpCalibrationError MagCalibrator::calibrate(const float acc[3], const float mag[3], float deltaT)
    {
        if (m_finished)
            return LpCaliError_None;

        float bInc;
        LpVector3f a, tV, tV2;

        convertArrayToLpVector3f(acc, &a);
        convertArrayToLpVector3f(mag, &tV);
        bCalOrientationFromAccMag(tV, a, &tV2, &bInc);

        m_elapsedTime += deltaT;
        bCalUpdateBMap(tV2, tV);

        if (m_elapsedTime >= CALIBRATION_DURATION)
            m_finished = true;

        return LpCaliError_None;
    }

    void MagCalibrator::reset()
    {
        m_elapsedTime = 0.f;
        m_finished = false;
    }

    LpMagCalibration MagCalibrator::calibration() const
    {
        LpMagCalibration cali;

        if (!m_finished)
        {
			createIdentity3x3(&cali.softIronMatrix);
			vectZero3x1(&cali.hardIronOffset);
			cali.fieldRadius = 0.f;

			for (int i = 0; i < ABSMAXPITCH; ++i)
				for (int j = 0; j < ABSMAXROLL; ++j)
					for (int k = 0; k < ABSMAXYAW; ++k)
						vectZero3x1(&cali.fieldMap[i][j][k]);

            return cali;
        }

		for (int i = 0; i < ABSMAXPITCH; ++i)
			for (int j = 0; j < ABSMAXROLL; ++j)
				for (int k = 0; k < ABSMAXYAW; ++k)
					for (int l = 0; l < 3; ++l)
						cali.fieldMap[i][j][k].data[l] = bCalGetFieldMapElement(i, j, k, l);

		if (bCalFitEllipsoid() == 1)
		{
			cali.softIronMatrix = bCalGetSoftIronMatrix();
			cali.hardIronOffset = bCalGetHardIronOffset();
			cali.fieldRadius = bCalGetFieldRadius();
		}
		else
		{
			createIdentity3x3(&cali.softIronMatrix);
			vectZero3x1(&cali.hardIronOffset);
			cali.fieldRadius = 0.f;
		}

        return cali;
    }
}
