#ifndef LP_CALIBRATION_MAGREFERENCE_H_
#define LP_CALIBRATION_MAGREFERENCE_H_

#include "LpMagnetometerReference.h"
#include "LpMatrix.h"

namespace lp
{
    class MagRefCalibrator
    {
        constexpr static float CALIBRATION_DURATION = 1000.f;

    public:
        MagRefCalibrator();

        LpCalibrationError calibrate(const float acc[3], const float mag[3], float deltaT);
        void reset();

        LpMagRefCalibration calibration() const;

        bool finished() const { return m_finished; }

    private:
        LpVector3f m_accum;
        float m_elapsedTime;

		unsigned int m_nSamples;
        bool m_finished;
    };
}
#endif
