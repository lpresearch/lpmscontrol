#include "AccAlignment.h"

#include <unordered_map>

#include "CalcMisalignment.h"

namespace
{
    uintptr_t g_counter = 1;
    std::unordered_map<uintptr_t, lp::AccAlignCalibrator> g_map;
}

LpAccAlignHandle lpCreateAccAlignCalibrator()
{
    auto pair = g_map.emplace(g_counter++, lp::AccAlignCalibrator());
    return pair.first->first;
}

LpCalibrationError lpDestroyAccAlignCalibrator(LpAccAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    g_map.erase(it);
    return LpCaliError_None;
}

LpCalibrationError lpResetAccAlignCalibrator(LpAccAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    it->second.reset();
    return LpCaliError_None;
}

LpCalibrationError lpCalibrateAccAlignCalibrator(LpAccAlignHandle handle, LpAccAlignCaliStage stage, const float acc[3], float deltaT)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    return it->second.calibrate(stage, acc, deltaT);
}

LpAccAlignCaliStage lpAccAlignCalibrationStage(LpAccAlignHandle handle)
{
    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpAccAlignCali_Invalid;

    return it->second.stage();
}

LpCalibrationError lpAccAlignCalibration(LpAccAlignHandle handle, LpAccAlignCalibration* const outValue)
{
    if (outValue == nullptr)
        return LpCaliError_MissingArgument;

    auto it = g_map.find(handle);
    if (it == g_map.end())
        return LpCaliError_InvalidHandle;

    *outValue = it->second.calibration();
    return LpCaliError_None;
}

namespace lp
{
    AccAlignCalibrator::AccAlignCalibrator()
        : m_elapsedTime(0.f)
        , m_nSamples(0)
        , m_stage(LpAccAlignCali_ZUp)
    {
        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpCalibrationError AccAlignCalibrator::calibrate(LpAccAlignCaliStage stage, const float acc[3], float deltaT)
    {
        if (m_stage == LpAccAlignCali_Finished)
            return LpCaliError_None;

        if (m_stage != stage)
            return LpCaliError_WrongStage;

        const unsigned int idx = m_stage - 1;

        for (int i = 0; i < 3; ++i)
            m_a[idx].data[i] += acc[i];

        for (int i = 0; i < 3; ++i)
        {
            if (acc[i] > 0.5f)
                m_b[idx].data[i] = 1.f;
            else if (acc[i] < -0.5f)
                m_b[idx].data[i] = -1.f;
            else
                m_b[idx].data[i] = 0.f;
        }

        m_elapsedTime += deltaT;
        ++m_nSamples;

        if (m_elapsedTime >= CALIBRATION_DURATION)
        {
            m_stage = LpAccAlignCaliStage(m_stage + 1);
            if (m_stage == LpAccAlignCali_Finished)
            {
                const float invSamples = 1.f / m_nSamples;
                for (auto i = 0; i < ITERATIONS; ++i)
                    scalarVectMult3x1(invSamples, &m_a[i], &m_a[i]);
            }
        }

        return LpCaliError_None;
    }

    void AccAlignCalibrator::reset()
    {
        m_elapsedTime = 0.f;
        m_nSamples = 0;
        m_stage = LpAccAlignCali_ZUp;

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_a[i]);

        for (auto i = 0; i < ITERATIONS; ++i)
            vectZero3x1(&m_b[i]);
    }

    LpAccAlignCalibration AccAlignCalibrator::calibration() const
    {
        LpAccAlignCalibration cali;

        if (m_stage != LpAccAlignCali_Finished)
        {
            createIdentity3x3(&cali.alignmentMatrix);
            return cali;
        }

        const float* aPtrs[ITERATIONS];
        const float* bPtrs[ITERATIONS];
        for (auto i = 0; i < ITERATIONS; ++i)
        {
            aPtrs[i] = m_a[i].data;
            bPtrs[i] = m_b[i].data;
        }

        maCalCalcAccMisalignment(aPtrs, bPtrs, &cali.alignmentMatrix, &cali.accBias, ITERATIONS);
        return cali;
    }
}
