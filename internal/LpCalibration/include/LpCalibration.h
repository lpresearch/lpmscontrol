#ifndef LP_CALIBRATION_API_CALIBRATION_H_
#define LP_CALIBRATION_API_CALIBRATION_H_

typedef enum LpCalibrationError
{
    LpCaliError_None,

    LpCaliError_InvalidHandle,
    LpCaliError_MissingArgument,
    LpCaliError_WrongStage,

    LpCaliError_Max

} LpCalibrationError;

#endif
