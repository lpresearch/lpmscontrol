#ifndef LP_CALIBRATION_API_MAGNETOMETERCALIBRATION_H_
#define LP_CALIBRATION_API_MAGNETOMETERCALIBRATION_H_

#include <stdint.h>
#include "LpCalibration.h"
#include "LpMatrix.h"

/* Magnetic field map dimensions. */
#define ABSMAXPITCH 3
#define ABSMAXROLL 6
#define ABSMAXYAW 6

typedef uintptr_t LpMagHandle;

struct LpMagCalibration
{
	LpVector3f fieldMap[ABSMAXPITCH][ABSMAXROLL][ABSMAXYAW];
	LpMatrix3x3f softIronMatrix;
	LpVector3f hardIronOffset;
	float fieldRadius;
};

/** Returns a handle to a newly created magnetometer alignment calibrator */
LpMagHandle lpCreateMagCalibrator();

/** Destroys the magnetometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpDestroyMagCalibrator(LpMagHandle handle);

/** Resets the magnetometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpResetMagCalibrator(LpMagHandle handle);

/** Adds the calibration sample to the calibration if the handle is valid, otherwise returns an error.
  * acc (Corrected accelerometer data)
  * mag (Raw magnetometer data)
  */
LpCalibrationError lpCalibrateMagCalibrator(LpMagHandle handle, const float acc[3], const float mag[3], float deltaT);

/** Returns whether the magnetometer calibration has finished if the handle is valid, otherwise returns false. */
bool lpIsMagCalibrationFinished(LpMagHandle handle);

/** Returns a calibration result if the handle is valid, otherwise returns an error. Returns identity while the calibration is busy. */
LpCalibrationError lpMagCalibration(LpMagHandle handle, LpMagCalibration* const outValue);

#endif
