#ifndef LP_CALIBRATION_API_MAGNETOMETERALIGNMENT_H_
#define LP_CALIBRATION_API_MAGNETOMETERALIGNMENT_H_

#include <stdint.h>
#include "LpCalibration.h"
#include "LpMatrix.h"

typedef uintptr_t LpMagAlignHandle;

typedef enum LpMagAlignCaliStage
{
    LpMagAlignCali_Invalid,

    LpMagAlignCali_ZUpNoCoil,
    LpMagAlignCali_ZUpWithCoil,
    LpMagAlignCali_ZDownNoCoil,
    LpMagAlignCali_ZDownWithCoil,
    LpMagAlignCali_XUpNoCoil,
    LpMagAlignCali_XUpWithCoil,
    LpMagAlignCali_XDownNoCoil,
    LpMagAlignCali_XDownWithCoil,
    LpMagAlignCali_YUpNoCoil,
    LpMagAlignCali_YUpWithCoil,
    LpMagAlignCali_YDownNoCoil,
    LpMagAlignCali_YDownWithCoil,
    LpMagAlignCali_Finished,

    LpMagAlignCali_Max
} LpMagAlignCaliStage;

struct LpMagAlignCalibration
{
    LpMatrix3x3f alignmentMatrix;
    LpVector3f accBias;
};

/** Returns a handle to a newly created magnetometer alignment calibrator */
LpMagAlignHandle lpCreateMagAlignCalibrator();

/** Destroys the magnetometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpDestroyMagAlignCalibrator(LpMagAlignHandle handle);

/** Resets the magnetometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpResetMagAlignCalibrator(LpMagAlignHandle handle);

/** Adds the calibration sample to the calibration if the calibration is in the specified stage, otherwise returns an error. */
LpCalibrationError lpCalibrateMagAlignCalibrator(LpMagAlignHandle handle, LpMagAlignCaliStage stage, const float acc[3], const float mag[3], float deltaT);

/** Returns the magnetometer alignment calibration stage if the handle is valid, otherwise returns LpMagAlignCali_Invalid. */
LpMagAlignCaliStage lpMagAlignCalibrationStage(LpMagAlignHandle handle);

/** Returns a calibration result if the handle is valid, otherwise returns an error. Returns identity while the calibration is busy. */
LpCalibrationError lpMagAlignCalibration(LpMagAlignHandle handle, LpMagAlignCalibration* const outValue);

#endif
