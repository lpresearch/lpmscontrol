#ifndef LP_CALIBRATION_API_MAGNETOMETERREFERENCE_H_
#define LP_CALIBRATION_API_MAGNETOMETERREFERENCE_H_

#include <stdint.h>
#include "LpCalibration.h"
#include "LpMatrix.h"

/* Magnetic field map dimensions. */
#define ABSMAXPITCH 3
#define ABSMAXROLL 6
#define ABSMAXYAW 6

typedef uintptr_t LpMagRefHandle;

struct LpMagRefCalibration
{
	LpVector3f reference;
};

/** Returns a handle to a newly created magnetometer reference calibrator */
LpMagRefHandle lpCreateMagRefCalibrator();

/** Destroys the magnetometer reference calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpDestroyMagRefCalibrator(LpMagRefHandle handle);

/** Resets the magnetometer reference calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpResetMagRefCalibrator(LpMagRefHandle handle);

/** Adds the calibration sample to the calibration if the handle is valid, otherwise returns an error.
  * acc (Corrected accelerometer data)
  * mag (Corrected magnetometer data)
  */
LpCalibrationError lpCalibrateMagRefCalibrator(LpMagRefHandle handle, const float acc[3], const float mag[3], float deltaT);

/** Returns whether the magnetometer reference calibration has finished if the handle is valid, otherwise returns false. */
bool lpIsMagRefCalibrationFinished(LpMagRefHandle handle);

/** Returns a calibration result if the handle is valid, otherwise returns an error. Returns identity while the calibration is busy. */
LpCalibrationError lpMagRefCalibration(LpMagRefHandle handle, LpMagRefCalibration* const outValue);

#endif
