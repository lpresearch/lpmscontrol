#ifndef LP_CALIBRATION_API_ACCELEROMETERALIGNMENT_H_
#define LP_CALIBRATION_API_ACCELEROMETERALIGNMENT_H_

#include <stdint.h>
#include "LpCalibration.h"
#include "LpMatrix.h"

typedef uintptr_t LpAccAlignHandle;

typedef enum LpAccAlignCaliStage
{
    LpAccAlignCali_Invalid,

    LpAccAlignCali_ZUp,
    LpAccAlignCali_ZDown,
    LpAccAlignCali_XUp,
    LpAccAlignCali_XDown,
    LpAccAlignCali_YUp,
    LpAccAlignCali_YDown,
    LpAccAlignCali_Finished,

    LpAccAlignCali_Max
} LpAccAlignCaliStage;

struct LpAccAlignCalibration
{
    LpMatrix3x3f alignmentMatrix;
    LpVector3f accBias;
};

/** Returns a handle to a newly created accelerometer alignment calibrator */
LpAccAlignHandle lpCreateAccAlignCalibrator();

/** Destroys the accelerometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpDestroyAccAlignCalibrator(LpAccAlignHandle handle);

/** Resets the accelerometer alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpResetAccAlignCalibrator(LpAccAlignHandle handle);

/** Adds the calibration sample to the calibration if the calibration is in the specified stage, otherwise returns an error. */
LpCalibrationError lpCalibrateAccAlignCalibrator(LpAccAlignHandle handle, LpAccAlignCaliStage stage, const float acc[3], float deltaT);

/** Returns the accelerometer alignment calibration stage if the handle is valid, otherwise returns LpAccAlignCali_Invalid. */
LpAccAlignCaliStage lpAccAlignCalibrationStage(LpAccAlignHandle handle);

/** Returns a calibration result if the handle is valid, otherwise returns an error. Returns identity while the calibration is busy. */
LpCalibrationError lpAccAlignCalibration(LpAccAlignHandle handle, LpAccAlignCalibration* const outValue);

#endif
