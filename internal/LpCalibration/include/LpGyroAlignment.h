#ifndef LP_CALIBRATION_API_GYROALIGNMENT_H_
#define LP_CALIBRATION_API_GYROALIGNMENT_H_

#include <stdint.h>
#include "LpCalibration.h"
#include "LpMatrix.h"

typedef uintptr_t LpGyrAlignHandle;

typedef enum LpGyrAlignCaliStage
{
    LpGyrAlignCali_Invalid,

    LpGyrAlignCali_XAxis,
    LpGyrAlignCali_YAxis,
    LpGyrAlignCali_ZAxis,
    LpGyrAlignCali_Finished,

    LpGyrAlignCali_Max
} LpGyrAlignCaliStage;

struct LpGyrAlignCalibration
{
    LpMatrix3x3f alignmentMatrix;
    LpVector3f alignmentBias;
};

/** Returns a handle to a newly created gyroscope alignment calibrator */
LpGyrAlignHandle lpCreateGyrAlignCalibrator();

/** Destroys the gyroscope alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpDestroyGyrAlignCalibrator(LpGyrAlignHandle handle);

/** Resets the gyroscope alignment calibrator if the handle is valid, otherwise returns an error. */
LpCalibrationError lpResetGyrAlignCalibrator(LpGyrAlignHandle handle);

/** Adds the calibration sample to the calibration if the calibration is in the specified stage, otherwise returns an error. */
LpCalibrationError lpCalibrateGyrAlignCalibrator(LpGyrAlignHandle handle, LpGyrAlignCaliStage stage, const float gyr[3], float deltaT);

/** Returns the gyroscope alignment calibration stage if the handle is valid, otherwise returns LpGyrAlignCali_Invalid. */
LpGyrAlignCaliStage lpGyrAlignCalibrationStage(LpGyrAlignHandle handle);

/** Returns a calibration result if the handle is valid, otherwise returns an error. Returns identity while the calibration is busy. */
LpCalibrationError lpGyrAlignCalibration(LpGyrAlignHandle handle, LpGyrAlignCalibration* const outValue);

#endif
